springMvp README
=================================================

PREREQUISITES

  - Java 6
  - Maven 2.2
  - A working internet connection

CHECKING OUT THE CODE

  The source code is maintained in springMvp GIT repository. To check
  out the latest development version, use the following URL:

    https://flexguse@bitbucket.org/flexguse/springmvp-addon.git

EDITING THE CODE IN AN IDE

  The springMvp add-on project can be imported in any IDE that supports Maven.

TRYING OUT THE DEMO

  For a very quick look at the demo go to
  	http://springmvp-demo.flexguse.cloudfoundry.com/
  	
  If you want to run the demo locally, you have to	

	  1. Compile and install the entire project:
	
	    $ mvn install
	
	  2. Start the built-in Jetty web server:
	
	    $ cd demo
	    $ mvn jetty:run
	
	  3. Open your favorite web browser and point it to:
	
	    http://localhost:8080/demo/

READING THE MANUAL

  1. Generate the manual:

    $ cd manual
    $ mvn package

  2. Open the file manual/target/docbkx/html/manual.html in your favorite web browser, 
  	 read the file manual/target/docbkx/pdf/manual.pdf in your PDF readoer 
  	 or copy the file manual/target/docbkx/epub/manual.epub to your mobile reading device.

PUBLISHING THE ADD-ON

  1. Build the add-on:

    $ mvn install assembly:assembly

  2. Publish the add-on to the Vaadin directory (http://vaadin.com/directory)
