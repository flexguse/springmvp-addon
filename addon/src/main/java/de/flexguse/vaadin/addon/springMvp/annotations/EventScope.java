/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.annotations;

import de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager;
import de.flexguse.vaadin.addon.springMvp.dispatcher.SpringMvpDispatcher;

/**
 * This Interface contains all {@link SpringMvpDispatcher}s which are supported
 * by the {@link HandleSpringMvpEvent} and {@link HandleSpringMvpEvents}
 * annotations by default. It is up to the developer to use other keys for the
 * dispatchers in the {@link DispatcherManager} and the annotations.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface EventScope {

	/**
	 * The event is dispatched in the scope of the SpringMvpApplication.
	 */
	final String SpringMvpApplication = "applicationScope";

	/**
	 * The event is dispatched in the scope of all SpringMvpApplications.
	 */
	final String AllSpringMvpApplications = "allApplicationsScope";

}
