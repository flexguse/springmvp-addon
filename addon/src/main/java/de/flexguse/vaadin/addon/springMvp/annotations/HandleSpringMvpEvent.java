/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import de.flexguse.vaadin.addon.springMvp.dispatcher.SpringMvpDispatcher;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * Annotate any object method managed by Spring to register for a single Event.
 * The default eventScope is SpringMvpApplication.
 * <p>
 * The annotation can be used for one argument methods like:
 * </p>
 * <p>
 * <b> {@literal @HandleSpringMvpEvent}</br> public void
 * handleEvent(ShowErrorMessageEvent event){}</b>
 * </p>
 * The method argument must be an implementation of {@link SpringMvpEvent}.
 * <p>
 * By default the method is registered for the {@link SpringMvpDispatcher} which
 * is used in SpringMvpApplication scope. If the method should listen for events
 * which are dispatched in AllSpringMvpApplications scope, set the
 * {@link EventScope} in the annotation.
 * <p>
 * <b>
 * {@literal @HandleSpringMvpEvent(eventScopes={EventScope.AllSpringMvpApplications})}
 * </br> public void handleEvent(ShowErrorMessageEvent event){}</b>
 * </p>
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface HandleSpringMvpEvent {

	public String[] eventScopes() default {EventScope.SpringMvpApplication};

}
