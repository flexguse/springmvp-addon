/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import de.flexguse.vaadin.addon.springMvp.dispatcher.SpringMvpDispatcher;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowHumanizedMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowWarningMessageEvent;

/**
 * Annotate any method in a Spring managed object to register for multiple
 * events. This can be useful for logging or some kinds of error events.
 * 
 * <p>
 * The annotation can be used for one argument methods like:
 * </p>
 * <p>
 * <b> {@literal @HandleSpringMvpEvents(value= ShowHumanizedMessageEvent.class,
 * ShowWarningMessageEvent.class})}<br/>
 * public void handleEvents(SpringMvpEvent event){}</b>
 * </p>
 * With this annotation the method is called every time a
 * {@link ShowHumanizedMessageEvent} or a {@link ShowWarningMessageEvent} is
 * dispatched.
 * <p>
 * By default the method is registered for the {@link SpringMvpDispatcher} which
 * is used in SpringMvpApplication scope. If the method should listen for events
 * which are dispatched in AllSpringMvpApplications scope, set the
 * {@link EventScope} in the annotation.
 * </p>
 * <p>
 * <b> {@literal @HandleSpringMvpEvents(value= ShowHumanizedMessageEvent.class,
 * ShowWarningMessageEvent.class} ,
 * eventScopes={EventScope.AllSpringMvpApplications})}<br/>
 * public void handleEvents(SpringMvpEvent event){}</b>
 * </p>
 * 
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface HandleSpringMvpEvents {

	Class<? extends SpringMvpEvent>[] value() default {};

	public String[] eventScopes() default { EventScope.SpringMvpApplication };

}
