/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.application;

import org.dellroad.stuff.vaadin.SpringContextApplication;

import com.vaadin.ui.Window.Notification;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;
import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.dispatcher.SpringMvpDispatcher;
import de.flexguse.vaadin.addon.springMvp.events.CloseSpringApplicationEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.AbstractMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowErrorMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowHumanizedMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowTrayNotificationEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowWarningMessageEvent;

/**
 * Extend this application.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public abstract class SpringMvpVaadinApplication extends
		SpringContextApplication {

	private static final long serialVersionUID = -8539503906450460202L;

	/**
	 * This helper method generates the notification.
	 * 
	 * @param event
	 *            the event holding the title and the messagetext
	 * @param notificationType
	 *            the notification type, taken from {@link Notification}
	 */
	private void showNotification(AbstractMessageEvent event,
			int notificationType) {
		getMainWindow().showNotification(event.getTitle(), event.getInfo(),
				notificationType, true);
	}

	/**
	 * This event handler method is called if the
	 * {@link ShowHumanizedMessageEvent} was dispatched in the
	 * {@link SpringMvpDispatcher}.
	 */
	@HandleSpringMvpEvent(eventScopes={EventScope.AllSpringMvpApplications, EventScope.SpringMvpApplication})
	public void onApplicationEvent(ShowHumanizedMessageEvent event) {
		showNotification(event, Notification.TYPE_HUMANIZED_MESSAGE);
	}

	/**
	 * This event handler method is called if the
	 * {@link ShowWarningMessageEvent} was dispatched in the
	 * {@link SpringMvpDispatcher}.
	 * 
	 * @param event
	 */
	@HandleSpringMvpEvent(eventScopes={EventScope.AllSpringMvpApplications, EventScope.SpringMvpApplication})
	public void onApplicationEvent(ShowWarningMessageEvent event) {
		showNotification(event, Notification.TYPE_WARNING_MESSAGE);
	}

	/**
	 * This event handler shows an error message.
	 * 
	 * @param event
	 */
	@HandleSpringMvpEvent(eventScopes={EventScope.AllSpringMvpApplications, EventScope.SpringMvpApplication})
	public void onApplicationEvent(ShowErrorMessageEvent event) {
		showNotification(event, Notification.TYPE_ERROR_MESSAGE);
	}

	/**
	 * This event handler shows a message in the tray.
	 * 
	 * @param event
	 */
	@HandleSpringMvpEvent(eventScopes={EventScope.AllSpringMvpApplications, EventScope.SpringMvpApplication})
	public void onApplicationEvent(ShowTrayNotificationEvent event) {
		showNotification(event, Notification.TYPE_TRAY_NOTIFICATION);
	}

	@HandleSpringMvpEvent
	public void onApplicationClose(CloseSpringApplicationEvent event) {
		close();
	}

}
