/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher;

import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvents;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * The {@link DispatcherManager} handles multiple {@link SpringMvpDispatcher}s.
 * Multiple {@link SpringMvpDispatcher}s are needed if events shall be
 * dispatched in different "channels", i.e. a {@link SpringMvpEvent} needs only
 * to be dispatched in the Vaadin application or in all Vaadin applications.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface DispatcherManager {

	/**
	 * Sets the {@link SpringMvpDispatcher} which is used to dispatch events in
	 * a Vaadin application.
	 * 
	 * @param dispatcher
	 */
	void setApplicationDispatcher(SpringMvpDispatcher dispatcher);

	/**
	 * Sets the {@link SpringMvpDispatcher} which is used to dispatch events in
	 * all Vaadin applications.
	 * 
	 * @param dispatcher
	 */
	void setAllApplicationsDispatcher(SpringMvpDispatcher dispatcher);

	/**
	 * Adds a single dispatcher
	 * 
	 * @param dispatcher
	 * @param dispatcherName
	 */
	void addDispatcher(SpringMvpDispatcher dispatcher, String dispatcherName);

	/**
	 * Removes a dispatcher.
	 * 
	 * @param dispatcherName
	 *            the name which was used in "addDispatcher"
	 */
	void removeDispatcher(String dispatcherName);

	/**
	 * Gets a registered {@link SpringMvpDispatcher}
	 * 
	 * @param dispatcherName
	 * @return found dispatcher or null
	 */
	SpringMvpDispatcher getDispatcher(String dispatcherName);

	/**
	 * This method dispatches the given {@link SpringMvpEvent} using the added
	 * {@link SpringMvpDispatcher}s.
	 * 
	 * @param event
	 */
	void dispatchEvent(SpringMvpEvent event);

	/**
	 * Registers a listener. The {@link HandleSpringMvpEvent} and
	 * {@link HandleSpringMvpEvents} annotations and the eventScopes are used
	 * for registration.
	 * 
	 * @param eventListener
	 */
	void registerListener(Object eventListener);

}
