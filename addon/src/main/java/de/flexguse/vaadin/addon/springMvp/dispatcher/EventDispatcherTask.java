/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * This task executes all registered event listener methods for an event.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class EventDispatcherTask implements Runnable {

	private EventHandlersCaller eventHandlersCaller;
	private SpringMvpEvent springMvpEvent;

	Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * Standard constructor for the {@link EventDispatcherTask}.
	 * 
	 * @param eventHandlersCaller
	 *            the object containing all registered handlers and
	 *            handlermethods for the event
	 * @param springMvpEvent
	 *            the event which is used as argument for listener method
	 *            execution
	 */
	public EventDispatcherTask(EventHandlersCaller eventHandlersCaller,
			SpringMvpEvent springMvpEvent) {
		this.eventHandlersCaller = eventHandlersCaller;
		this.springMvpEvent = springMvpEvent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		if (eventHandlersCaller != null && springMvpEvent != null) {

			// call all listener methods
			eventHandlersCaller.callHandlerMethods(springMvpEvent);

		} else {
			logger.debug("EventHandlerCallers and/or SpringMvpEvent are null, no listener method executed");
		}

	}

}
