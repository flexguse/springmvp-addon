/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher;

import java.lang.reflect.Method;

import de.flexguse.vaadin.addon.springMvp.dispatcher.model.EventClassInfo;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.MethodCallInfo;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * An {@link EventHandlersCaller} manages and calls all EventHandlers for an
 * EventClass.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface EventHandlersCaller {

	/**
	 * Sets the eventClass for which this caller is used. This information is
	 * needed to be able to check if the registered methods can be called with
	 * this event.
	 * 
	 * @param springMvpEvent
	 */
	void setEventClassInfo(EventClassInfo eventClassInfo);

	/**
	 * Clears the list with EventHandlers.
	 */
	void cleanUp();

	/**
	 * Registers a method in an EventHandler.
	 * 
	 * @param eventHandler
	 *            the eventHandler object
	 * @param eventHandlerMethod
	 *            the method which is to be called
	 */
	void registerHandlerMethod(Object eventHandler, Method eventHandlerMethod);

	/**
	 * Registers a MethodCallInfo for an object.
	 * 
	 * @param eventHandler
	 * @param methodCallInfo
	 */
	void registerHandlerMethod(Object eventHandler,
			MethodCallInfo methodCallInfo);

	/**
	 * Unregisters all methods in the eventHandler.
	 * 
	 * @param eventHandler
	 */
	void unregisterHandler(Object eventHandler);

	/**
	 * Unregisters a specific method of the eventHandler.
	 * 
	 * @param eventHandler
	 * @param eventHandlerMethod
	 * @param eventClassInfo
	 *            the eventClassInfo for which the method was registered.
	 *            Important to be given if the method argument is
	 *            {@link SpringMvpEvent} and the method is regstered for
	 *            specific event.
	 */
	void unregisterHandlerMethod(Object eventHandler,
			Method eventHandlerMethod, EventClassInfo eventClassInfo);

	/**
	 * Calls all eventHandler methods registered in this
	 * {@link EventHandlersCaller}. If the eventHandler is no more available or
	 * the registered method is no more found, the eventHandler method is
	 * removed.
	 * 
	 * @param event
	 *            the event which is used as method argument for the
	 *            eventHandler method
	 */
	void callHandlerMethods(SpringMvpEvent event);

	/**
	 * 
	 * @return true if nothing is registered in this {@link EventHandlersCaller}
	 *         , else false
	 */
	boolean isEmpty();

}
