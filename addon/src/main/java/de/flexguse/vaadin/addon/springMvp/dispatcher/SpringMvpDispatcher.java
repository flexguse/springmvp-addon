/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher;

import java.lang.reflect.Method;

import de.flexguse.vaadin.addon.springMvp.dispatcher.model.EventMethodMap;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.MethodCallInfo;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface SpringMvpDispatcher {

	/**
	 * Registers an EventListener.
	 * 
	 * @param springMvpEventListener
	 *            the Object which contains annotated methods to handle
	 *            {@link SpringMvpEvent}s.
	 */
	void registerListener(Object springMvpEventListener);

	/**
	 * Unregisters an EventListener.
	 * 
	 * @param springMvpEventListener
	 *            the object which was previously an listener and which shall no
	 *            more listen to {@link SpringMvpEvent}s.
	 */
	void unregisterListener(Object springMvpEventListener);

	/**
	 * Unregisters a method in an eventListener for a specific event-class.
	 * 
	 * @param springMvpEventListener
	 * @param executionMethod
	 * @param springMvpEventClass
	 */
	void unregisterListener(Object springMvpEventListener,
			Method executionMethod,
			Class<? extends SpringMvpEvent> springMvpEventClass);

	/**
	 * Unregisters a method in an eventListener for a specific event. This
	 * method must be used if the {@link SpringMvpEvent} uses generics.
	 * 
	 * @param springMvpEventListener
	 * @param executionMethod
	 * @param springMvpEvent
	 */
	void unregisterListener(Object springMvpEventListener,
			Method executionMethod, SpringMvpEvent springMvpEvent);

	/**
	 * Dispatches the given event and calls all registered methods in the
	 * EventListener for the given {@link SpringMvpEvent}.
	 * 
	 * @param springMvpEvent
	 */
	void dispatchEvent(SpringMvpEvent springMvpEvent);

	/**
	 * Registers an specific method in an listener for an event-class.
	 * 
	 * @param springMvpEventListener
	 * @param executionMethod
	 * @param springMvpEventClass
	 */
	void registerListener(Object springMvpEventListener,
			Method executionMethod,
			Class<? extends SpringMvpEvent> springMvpEventClass);

	/**
	 * Registers an specific method in a listener for an event. This method must
	 * be used if the {@link SpringMvpEvent} uses generics.
	 * 
	 * @param springMvpEventListener
	 * @param executionMethod
	 * @param springMvpEvent
	 */
	void registerListener(Object springMvpEventListener,
			Method executionMethod, SpringMvpEvent springMvpEvent);

	/**
	 * Registers an specific method as listener for an event.
	 * 
	 * @param springMvpListener
	 * @param methodCallInfo
	 */
	void registerListener(Object springMvpListener,
			SpringMvpEvent springMvpEvent, MethodCallInfo methodCallInfo);

	/**
	 * Registeres multiple methods.
	 * 
	 * @param springMvpListener
	 * @param eventMethodMap
	 */
	void registerListenerMethods(Object springMvpListener,
			EventMethodMap eventMethodMap);

	/**
	 * This method closes the dispatcher and removes all remaining listeners.
	 */
	void close();

}
