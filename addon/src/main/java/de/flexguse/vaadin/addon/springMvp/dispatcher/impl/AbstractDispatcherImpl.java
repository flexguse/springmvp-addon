/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.impl;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.flexguse.vaadin.addon.springMvp.dispatcher.model.EventClassInfo;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.MethodCallInfo;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.util.SpringMvpHandlerUtil;

/**
 * Contains some helpful methods for logging.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public abstract class AbstractDispatcherImpl {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private SpringMvpHandlerUtil springMvpHandlerUtil;

	/**
	 * @return the springMvpHandlerUtil
	 */
	public SpringMvpHandlerUtil getSpringMvpHandlerUtil() {
		return springMvpHandlerUtil;
	}

	/**
	 * @param springMvpHandlerUtil
	 *            the springMvpHandlerUtil to set
	 */
	public void setSpringMvpHandlerUtil(
			SpringMvpHandlerUtil springMvpHandlerUtil) {
		this.springMvpHandlerUtil = springMvpHandlerUtil;
	}

	/**
	 * This helper method does the debug logging if debug loggin is enabled in
	 * the logger.
	 * 
	 * @param message
	 *            the message which is put into the log
	 */
	protected void logDebug(String message) {
		if (logger.isDebugEnabled()) {
			logger.debug(message);
		}
	}

	/**
	 * This helper method generates the absolute path of the method. Special
	 * feature is the ObjectID of the eventHandler is added.
	 * 
	 * @param eventHandler
	 * @param eventHandlerMethod
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected String getMethodInfo(Object eventHandler,
			Method eventHandlerMethod) {

		// add method properties
		Class<?>[] methodParameterTypes = eventHandlerMethod
				.getParameterTypes();

		if (methodParameterTypes != null && methodParameterTypes.length > 0) {

			try {
				Class<? extends SpringMvpEvent> methodParameterType = (Class<? extends SpringMvpEvent>) methodParameterTypes[0];

				return getMethodInfo(eventHandler,
						eventHandlerMethod.getName(), getSpringMvpHandlerUtil()
								.getEventClassInfo(methodParameterType));

			} catch (ClassCastException e) {
				logger.debug(
						"Unable to cast method argument to SpringMvpEvent", e);
			}

		}

		return "";

	}

	/**
	 * This helper method generates a debug string.
	 * 
	 * @param eventHandler
	 * @param methodName
	 * @param methodArgument
	 * @return
	 */
	private String getMethodInfo(Object eventHandler, String methodName,
			EventClassInfo eventClassInfo) {

		StringBuilder result = new StringBuilder();
		result.append(eventHandler).append(": ");
		result.append(methodName).append("(");

		// add method property
		if (eventClassInfo != null) {

			result.append(eventClassInfo.getEventClass().getName());
			if (eventClassInfo.getEventGenericClass() != null) {
				result.append("<")
						.append(eventClassInfo.getEventGenericClass().getName())
						.append(">");
			}

		}

		result.append(")");

		return result.toString();

	}

	/**
	 * This helper method generates the absolute path of the method.
	 * 
	 * @param eventHandler
	 * @param methodCallInfo
	 * @return
	 */
	protected String getMethodInfo(Object eventHandler,
			MethodCallInfo methodCallInfo) {

		return getMethodInfo(eventHandler, methodCallInfo.getMethodName(),
				methodCallInfo.getEventClassInfo());

	}

}
