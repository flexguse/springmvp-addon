/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.impl;

import java.util.Map;
import java.util.WeakHashMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;
import de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager;
import de.flexguse.vaadin.addon.springMvp.dispatcher.SpringMvpDispatcher;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.EventMethodMap;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.util.SpringMvpHandlerUtil;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class DispatcherManagerImpl implements DispatcherManager {

	private WeakHashMap<String, SpringMvpDispatcher> dispatcherMap = new WeakHashMap<String, SpringMvpDispatcher>();

	private Logger logger = LoggerFactory.getLogger(getClass());

	private SpringMvpHandlerUtil springMvpHandlerUtil;

	/**
	 * @param springMvpHandlerUtil
	 *            the springMvpHandlerUtil to set
	 */
	public void setSpringMvpHandlerUtil(
			SpringMvpHandlerUtil springMvpHandlerUtil) {
		this.springMvpHandlerUtil = springMvpHandlerUtil;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager#
	 * setApplicationDispatcher
	 * (de.flexguse.vaadin.addon.springMvp.dispatcher.SpringMvpDispatcher)
	 */
	@Override
	public void setApplicationDispatcher(SpringMvpDispatcher dispatcher) {
		if (checkDispatcher(dispatcher)) {
			dispatcherMap.put(EventScope.SpringMvpApplication, dispatcher);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager#
	 * setAllApplicationsDispatcher
	 * (de.flexguse.vaadin.addon.springMvp.dispatcher.SpringMvpDispatcher)
	 */
	@Override
	public void setAllApplicationsDispatcher(SpringMvpDispatcher dispatcher) {
		if (checkDispatcher(dispatcher)) {
			dispatcherMap.put(EventScope.AllSpringMvpApplications, dispatcher);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager#addDispatcher
	 * (de.flexguse.vaadin.addon.springMvp.dispatcher.SpringMvpDispatcher,
	 * java.lang.String)
	 */
	@Override
	public void addDispatcher(SpringMvpDispatcher dispatcher,
			String dispatcherName) {

		if (checkDispatcher(dispatcher)) {
			if (StringUtils.isBlank(dispatcherName)) {
				logger.error("empty dispatcher keys are not allowed, the dispatcher was not registered");
			} else {
				dispatcherMap.put(dispatcherName, dispatcher);
			}

		}

	}

	/**
	 * This helper method checks the given dispatcher.
	 * 
	 * @param dispatcher
	 * @return
	 */
	private boolean checkDispatcher(SpringMvpDispatcher dispatcher) {

		if (dispatcher == null) {
			logger.error("null SpringMvpDispatchers are not allowed");
			return false;
		}

		return true;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager#
	 * removeDispatcher(java.lang.String)
	 */
	@Override
	public void removeDispatcher(String dispatcherName) {
		if (StringUtils.isNotBlank(dispatcherName)) {
			dispatcherMap.remove(dispatcherName);
		} else {
			logger.error("Dispatchers with empty or null name can't be removed");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager#getDispatcher
	 * (java.lang.String)
	 */
	@Override
	public SpringMvpDispatcher getDispatcher(String dispatcherName) {

		if (StringUtils.isBlank(dispatcherName)) {
			logger.error("a Dispatcher with null-key is can't be registered");
			return null;
		}

		return dispatcherMap.get(dispatcherName);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager#dispatchEvent
	 * (de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent)
	 */
	@Override
	public void dispatchEvent(SpringMvpEvent event) {

		// get dispatcher for event scope
		SpringMvpDispatcher dispatcher = dispatcherMap.get(event
				.getEventScope());
		if (dispatcher == null) {
			logger.error("no SpringMvpDispatcher found for scope '"
					+ event.getEventScope()
					+ "'. The event was not dispatched.");
		} else {
			dispatcher.dispatchEvent(event);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager#
	 * registerListener
	 * (de.flexguse.vaadin.addon.springMvp.dispatcher.SpringMvpDispatcher)
	 */
	@Override
	public void registerListener(Object eventListener) {

		if (springMvpHandlerUtil != null) {

			EventMethodMap eventMethodMap = springMvpHandlerUtil
					.getEventMethodMap(eventListener);
			if (eventMethodMap != null) {
				Map<String, EventMethodMap> sortedByDispatcherKey = eventMethodMap
						.sortByDispatcherKeys();
				if (!sortedByDispatcherKey.isEmpty()) {

					for (String dispatcherKey : sortedByDispatcherKey.keySet()) {

						SpringMvpDispatcher dispatcher = dispatcherMap
								.get(dispatcherKey);
						if (dispatcher == null) {
							logger.error("no dispatcher with key '"
									+ dispatcherKey + "' registered");
						} else {

							dispatcher.registerListenerMethods(eventListener,
									sortedByDispatcherKey.get(dispatcherKey));

						}

					}

				}
			}

		} else {
			logger.error("no SpringMvpHandlerUtil, registration of eventListener can't be done");
		}

	}

}
