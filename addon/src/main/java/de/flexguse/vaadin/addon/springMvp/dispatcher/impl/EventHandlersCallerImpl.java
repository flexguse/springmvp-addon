/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.impl;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

import de.flexguse.vaadin.addon.springMvp.dispatcher.EventHandlersCaller;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.EventClassInfo;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.MethodCallInfo;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * This helper class contains all registered EventHandlers for an EventClass.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class EventHandlersCallerImpl extends AbstractDispatcherImpl implements
		EventHandlersCaller {

	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * <ul>
	 * <li>key: the eventHandler</li>
	 * <li>value: list of registered handler methods</li>
	 * </ul>
	 */
	private Map<Object, Set<MethodCallInfo>> eventHandlersRegistry;

	/**
	 * The event class the methods are registered for.
	 */
	private EventClassInfo eventClassInfo;

	@Override
	public void setEventClassInfo(EventClassInfo eventClassInfo) {
		this.eventClassInfo = eventClassInfo;

	}

	public EventHandlersCallerImpl() {
		eventHandlersRegistry = new WeakHashMap<Object, Set<MethodCallInfo>>();
	}

	@Override
	public void cleanUp() {
		eventHandlersRegistry.clear();
		logDebug("eventHandlerRegistry was cleaned up");
	}

	@Override
	public void registerHandlerMethod(Object eventHandler,
			Method eventHandlerMethod) {

		if (eventHandler != null && eventHandlerMethod != null) {

			// register the handler
			MethodCallInfo methodCallInfo = getSpringMvpHandlerUtil()
					.createMethodCallInfo(eventHandlerMethod, null);
			if (methodCallInfo != null) {
				registerHandlerMethod(eventHandler, methodCallInfo);
			} else {
				logger.debug("MethodCallInfo was not created, the eventHandler was not registered");
			}

		} else {
			logDebug("EventHandler and eventHandlerMethod must not be null. No eventHandlerMethod was registered.");
		}

	}

	/**
	 * This helper method registers a methodCallInfo for an eventHandler.
	 * 
	 * @param eventHandler
	 * @param methodCallInfo
	 */
	@Override
	public void registerHandlerMethod(Object eventHandler,
			MethodCallInfo methodCallInfo) {

		if (eventHandler != null && methodCallInfo != null) {

			/*
			 * when the checks passed the method is registered
			 */
			Set<MethodCallInfo> methodCallInfos = eventHandlersRegistry
					.get(eventHandler);
			if (methodCallInfos == null) {
				methodCallInfos = new HashSet<MethodCallInfo>();
				eventHandlersRegistry.put(eventHandler, methodCallInfos);
			}

			methodCallInfos.add(methodCallInfo);

			if (logger.isDebugEnabled()) {
				StringBuilder message = new StringBuilder();
				message.append(getMethodInfo(eventHandler, methodCallInfo))
						.append(" was registered as eventHandler for ")
						.append(eventClassInfo.toString());
				logDebug(message.toString());
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("EventHandler and/or MethodCallInfo are null, nothing was registered as eventHandler");
			}
		}

	}

	@Override
	public void unregisterHandler(Object eventHandler) {

		if (eventHandler != null) {

			Object removedHandler = eventHandlersRegistry.remove(eventHandler);

			if (logger.isDebugEnabled()) {

				if (removedHandler != null) {
					StringBuilder message = new StringBuilder();
					message.append(eventHandler)
							.append(" was unregistered as eventHandler for ")
							.append(eventClassInfo.toString());
					logDebug(message.toString());
				} else {
					StringBuilder message = new StringBuilder();
					message.append(eventHandler)
							.append(" could not be unregistered as eventHandler for ")
							.append(eventClassInfo.toString())
							.append(" because it was not registered.");
					logDebug(message.toString());

				}

			}
		} else {
			logDebug("null eventHandlers can't be unregistered");
		}
	}

	@Override
	public void unregisterHandlerMethod(Object eventHandler,
			Method eventHandlerMethod, EventClassInfo eventClassInfo) {

		if (eventHandler == null || eventHandlerMethod == null) {
			logDebug("eventHandler and eventHandlerMethod must not be null. Nothing was unregistered.");
			return;
		}

		Set<MethodCallInfo> methodCallInfos = eventHandlersRegistry
				.get(eventHandler);

		if (methodCallInfos == null) {

			if (logger.isDebugEnabled()) {
				StringBuilder message = new StringBuilder();
				message.append("The method ")
						.append(getMethodInfo(eventHandler, eventHandlerMethod))
						.append(" can't be unregistered because the eventhandler was not found in registry.");
				logger.debug(message.toString());
			}

			return;
		}

		/*
		 * It is not clear which method should be removed. A method with
		 * SpringMvpEvent or a specific SpringMvpEvent may be registered. Both
		 * methods needs to be removed.
		 */
		MethodCallInfo toBeRemoved = getSpringMvpHandlerUtil()
				.createMethodCallInfo(eventHandlerMethod, eventClassInfo);
		boolean removed = false;
		if (toBeRemoved != null) {
			removed = methodCallInfos.remove(toBeRemoved);
		}

		// cleanup the registry
		if (methodCallInfos.size() == 0) {
			eventHandlersRegistry.remove(eventHandler);
		}

		if (logger.isDebugEnabled()) {

			if (removed) {
				StringBuilder message = new StringBuilder();
				message.append("The method ");
				message.append(getMethodInfo(eventHandler, eventHandlerMethod))
						.append(" was unregistered for event ")
						.append(eventClassInfo.toString());
				logger.debug(message.toString());
			} else {
				StringBuilder message = new StringBuilder();
				message.append("The method ");
				message.append(getMethodInfo(eventHandler, eventHandlerMethod))
						.append(" was NOT unregistered for event ")
						.append(eventClassInfo.toString())
						.append(" because it was not found in the registry.");
				logger.debug(message.toString());
			}

		}

	}

	@Override
	public void callHandlerMethods(SpringMvpEvent event) {

		if (event == null) {
			if (logger.isDebugEnabled()) {
				logDebug("event is null, no eventHandlerMethods were called");
				return;
			}
		}

		/*
		 * Loop through map and call each registered method.
		 */
		Set<Object> mapKeys = eventHandlersRegistry.keySet();
		Iterator<Object> keyIterator = mapKeys.iterator();
		boolean handlerMethodsWereCalled = false;
		while (keyIterator.hasNext()) {
			Object eventHandler = keyIterator.next();

			Set<MethodCallInfo> methodCallInfos = eventHandlersRegistry
					.get(eventHandler);
			if (methodCallInfos != null) {

				Iterator<MethodCallInfo> methodCallIterator = methodCallInfos
						.iterator();
				while (methodCallIterator.hasNext()) {
					MethodCallInfo info = methodCallIterator.next();

					try {

						// get all methods in the eventHandler
						Method methodToCall = getSpringMvpHandlerUtil()
								.getDeclaredMethod(eventHandler,
										info.getMethodName(),
										info.getEventClassInfo());

						if (methodToCall == null) {
							methodToCall = getSpringMvpHandlerUtil().getDeclareMethod(eventHandler, info.getMethodName(), SpringMvpEvent.class);
						}
						ReflectionUtils.invokeMethod(methodToCall,
								eventHandler, event);
						handlerMethodsWereCalled = true;
						if (logger.isDebugEnabled()) {

							StringBuilder message = new StringBuilder();
							message.append("method ")
									.append(getMethodInfo(eventHandler,
											methodToCall))
									.append(" was invoked with ").append(event);
							logger.debug(message.toString());

						}
					}

					catch (SecurityException e) {
						methodCallIterator.remove();
						logger.error(
								"method in eventHandler can not be accessed due to security reasons. Method was unregistered.",
								e);
					}

				}

			}
		}

		if (!handlerMethodsWereCalled) {
			if (logger.isDebugEnabled()) {
				StringBuilder message = new StringBuilder();
				message.append("No eventHandlers found for event ").append(
						event.getClass().getName());
				logger.debug(message.toString());
			}
		}

	}

	/**
	 * Should be used for testing purposes only.
	 * 
	 * @return
	 */
	public Map<Object, Set<MethodCallInfo>> getEventHandlersRegistry() {
		return eventHandlersRegistry;
	}

	@Override
	public boolean isEmpty() {
		return eventHandlersRegistry.size() == 0;
	}

}
