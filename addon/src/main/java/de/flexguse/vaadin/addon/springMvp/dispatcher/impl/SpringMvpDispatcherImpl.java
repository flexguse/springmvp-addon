/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.impl;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.task.TaskExecutor;

import de.flexguse.vaadin.addon.springMvp.dispatcher.EventDispatcherTask;
import de.flexguse.vaadin.addon.springMvp.dispatcher.EventHandlersCaller;
import de.flexguse.vaadin.addon.springMvp.dispatcher.SpringMvpDispatcher;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.EventClassInfo;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.EventMethodMap;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.MethodCallInfo;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.MethodCallInfoList;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent.ExecutionType;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class SpringMvpDispatcherImpl extends AbstractDispatcherImpl implements
		SpringMvpDispatcher, ApplicationContextAware {

	Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * <ul>
	 * <li>key: the eventClassInfo containing the Class of the Event and - if
	 * present - the generic class of the event</li>
	 * <li>value: the eventHandlersCaller</li>
	 * </ul>
	 */
	private Map<EventClassInfo, EventHandlersCaller> eventCallerLookupMap;

	private TaskExecutor asyncTaskExecutor;
	private TaskExecutor syncTaskExecutor;

	private ApplicationContext springContext;

	/**
	 * Constructor for the dispatcher implementation
	 * 
	 * @param asyncTaskExecutor
	 *            the TaskExecutor which is used for asynchronous task execution
	 * @param syncTaskExecutor
	 *            the TaskExecutor which is used for synchronous task execution
	 * @param mvpUtil
	 *            the Util which is used for object and method introspection
	 */
	public SpringMvpDispatcherImpl(TaskExecutor asyncTaskExecutor,
			TaskExecutor syncTaskExecutor) {

		if (asyncTaskExecutor == null) {
			throw new RuntimeException(
					"TaskExecutor for asynchronous event dispatching must be given");
		}

		if (syncTaskExecutor == null) {
			throw new RuntimeException(
					"TaskExecutor for synchronous event dispatching must be given");
		}

		this.asyncTaskExecutor = asyncTaskExecutor;
		this.syncTaskExecutor = syncTaskExecutor;

		eventCallerLookupMap = new HashMap<EventClassInfo, EventHandlersCaller>();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		springContext = applicationContext;

	}

	@Override
	public void registerListener(Object springMvpEventListener) {

		if (springMvpEventListener != null) {

			EventMethodMap eventMethodMap = getSpringMvpHandlerUtil()
					.getEventMethodMap(springMvpEventListener);

			if (eventMethodMap != null) {

				Set<EventClassInfo> eventClassInfos = eventMethodMap.getKeys();
				if (eventClassInfos != null) {
					for (EventClassInfo eventClassInfo : eventClassInfos) {

						MethodCallInfoList methodCallInfoList = eventMethodMap
								.get(eventClassInfo);
						if (methodCallInfoList != null) {
							for (MethodCallInfo methodCallInfo : methodCallInfoList
									.getMethodCallInfos()) {
								registerListener(springMvpEventListener,
										eventClassInfo, methodCallInfo);
							}
						}
					}
				}

			} else {
				if (logger.isDebugEnabled()) {
					logger.debug("No annotated event handler methods found in "
							+ springMvpEventListener);
				}
			}

		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("Null objects can't be registered as event listeners.");
			}
		}

	}

	/**
	 * This helper method registeres a springMvpEventListener using
	 * eventClassInfo and methodCallInfo.
	 * 
	 * @param springMvpEventListener
	 * @param eventClassInfo
	 * @param methodCallInfo
	 */
	private void registerListener(Object springMvpEventListener,
			EventClassInfo eventClassInfo, MethodCallInfo methodCallInfo) {

		if (springMvpEventListener != null && eventClassInfo != null
				&& methodCallInfo != null) {

			EventHandlersCaller eventHandlersCaller = eventCallerLookupMap
					.get(eventClassInfo);

			if (eventHandlersCaller == null) {
				eventHandlersCaller = springContext
						.getBean(EventHandlersCaller.class);
				eventHandlersCaller.setEventClassInfo(eventClassInfo);
				eventCallerLookupMap.put(eventClassInfo, eventHandlersCaller);
			}

			eventHandlersCaller.registerHandlerMethod(springMvpEventListener,
					methodCallInfo);

		} else {
			logger.error("EventListener object, eventClassInfo and event methodCallInfo must not be null, nothing was registered");
		}
	}

	@Override
	public void dispatchEvent(SpringMvpEvent springMvpEvent) {

		if (springMvpEvent == null) {
			logger.error("Null events can not be dispatched.");
			return;
		}

		EventClassInfo eventClassInfo = getSpringMvpHandlerUtil().getEventClassInfo(springMvpEvent);
		EventHandlersCaller eventHandlerCallers = eventCallerLookupMap
				.get(eventClassInfo);

		if (eventHandlerCallers == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("No EventHandlersCaller registered for "
						+ springMvpEvent.getClass().getSimpleName()
						+ ". No handler methods were executed.");

			}
			return;

		}

		EventDispatcherTask eventDispatcherTask = new EventDispatcherTask(
				eventHandlerCallers, springMvpEvent);

		if (ExecutionType.SYNC.equals(springMvpEvent.getExecutionType())) {
			syncTaskExecutor.execute(eventDispatcherTask);
		} else {
			asyncTaskExecutor.execute(eventDispatcherTask);
		}

	}

	@Override
	public void registerListener(Object springMvpEventListener,
			Method executionMethod,
			Class<? extends SpringMvpEvent> springMvpEventClass) {

		if (springMvpEventListener == null) {
			logger.error("Unable to register null listener");
			return;
		}

		if (executionMethod == null) {
			logger.error("Unable to register null method in event listener");
			return;
		}

		if (springMvpEventClass == null) {
			logger.error("Unable to register listener for null event");
			return;
		}

		registerListener(springMvpEventListener, executionMethod,
				new EventClassInfo(springMvpEventClass));

	}

	@Override
	public void registerListener(Object springMvpListener,
			SpringMvpEvent springMvpEvent, MethodCallInfo methodCallInfo) {

		if (springMvpListener != null && methodCallInfo != null) {

			registerListener(springMvpListener, getSpringMvpHandlerUtil()
					.getEventClassInfo(springMvpEvent), methodCallInfo);

		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("SpringMvpListener and/or MethodCallInfo are null, nothing was registered as listener method");
			}
		}

	}

	@Override
	public void unregisterListener(Object springMvpEventListener,
			Method executionMethod,
			Class<? extends SpringMvpEvent> springMvpEventClass) {

		unregisterListener(springMvpEventListener, executionMethod,
				getSpringMvpHandlerUtil()
						.getEventClassInfo(springMvpEventClass));

	}

	@Override
	public void unregisterListener(Object springMvpEventListener) {

		if (springMvpEventListener != null) {

			// loop through all EventCallerHandlers and unregister listener
			EventMethodMap eventMethodMap = getSpringMvpHandlerUtil()
					.getEventMethodMap(springMvpEventListener);

			if (!eventMethodMap.isEmpty()) {

				Iterator<EventClassInfo> iterator = eventMethodMap.getKeys()
						.iterator();
				while (iterator.hasNext()) {

					EventClassInfo eventClassInfo = iterator.next();
					MethodCallInfoList methodCallInfoList = eventMethodMap
							.get(eventClassInfo);
					if (methodCallInfoList != null
							&& methodCallInfoList.getMethodCallInfos() != null) {
						for (MethodCallInfo methodCallInfo : methodCallInfoList
								.getMethodCallInfos()) {
							Method methodToUnregister = getSpringMvpHandlerUtil()
									.getDeclaredMethod(springMvpEventListener,
											methodCallInfo.getMethodName(),
											eventClassInfo);
							if (methodToUnregister != null) {
								unregisterListener(springMvpEventListener,
										methodToUnregister, eventClassInfo);
							} else {
								StringBuilder message = new StringBuilder();
								message.append("The method ")
										.append(methodCallInfo.getMethodName())
										.append(" was not found in the ventListener ")
										.append(springMvpEventListener
												.getClass().getName())
										.append(". No unregistering was done for this method.");

								logger.error(message.toString());
							}

						}
					}

				}
			}

		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("Null objects can't be unregistered as event listeners.");
			}
		}

	}

	/**
	 * This method does the unregistering.
	 * 
	 * @param springMvpListener
	 *            the EventListener
	 * @param executionMethod
	 *            the method in the EventListener which shall no more to be
	 *            called
	 * @param eventClassInfo
	 *            the information about the event.
	 */
	private void unregisterListener(Object springMvpEventListener,
			Method executionMethod, EventClassInfo eventClassInfo) {

		EventHandlersCaller eventHandlersCaller = eventCallerLookupMap
				.get(eventClassInfo);
		if (eventHandlersCaller != null) {
			eventHandlersCaller.unregisterHandlerMethod(springMvpEventListener,
					executionMethod, eventClassInfo);
			if (eventHandlersCaller.isEmpty()) {
				eventCallerLookupMap.remove(eventClassInfo);
				if (logger.isDebugEnabled()) {
					StringBuilder message = new StringBuilder();
					message.append("eventHandlersCaller for ")
							.append(eventClassInfo.toString())
							.append(" is empty and was removed");
					logger.debug(message.toString());
				}
			}
		} else {
			if (logger.isDebugEnabled()) {
				StringBuilder message = new StringBuilder();
				message.append("No EventHandlersCaller found for event ");
				if (eventClassInfo != null) {
					message.append(eventClassInfo.toString());
				} else {
					message.append(" null ");
				}
				message.append(". Nothing was unregistered.");
			}
		}

	}

	/**
	 * This method does the registering of the eventListener for an event.
	 * 
	 * @param springMvpEventListener
	 * @param executionMethod
	 * @param eventClassInfo
	 */
	private void registerListener(Object springMvpEventListener,
			Method executionMethod, EventClassInfo eventClassInfo) {

		if (springMvpEventListener != null && executionMethod != null
				&& eventClassInfo != null) {

			registerListener(
					springMvpEventListener,
					eventClassInfo,
					getSpringMvpHandlerUtil().createMethodCallInfo(
							executionMethod, eventClassInfo));

		} else {
			logger.error("EventListener object, methodName and eventClassInfo must not be null, nothing was registered");
		}

	}

	@Override
	public void unregisterListener(Object springMvpEventListener,
			Method executionMethod, SpringMvpEvent springMvpEvent) {

		if (springMvpEvent != null) {

			unregisterListener(springMvpEventListener, executionMethod,
					getSpringMvpHandlerUtil().getEventClassInfo(springMvpEvent));

		}

	}

	@Override
	public void close() {

		for (EventHandlersCaller eventHandlerCaller : eventCallerLookupMap
				.values()) {
			eventHandlerCaller.cleanUp();
		}
		eventCallerLookupMap.clear();
		logger.info("unregistered all listeners");

	}

	/**
	 * Should only be used for testing purposes.
	 * 
	 * @return
	 */
	public Map<EventClassInfo, EventHandlersCaller> getEventCallerLookupMap() {
		return eventCallerLookupMap;
	}

	@Override
	public void registerListenerMethods(Object springMvpListener,
			EventMethodMap eventMethodMap) {

		if (springMvpListener != null && eventMethodMap != null) {
			for (EventClassInfo eventClassInfo : eventMethodMap.getKeys()) {

				MethodCallInfoList methodCallInfoList = eventMethodMap
						.get(eventClassInfo);
				if (methodCallInfoList != null) {
					for (MethodCallInfo methodCallInfo : methodCallInfoList
							.getMethodCallInfos()) {

						registerListener(springMvpListener, eventClassInfo,
								methodCallInfo);
					}
				}
			}
		}
	}

	@Override
	public void registerListener(Object springMvpEventListener,
			Method executionMethod, SpringMvpEvent springMvpEvent) {

		registerListener(springMvpEventListener, executionMethod,
				getSpringMvpHandlerUtil().getEventClassInfo(springMvpEvent));

	}
}
