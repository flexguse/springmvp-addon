/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.model;

import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * <p>
 * This class contains information about an Event class. If an custom
 * {@link SpringMvpEvent} implementation using Java Generics is used, there is
 * no way to get the Generics information in runtime.
 * </p>
 * <p>
 * Therefore this class was created to hold the EventClass and the Generics
 * class information.
 * </p>
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class EventClassInfo {

	private Class<? extends SpringMvpEvent> eventClass;
	private Class<?> eventGenericClass;

	public EventClassInfo() {
	}

	public EventClassInfo(Class<? extends SpringMvpEvent> eventClass) {
		this.eventClass = eventClass;
	}

	public EventClassInfo(Class<? extends SpringMvpEvent> eventClass,
			Class<?> eventGenericClass) {
		this.eventClass = eventClass;
		this.eventGenericClass = eventGenericClass;
	}

	/**
	 * @return the eventClass
	 */
	public Class<? extends SpringMvpEvent> getEventClass() {
		return eventClass;
	}

	/**
	 * @param eventClass
	 *            the eventClass to set
	 */
	public void setEventClass(Class<? extends SpringMvpEvent> eventClass) {
		this.eventClass = eventClass;
	}

	/**
	 * @return the eventGenericClass
	 */
	public Class<?> getEventGenericClass() {
		return eventGenericClass;
	}

	/**
	 * @param eventGenericClass
	 *            the eventGenericClass to set
	 */
	public void setEventGenericClass(Class<?> eventGenericClass) {
		this.eventGenericClass = eventGenericClass;
	}

	@Override
	public String toString() {

		StringBuilder string = new StringBuilder();
		if (eventClass != null) {
			string.append(eventClass.getName());
		}
		if (eventGenericClass != null) {
			string.append("<").append(eventGenericClass.getName()).append(">");
		}

		return string.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((eventClass == null) ? 0 : eventClass.hashCode());
		result = prime
				* result
				+ ((eventGenericClass == null) ? 0 : eventGenericClass
						.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof EventClassInfo)) {
			return false;
		}
		EventClassInfo other = (EventClassInfo) obj;
		if (eventClass == null) {
			if (other.eventClass != null) {
				return false;
			}
		} else if (!eventClass.equals(other.eventClass)) {
			return false;
		}
		if (eventGenericClass == null) {
			if (other.eventGenericClass != null) {
				return false;
			}
		} else if (!eventGenericClass.equals(other.eventGenericClass)) {
			return false;
		}
		return true;
	}

}
