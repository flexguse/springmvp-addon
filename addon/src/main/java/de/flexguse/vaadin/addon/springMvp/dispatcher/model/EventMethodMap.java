/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * The {@link EventMethodMap} contains all methods ordered by the EventClass for
 * an {@link SpringMvpEvent}Listener.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class EventMethodMap {

	private Map<EventClassInfo, MethodCallInfoList> eventListenerMethodMap = new HashMap<EventClassInfo, MethodCallInfoList>();

	/**
	 * This method adds a list of {@link MethodCallInfo} for an event class
	 * name.
	 * 
	 * @param eventClassInfo
	 * @param methodCallInfoList
	 */
	public void addEventListeners(EventClassInfo eventClassInfo,
			MethodCallInfoList methodCallInfoList) {

		if (eventClassInfo != null && methodCallInfoList != null) {

			// check if there already is an entry for the eventClassName
			MethodCallInfoList existingMethodCallInfoList = eventListenerMethodMap
					.get(eventClassInfo);
			if (existingMethodCallInfoList == null) {
				eventListenerMethodMap.put(eventClassInfo, methodCallInfoList);
			} else {
				existingMethodCallInfoList.addAll(methodCallInfoList);
			}

		}

	}

	/**
	 * This method removes all {@link MethodCallInfo}s in the
	 * {@link MethodCallInfoList} for the given event class name.
	 * 
	 * @param eventClassInfo
	 * @param methodCallInfoList
	 */
	public void removeEventListeners(EventClassInfo eventClassInfo,
			MethodCallInfoList methodCallInfoList) {

		if (eventClassInfo != null && methodCallInfoList != null) {

			MethodCallInfoList existingMethodCallInfoList = eventListenerMethodMap
					.get(eventClassInfo);
			if (existingMethodCallInfoList != null) {
				existingMethodCallInfoList
						.removeMethodCallInfos(methodCallInfoList);
			}

		}

	}

	/**
	 * Removes a single {@link MethodCallInfo} for the given eventClassName.
	 * 
	 * @param eventClassInfo
	 * @param methodCallInfo
	 */
	public void removeEventListener(EventClassInfo eventClassInfo,
			MethodCallInfo methodCallInfo) {

		if (eventClassInfo != null && methodCallInfo != null) {
			MethodCallInfoList existingMethodCallInfoList = eventListenerMethodMap
					.get(eventClassInfo);
			if (existingMethodCallInfoList != null) {
				existingMethodCallInfoList.removeMethodCallInfo(methodCallInfo);
				if (existingMethodCallInfoList.isEmpty()) {
					eventListenerMethodMap.remove(eventClassInfo);
				}
			}
		}

	}

	/**
	 * Adds a single {@link MethodCallInfo} for an event class name.
	 * 
	 * @param eventClassInfo
	 * @param methodCallInfo
	 */
	public void addEventListener(EventClassInfo eventClassInfo,
			MethodCallInfo methodCallInfo) {

		addEventListeners(eventClassInfo,
				new MethodCallInfoList(methodCallInfo));

	}

	/**
	 * 
	 * @return all event names registered in this map
	 */
	public Set<EventClassInfo> getKeys() {
		return eventListenerMethodMap.keySet();
	}

	/**
	 * Gets the registered {@link MethodCallInfoList} for the given
	 * eventClassName.
	 * 
	 * @param eventClassInfo
	 * @return null if not found, else {@link MethodCallInfoList}
	 */
	public MethodCallInfoList get(EventClassInfo eventClassInfo) {
		if (eventClassInfo != null) {
			return eventListenerMethodMap.get(eventClassInfo);
		}
		return null;
	}

	/**
	 * Checks if nothing is registered.
	 * 
	 * @return true if empty, else false
	 */
	public boolean isEmpty() {

		return eventListenerMethodMap.isEmpty();

	}

	/**
	 * This method splits the map and resorts the contents by dispatcher key (or
	 * EventScope).
	 */
	public Map<String, EventMethodMap> sortByDispatcherKeys() {

		Map<String, EventMethodMap> result = new HashMap<String, EventMethodMap>();

		// loop through contents
		for (EventClassInfo eventClassInfo : getKeys()) {

			MethodCallInfoList methodCallInfoList = get(eventClassInfo);
			for (MethodCallInfo methodCallInfo : methodCallInfoList
					.getMethodCallInfos()) {
				List<String> dispatcherKeys = methodCallInfo
						.getDispatcherKeys();
				for (String dispatcherKey : dispatcherKeys) {
					EventMethodMap eventMethodMap = result.get(dispatcherKey);

					if (eventMethodMap == null) {
						eventMethodMap = new EventMethodMap();
						result.put(dispatcherKey, eventMethodMap);
					}

					eventMethodMap.addEventListener(eventClassInfo,
							methodCallInfo);
				}
			}

		}

		return result;
	}

}
