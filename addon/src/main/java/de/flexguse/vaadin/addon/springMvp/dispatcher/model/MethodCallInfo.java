/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.model;

import java.util.List;

/**
 * This object contains all information needed to call a method on an Object.
 * 
 * It is designed to be able to hold information for an 1 argument method.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class MethodCallInfo {

	private String methodName;
	private EventClassInfo eventClassInfo;
	private List<String> dispatcherKeys;

	public String getMethodName() {
		return methodName;
	}

	/**
	 * @return the dispatcherKeys
	 */
	public List<String> getDispatcherKeys() {
		return dispatcherKeys;
	}
	
	/**
	 * @return the eventClassInfo
	 */
	public EventClassInfo getEventClassInfo() {
		return eventClassInfo;
	}

	public MethodCallInfo(String methodName, EventClassInfo eventClassInfo, List<String> dispatcherKeys) {

		this.methodName = methodName;
		this.eventClassInfo = eventClassInfo;
		this.dispatcherKeys = dispatcherKeys;

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dispatcherKeys == null) ? 0 : dispatcherKeys.hashCode());
		result = prime * result
				+ ((eventClassInfo == null) ? 0 : eventClassInfo.hashCode());
		result = prime * result
				+ ((methodName == null) ? 0 : methodName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof MethodCallInfo)) {
			return false;
		}
		MethodCallInfo other = (MethodCallInfo) obj;
		if (dispatcherKeys == null) {
			if (other.dispatcherKeys != null) {
				return false;
			}
		} else if (!dispatcherKeys.equals(other.dispatcherKeys)) {
			return false;
		}
		if (eventClassInfo == null) {
			if (other.eventClassInfo != null) {
				return false;
			}
		} else if (!eventClassInfo.equals(other.eventClassInfo)) {
			return false;
		}
		if (methodName == null) {
			if (other.methodName != null) {
				return false;
			}
		} else if (!methodName.equals(other.methodName)) {
			return false;
		}
		return true;
	}


	
}
