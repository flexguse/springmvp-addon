/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.model;

import java.util.ArrayList;
import java.util.List;

/**
 * The MethodCallInfoList contains several {@link MethodCallInfo} objects. The
 * {@link MethodCallInfoList} is usually used to collect multiple method calls
 * for an event.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class MethodCallInfoList {

	private List<MethodCallInfo> methodCallInfos = new ArrayList<MethodCallInfo>();

	public List<MethodCallInfo> getMethodCallInfos() {
		return methodCallInfos;
	}

	public void setMethodCallInfos(List<MethodCallInfo> methodCallInfos) {
		this.methodCallInfos = methodCallInfos;
	}

	public MethodCallInfoList() {
	}

	public MethodCallInfoList(MethodCallInfo methodCallInfo) {
		if (methodCallInfo != null) {
			methodCallInfos.add(methodCallInfo);
		}
	}

	/**
	 * Adds all given {@link MethodCallInfo} objects.
	 * 
	 * @param methodCallInfoList
	 */
	public void addAll(MethodCallInfoList methodCallInfoList) {
		if (methodCallInfoList != null) {
			methodCallInfos.addAll(methodCallInfoList.getMethodCallInfos());
		}
	}

	/**
	 * Adds a single {@link MethodCallInfo} to the list.
	 * 
	 * @param methodCallInfo
	 */
	public void addMethodCallInfo(MethodCallInfo methodCallInfo) {
		if (methodCallInfo != null) {
			methodCallInfos.add(methodCallInfo);
		}

	}

	/**
	 * Removes a {@link MethodCallInfo} from the list.
	 * 
	 * @param methodCallInfo
	 */
	public void removeMethodCallInfo(MethodCallInfo methodCallInfo) {
		if (methodCallInfo != null) {
			methodCallInfos.remove(methodCallInfo);
		}
	}

	/**
	 * Removes all {@link MethodCallInfo}s in the given
	 * {@link MethodCallInfoList}.
	 * 
	 * @param methodCallInfoList
	 */
	public void removeMethodCallInfos(MethodCallInfoList methodCallInfoList) {

		if (methodCallInfoList != null) {
			methodCallInfos.removeAll(methodCallInfoList.getMethodCallInfos());
		}

	}

	/**
	 * Checks if the list is empty.
	 * 
	 * @return true if no {@link MethodCallInfo} objects are contained, else
	 *         false
	 */
	public boolean isEmpty() {
		return methodCallInfos.isEmpty();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((methodCallInfos == null) ? 0 : methodCallInfos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MethodCallInfoList other = (MethodCallInfoList) obj;
		if (methodCallInfos == null) {
			if (other.methodCallInfos != null)
				return false;
		} else if (!methodCallInfos.equals(other.methodCallInfos))
			return false;
		return true;
	}

}
