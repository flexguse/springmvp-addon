/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.events;

/**
 * Dispatch this event if you want to close the application including the spring
 * context.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class CloseSpringApplicationEvent extends SpringMvpEvent {

	private static final long serialVersionUID = -6987276326913703329L;

	public CloseSpringApplicationEvent(Object eventSource) {
		super(eventSource, null);
	}

}
