/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.events;

import java.io.Serializable;
import java.lang.ref.WeakReference;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;

/**
 * This abstract class is the base for all events dispatched in the SpringMvp.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@SuppressWarnings("serial")
public abstract class SpringMvpEvent implements Serializable {

	/**
	 * The Object which dispatched the event.
	 */
	private WeakReference<Object> source;

	private ExecutionType executionType = ExecutionType.SYNC;

	private String eventScope = EventScope.SpringMvpApplication;

	private Class<?> genericType;

	/**
	 * Default constructor.
	 * 
	 * @param eventSource
	 *            the object which dispatched the event.
	 */
	public SpringMvpEvent(Object eventSource, Class<?> genericType) {
		source = new WeakReference<Object>(eventSource);
		this.genericType = genericType;
	}

	/**
	 * @return the executionType
	 */
	public ExecutionType getExecutionType() {
		return executionType;
	}

	/**
	 * @param executionType
	 *            the executionType to set
	 */
	public void setExecutionType(ExecutionType executionType) {
		this.executionType = executionType;
	}

	/**
	 * @return the source
	 */
	public Object getSource() {

		return source.get();
	}

	/**
	 * @return the eventScope
	 */
	public String getEventScope() {
		return eventScope;
	}

	/**
	 * @param eventScope
	 *            the eventScope to set
	 */
	public void setEventScope(String eventScope) {
		this.eventScope = eventScope;
	}

	/**
	 * @return the genericType
	 */
	public Class<?> getGenericType() {
		return genericType;
	}

	/**
	 * ExecutionTypes for {@link SpringMvpEvent}s.
	 * 
	 * @author Christoph Guse, info@flexguse.de
	 * 
	 */
	public enum ExecutionType {
		/**
		 * The event is executed asynchronously. This means the called method
		 * returns immediately and does not wait for execution of the inner
		 * code. Useful in cases where the execution of a method takes some time
		 * and the execution can be done in the background.
		 */
		ASYNC,
		/**
		 * The event is executed synchronously. This is the default behavior of
		 * {@link SpringMvpEvent}.
		 */
		SYNC
	}

}
