/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.events.messages;

import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;


/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public abstract class AbstractMessageEvent extends SpringMvpEvent {

    private static final long serialVersionUID = 2711179313136698673L;

    protected String title;
    protected String info;
        
    public AbstractMessageEvent(Object source, String title, String info) {
	super(source, null);

	this.title = title;
	this.info = info;
    }
    
    /**
     * @return the title
     */
    public String getTitle() {
	return title;
    }

    /**
     * @return the info
     */
    public String getInfo() {
	return info;
    }
}
