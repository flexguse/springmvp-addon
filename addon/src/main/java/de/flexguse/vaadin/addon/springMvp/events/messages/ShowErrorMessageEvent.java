/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.events.messages;

import de.flexguse.vaadin.addon.springMvp.application.SpringMvpVaadinApplication;

/**
 * Dispatch this event in your {@link SpringMvpVaadinApplication} application
 * and an Errormessage is shown. The listener for this event is implemented in
 * {@link SpringMvpVaadinApplication}.
 * 
 * <img src="doc-files/notification-error.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ShowErrorMessageEvent extends AbstractMessageEvent {

	private static final long serialVersionUID = 2170387508724218738L;

	public ShowErrorMessageEvent(Object source, String title, String info) {
		super(source, title, info);
	}

}
