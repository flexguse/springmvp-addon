/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.events.messages;

import de.flexguse.vaadin.addon.springMvp.application.SpringMvpVaadinApplication;

/**
 * Dispatch this event in your {@link SpringMvpVaadinApplication} application
 * and an Message is shown. The listener for this event is implemented in
 * {@link SpringMvpVaadinApplication}.
 * 
 * <img src="doc-files/notification-humanized.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ShowHumanizedMessageEvent extends AbstractMessageEvent {

	private static final long serialVersionUID = 6394518306553311894L;

	public ShowHumanizedMessageEvent(Object source, String title, String info) {
		super(source, title, info);
	}

}
