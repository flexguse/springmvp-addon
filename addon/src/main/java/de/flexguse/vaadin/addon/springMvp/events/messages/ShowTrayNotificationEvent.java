/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.events.messages;

import de.flexguse.vaadin.addon.springMvp.application.SpringMvpVaadinApplication;

/**
 * Dispatch this event in your {@link SpringMvpVaadinApplication} application
 * and a Tray notification is shown. The listener for this event is implemented
 * in {@link SpringMvpVaadinApplication}.
 * 
 * <img src="doc-files/notification-tray.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ShowTrayNotificationEvent extends AbstractMessageEvent {

	private static final long serialVersionUID = -441202327905116998L;

	public ShowTrayNotificationEvent(Object source, String title, String info) {
		super(source, title, info);
	}

}
