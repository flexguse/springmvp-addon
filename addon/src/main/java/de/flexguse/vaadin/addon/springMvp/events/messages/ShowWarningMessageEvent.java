/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.events.messages;

import de.flexguse.vaadin.addon.springMvp.application.SpringMvpVaadinApplication;

/**
 * Dispatch this event in your {@link SpringMvpVaadinApplication} application
 * and a Warningmessage is shown. The listener for this event is implemented in
 * {@link SpringMvpVaadinApplication}.
 * 
 * <img src="doc-files/notification-warning.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ShowWarningMessageEvent extends AbstractMessageEvent {

	private static final long serialVersionUID = 2391709552503908070L;

	public ShowWarningMessageEvent(Object source, String title, String info) {
		super(source, title, info);
	}

}
