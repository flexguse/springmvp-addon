/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.events.model;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * Dispatch this event if a model was added in your application and every Vaadin
 * application needs to be notified.
 * <p>
 * The default eventScope is set to AllSpringMvpApplications and the
 * executionType is set to ASYNC so the dispatching of this event does not slow
 * down the UI responsiveness.
 * </p>
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ModelWasAddedEvent<T> extends SpringMvpEvent {

	private static final long serialVersionUID = -979637149184275909L;

	private T addedModel;

	/**
	 * @return the addedModel
	 */
	public T getAddedModel() {
		return addedModel;
	}

	/**
	 * @param addedModel
	 *            the addedModel to set
	 */
	public void setAddedModel(T addedModel) {
		this.addedModel = addedModel;
	}

	public ModelWasAddedEvent(Object eventSource, Class<?> genericType) {
		super(eventSource, genericType);

		setEventScope(EventScope.AllSpringMvpApplications);
		setExecutionType(ExecutionType.ASYNC);
	}

}
