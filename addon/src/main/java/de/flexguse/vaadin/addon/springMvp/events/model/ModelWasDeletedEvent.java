/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.events.model;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * Dispatch this event if a model was updated in your application and every
 * Vaadin application needs to be notified.
 * <p>
 * The default eventScope is set to AllSpringMvpApplications and the
 * executionType is set to ASYNC so the dispatching of this event does not slow
 * down the UI responsiveness.
 * </p>
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ModelWasDeletedEvent<T> extends SpringMvpEvent {

	private static final long serialVersionUID = -6682641942015366218L;

	private T deletedModel;

	/**
	 * @return the deletedModel
	 */
	public T getDeletedModel() {
		return deletedModel;
	}

	/**
	 * @param deletedModel
	 *            the deletedModel to set
	 */
	public void setDeletedModel(T deletedModel) {
		this.deletedModel = deletedModel;
	}

	public ModelWasDeletedEvent(Object eventSource, Class<T> modelClass) {
		super(eventSource, modelClass);
		
		setEventScope(EventScope.AllSpringMvpApplications);
		setExecutionType(ExecutionType.ASYNC);
	}

}
