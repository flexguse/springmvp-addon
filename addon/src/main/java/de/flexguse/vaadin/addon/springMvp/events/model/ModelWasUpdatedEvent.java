/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.events.model;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * Dispatch this event if a model was updated in your application and every
 * Vaadin application needs to be notified.
 * <p>
 * The default eventScope is set to AllSpringMvpApplications and the
 * executionType is set to ASYNC so the dispatching of this event does not slow
 * down the UI responsiveness.
 * </p>
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ModelWasUpdatedEvent<T> extends SpringMvpEvent {

	private static final long serialVersionUID = 2403623889140853795L;

	private T updatedModel;

	/**
	 * @return the updatedModel
	 */
	public T getUpdatedModel() {
		return updatedModel;
	}

	/**
	 * @param updatedModel
	 *            the updatedModel to set
	 */
	public void setUpdatedModel(T updatedModel) {
		this.updatedModel = updatedModel;
	}

	public ModelWasUpdatedEvent(Object eventSource, Class<T> modelClass) {
		super(eventSource, modelClass);

		setEventScope(EventScope.AllSpringMvpApplications);
		setExecutionType(ExecutionType.ASYNC);
	}

}
