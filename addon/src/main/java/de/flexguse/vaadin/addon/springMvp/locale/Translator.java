/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.locale;

import java.util.Locale;

import org.springframework.context.support.AbstractMessageSource;

/**
 * Method to handle ResourceBundles in a convenient way.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface Translator {

	/**
	 * Sets the message source which contains the ResourceBundles.
	 * 
	 * @param messageSource
	 */
	void setMessageSource(AbstractMessageSource messageSource);

	/**
	 * Sets the locale which is used to get the translations. The default locale
	 * is Locale.ENGLISH.
	 * 
	 * @param locale
	 */
	void setLocale(Locale locale);

	/**
	 * Sets the language which is used to get the translations. The default
	 * language is en.
	 * 
	 * @param language
	 *            like en, de, it etc.
	 */
	void setLanguage(String language);

	/**
	 * Gets a translation for the given key.
	 * 
	 * @param key
	 *            the key in the resource bundle
	 * @return the translation or an message the translation was not found
	 */
	String getTranslation(String key);

	/**
	 * Gets a translation for the given key filled with the given value.
	 * 
	 * @param key
	 *            the key in the resource bundle
	 * @param values
	 *            the values which are put into the {0}... placeholders
	 * @return the translation or an message the translation was not found
	 */
	String getTranslation(String key, Object[] values);

	/**
	 * 
	 * @return the locale which is used for looking for translations
	 */
	Locale getUsedLocale();

}
