/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.locale.impl;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractMessageSource;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * Implementation of {@link Translator} which uses Spring
 * {@link AbstractMessageSource}.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class TranslatorImpl implements Translator {

	public static final String MESSAGESOURCE_NOT_SET = "messagesource not set";
	private AbstractMessageSource messageSource;
	private Locale usedLocale = Locale.ENGLISH;

	private Logger logger = LoggerFactory.getLogger(getClass());

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.vaadin.addon.springMvp.locale.Translator#setMessageSource
	 * (org.springframework.context.support.AbstractMessageSource)
	 */
	@Override
	public void setMessageSource(AbstractMessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.vaadin.addon.springMvp.locale.Translator#setLocale(java.util
	 * .Locale)
	 */
	@Override
	public void setLocale(Locale locale) {
		if (locale != null) {
			usedLocale = locale;
		} else {
			logger.error("null Locale can't be set");
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.vaadin.addon.springMvp.locale.Translator#setLanguage(java
	 * .lang.String)
	 */
	@Override
	public void setLanguage(String language) {
		if (StringUtils.isNotBlank(language)) {
			usedLocale = new Locale(language);
		} else {
			logger.error("empty language can't be set");
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.vaadin.addon.springMvp.locale.Translator#getTranslation(java
	 * .lang.String)
	 */
	@Override
	public String getTranslation(String key) {
		return getTranslation(key, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.vaadin.addon.springMvp.locale.Translator#getTranslation(java
	 * .lang.String, java.lang.Object[])
	 */
	@Override
	public String getTranslation(String key, Object[] values) {

		if (messageSource != null) {
			StringBuilder defaultMessage = new StringBuilder();
			defaultMessage.append("'").append(key).append("' for '")
					.append(usedLocale.toLanguageTag()).append("' ");
			if (values != null) {
				defaultMessage.append("and values '");
				for (Object value : values) {
					if (value != null) {
						defaultMessage.append(value.toString()).append(" ");
					}
				}
				defaultMessage.append("' ");
			}
			defaultMessage.append("not found");

			return messageSource.getMessage(key, values,
					defaultMessage.toString(), usedLocale);
		}

		return MESSAGESOURCE_NOT_SET;

	}

	@Override
	public Locale getUsedLocale() {
		return usedLocale;
	}

	/**
	 * Use this method to set the locale which is set in the user's browser. To
	 * work properly a listener must be added to web.xml:
	 * <p>
	 * {@literal <listener>}<br/>
	 * &nbsp;&nbsp;{@literal<listener-class>}
	 * org.springframework.web.context.request. RequestContextListener
	 * {@literal</listener-class>}<br/>
	 * {@literal<listener>}
	 * </p>
	 * 
	 * @param localeResolver
	 */
	public void setLocaleFromRequest(boolean useRequestLocale) {
		if (useRequestLocale) {

			ServletRequestAttributes servletRequestAttributes = ((ServletRequestAttributes) RequestContextHolder
					.getRequestAttributes());
			if (servletRequestAttributes != null) {
				HttpServletRequest request = servletRequestAttributes
						.getRequest();
				setLocale(request.getLocale());
			} else {
				logger.error("Unable to get RequestContextHolder. Did you define the RequestContextListener in web.xml?");
			}

		}
	}
}
