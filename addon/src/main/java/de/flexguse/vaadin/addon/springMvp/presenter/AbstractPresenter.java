/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.presenter;

import java.lang.ref.WeakReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * The {@link AbstractPresenter} is a slim {@link Presenter} implementation.
 * Extend this class to implement your view-specific methods.
 * 
 * @author Christoph Guse, info@flexguse.de
 * @param <T>
 * 
 */
public abstract class AbstractPresenter<T> implements Presenter<T> {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private WeakReference<T> view;

	protected DispatcherManager dispatcherManager;

	private Translator translator;

	@Override
	public void setView(T view) {
		this.view = new WeakReference<T>(view);
	}

	@Override
	public T getView() {
		return view.get();
	}

	@Override
	public void setTranslator(Translator translator) {
		this.translator = translator;
	}

	@Override
	public Translator getTranslator() {
		return translator;
	}

	@Override
	public String translate(String key) {
		return translator.getTranslation(key);
	}

	@Override
	public String translate(String key, Object[] values) {
		return translator.getTranslation(key, values);
	}

	@Override
	public void setDispatcherManager(DispatcherManager dispatcherManager) {
		this.dispatcherManager = dispatcherManager;
	}

	@Override
	public void dispatchEvent(SpringMvpEvent event) {

		if (dispatcherManager != null) {
			dispatcherManager.dispatchEvent(event);
		} else {
			logger.debug("EventDispatcher is not set in Presenter, event can't be dispatched.");
		}

	}
}
