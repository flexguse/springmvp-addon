/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.presenter;

import de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager;
import de.flexguse.vaadin.addon.springMvp.dispatcher.SpringMvpDispatcher;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.locale.Translator;
import de.flexguse.vaadin.addon.springMvp.view.View;

/**
 * The presenter is intended to hold all the handling logic for a {@link View}.
 * Keep the {@link View} as cleaned as possible.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface Presenter<T> {

	/**
	 * Sets the View in the presenter.
	 * 
	 * @param view
	 */
	void setView(T view);

	/**
	 * Gets the View in the presenter.
	 * 
	 * @return view or null
	 */
	T getView();

	/**
	 * Sets the translator which can be used to access Spring managed
	 * ResourceBundles.
	 * 
	 * @param translator
	 */
	void setTranslator(Translator translator);

	/**
	 * Gets the translator which can be used to access Spring managed
	 * ResourceBundles.
	 * 
	 * @return
	 */
	Translator getTranslator();

	/**
	 * Convenience method to get translation from the set {@link Translator}.
	 * 
	 * @param key
	 *            the key for the translation in the resource bundle
	 * @return
	 */
	public String translate(String key);

	/**
	 * Convenience method to get translation from the set {@link Translator}.
	 * 
	 * @param key
	 *            the key for the translation in the resource bundle
	 * @param values
	 *            the values wich are put into the {0}... expressions in the
	 *            translations.
	 * @return
	 */
	public String translate(String key, Object[] values);

	/**
	 * Sets the {@link SpringMvpDispatcher} which is used in the presenter to
	 * dispatch events.
	 * 
	 * @param eventDispatcher
	 */
	public void setDispatcherManager(DispatcherManager eventDispatcher);

	/**
	 * Dispatches the given event. Is a convenience method so the
	 * EventDispatcher needs not to be autowired into the view.
	 * 
	 * @param event
	 */
	public void dispatchEvent(SpringMvpEvent event);

}
