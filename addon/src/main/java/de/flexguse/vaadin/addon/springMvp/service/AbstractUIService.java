/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.Application;
import com.vaadin.ui.Window;

import de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * This simple implementation of {@link UIService} provides the main
 * functionality. Extend this class to implement your own {@link UIService}s.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class AbstractUIService implements UIService {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private Application application;
	
	private Translator translator;

	private DispatcherManager eventDispatcherManager;

	public void setEventDispatcherManager(DispatcherManager eventDispatcherManager) {
		this.eventDispatcherManager = eventDispatcherManager;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.vaadin.addon.springMvp.service.UIService#setApplication(com
	 * .vaadin.Application)
	 */
	@Override
	public void setApplication(Application application) {
		this.application = application;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.flexguse.vaadin.addon.springMvp.service.UIService#getMainWindow()
	 */
	@Override
	public Window getMainWindow() {

		if (application != null) {
			return application.getMainWindow();
		}

		return null;
	}

	/**
	 * This method dispatches the given event.
	 * 
	 * @param event
	 */
	public void dispatchEvent(SpringMvpEvent event) {

		if (eventDispatcherManager != null) {
			eventDispatcherManager.dispatchEvent(event);
		} else {
			logger.error("DispatcherManager is not set in Presenter, event can't be dispatched.");
		}

	}
		
	/**
	 * @param translator the translator to set
	 */
	public void setTranslator(Translator translator) {
		this.translator = translator;
	}

	@Override
	public Translator getTranslator() {
		return translator;
	}

	@Override
	public String translate(String key) {
		return translator.getTranslation(key);
	}
	
	@Override
	public String translate(String key, Object[] values){
		return translator.getTranslation(key, values);
	}

}
