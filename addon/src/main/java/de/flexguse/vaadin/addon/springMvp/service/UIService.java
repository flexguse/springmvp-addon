/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.service;

import com.vaadin.Application;
import com.vaadin.ui.Window;

import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * Implement {@link UIService} to avoid direct access to backend services. This
 * can mean some overhead if you provide the backend service methods directly
 * but in case of additional logic like confirmation questions or service run
 * notices (like 'Object was saved' or 'Object was updated') the UI service is
 * the ideal place to implement.
 * 
 * <p>
 * Often it is a good idea to set the UIService on singleton-scope for a Vaadin
 * application instance.
 * </p>
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface UIService {

	/**
	 * Set the Vaadin application to be able to open UI dialogues.
	 * 
	 * @param application
	 */
	void setApplication(Application application);

	/**
	 * Convenience method which gets the main Vaadin application window.
	 * 
	 * @return the MainWindow or null, if application is not set.
	 */
	Window getMainWindow();

	/**
	 * Gets the translator for the application.
	 * 
	 * @return
	 */
	Translator getTranslator();

	/**
	 * Uses the translator to get a translation from the resource bundles.
	 * 
	 * @param translationKey
	 *            the key in the resource bundle
	 * @return found translation or not found information text
	 */
	String translate(String translationKey);

	/**
	 * Uses the translator to get a translation from the resource bundles.
	 * 
	 * @param translationKey
	 *            the key for a translation in the resource bundle
	 * @param values
	 *            the values which are put into the resource bundle expression
	 * @return
	 */
	String translate(String translationKey, Object[] values);
}
