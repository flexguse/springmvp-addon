/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager;
import de.flexguse.vaadin.addon.springMvp.dispatcher.SpringMvpDispatcher;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * This util {@link BeanPostProcessor} tries to register each Bean matching the
 * given package expression as {@link SpringMvpEvent} handlers.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class SpringMvpBeanPostProcessor implements BeanPostProcessor {

	private DispatcherManager dispatcherManager;
	private String handlerPackageExpression = ".*";

	/**
	 * Constructor setting the {@link SpringMvpDispatcher}. Each bean created by
	 * the Spring context is checked if it has to be registered.
	 * 
	 * @param springMvpApplicationDispatcher
	 *            the dispatcher for {@link SpringMvpEvent}s.
	 */
	public SpringMvpBeanPostProcessor(DispatcherManager dispatcherManager) {

		setDispatcherManager(dispatcherManager);
	}

	/**
	 * This helper method sets the {@link DispatcherManager}.
	 * 
	 * @param dispatcherManager
	 */
	private void setDispatcherManager(DispatcherManager dispatcherManager) {
		if (dispatcherManager == null) {
			throw new RuntimeException("null DispatcherManager is not allowed");
		}

		this.dispatcherManager = dispatcherManager;
	}

	/**
	 * Constructor setting the {@link SpringMvpDispatcher} and the
	 * handlerPackageExpression.
	 * 
	 * @param springMvpDispatcher
	 *            the dispatcher for {@link SpringMvpEvent}s.
	 * @param handlerPackageExpression
	 *            a regular expression the package of the created object in the
	 *            Spring context must match so not all created beans are
	 *            registered at the {@link SpringMvpDispatcher}.
	 */
	public SpringMvpBeanPostProcessor(DispatcherManager dispatcherManager,
			String handlerPackageExpression) {

		if (StringUtils.isBlank(handlerPackageExpression)) {
			throw new RuntimeException(
					"empty handlerPackageExpression is not allowed");
		}

		setDispatcherManager(dispatcherManager);
		
		this.handlerPackageExpression = handlerPackageExpression;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.beans.factory.config.BeanPostProcessor#
	 * postProcessBeforeInitialization(java.lang.Object, java.lang.String)
	 */
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName)
			throws BeansException {

		// nothing is done before the bean initialization.
		return bean;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.beans.factory.config.BeanPostProcessor#
	 * postProcessAfterInitialization(java.lang.Object, java.lang.String)
	 */
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName)
			throws BeansException {

		String beanClassName = bean.getClass().getName();

		if (StringUtils.isNotBlank(beanClassName)) {

			if (beanClassName.matches(handlerPackageExpression)) {
				dispatcherManager.registerListener(bean);
			}

		}

		return bean;
	}
}
