/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.util;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvents;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.EventClassInfo;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.EventMethodMap;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.MethodCallInfo;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * The {@link SpringMvpHandlerUtil} searches objects for the
 * {@link HandleSpringMvpEvent} and {@link HandleSpringMvpEvents} annotations
 * and returns all necessary information needed for the SpringMvpHandler.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class SpringMvpHandlerUtil {

	private Logger logger = LoggerFactory.getLogger(getClass());

	// @Cacheable("springMvp")
	public EventMethodMap getEventMethodMap(Object springMvpEventListener) {

		if (springMvpEventListener != null) {

			EventMethodMap eventMethodMap = new EventMethodMap();

			for (Method method : getDeclaredMethods(springMvpEventListener)) {

				/*
				 * handle all methods annotated with @HandleSpringMvpEvent
				 */
				if (method.getAnnotation(HandleSpringMvpEvent.class) != null) {

					addMethod(eventMethodMap, springMvpEventListener, method);
				}

				/*
				 * handle all methods annotated with @HandleSpringMvpEvents
				 */
				HandleSpringMvpEvents handleMultipleEventsAnnotation = method
						.getAnnotation(HandleSpringMvpEvents.class);
				if (handleMultipleEventsAnnotation != null) {

					Class<? extends SpringMvpEvent>[] registeredForEvents = handleMultipleEventsAnnotation
							.value();
					if (registeredForEvents != null
							&& registeredForEvents.length > 0) {

						for (Class<? extends SpringMvpEvent> event : registeredForEvents) {
							addMethod(eventMethodMap, springMvpEventListener,
									method, getEventClassInfo(event));
						}

					} else {
						if (logger.isDebugEnabled()) {
							logger.debug("the annotation @HandleSpringMvpEvents needs event classes given to be registered in event dispatcher.");
						}
					}

				}
			}

			return eventMethodMap;

		}

		return null;
	}

	/**
	 * This helper method gets all declared methods in the given listener
	 * object.
	 * 
	 * @param springMvpListener
	 * @return list of methods or empty list
	 */
	private List<Method> getDeclaredMethods(Object springMvpEventListener) {

		List<Method> result = new ArrayList<Method>();

		Method[] declaredMethods = ReflectionUtils
				.getAllDeclaredMethods(springMvpEventListener.getClass());

		if (declaredMethods != null) {
			result.addAll(Arrays.asList(declaredMethods));
		}

		return result;
	}

	/**
	 * This method gets the first method from the given EventListener matching
	 * the given parameters.
	 * 
	 * @param springMvpEventListener
	 *            the EventListener which contains the method.
	 * @param methodName
	 *            the name of the method
	 * @param eventClass
	 *            the argument-class of the method
	 * @param genericEventClass
	 *            the generic class of the eventClass. If given a method must
	 *            exist like <b>public void
	 *            methodName(EventClass<GenericEventClass> methodArgument)</b>;
	 * @return found method or null
	 */
	public Method getDeclareMethod(Object springMvpEventListener,
			String methodName, Class<? extends SpringMvpEvent> eventClass) {

		if (springMvpEventListener != null
				&& StringUtils.isNotBlank(methodName) && eventClass != null) {
			return ReflectionUtils.findMethod(
					springMvpEventListener.getClass(), methodName, eventClass);
		} else {
			logger.debug("EventListener, Methodname and eventClass must not be null or empty");
		}

		return null;

	}

	/**
	 * This method gets the method in the given eventListener, which matches the
	 * given {@link EventClassInfo}.
	 * 
	 * @param springMvpEventListener
	 * @param methodName
	 * @param eventClassInfo
	 * @return found method or null
	 */
	public Method getDeclaredMethod(Object springMvpEventListener,
			String methodName, EventClassInfo eventClassInfo) {

		if (springMvpEventListener != null
				&& StringUtils.isNotBlank(methodName) && eventClassInfo != null) {

			Method result = getDeclareMethod(springMvpEventListener, methodName,
					eventClassInfo.getEventClass());
			if(result == null){
				result = getDeclareMethod(springMvpEventListener, methodName,
						SpringMvpEvent.class);
			}
			
			return result;

		}

		return null;

	}

	/**
	 * This helper method adds the given method to the eventMethodMap. The Event
	 * is the argument for the method.
	 * 
	 * @param eventMethodMap
	 * @param method
	 */
	private void addMethod(EventMethodMap eventMethodMap,
			Object springMvpEventListener, Method executionMethod) {

		addMethod(eventMethodMap, springMvpEventListener, executionMethod, null);

	}

	/**
	 * This helper method adds the given executionMethod to the eventMethodMap.
	 * 
	 * @param eventMethodMap
	 * @param springMvpEventListener
	 * @param executionMethod
	 * @param springMvpEventClass
	 */
	private void addMethod(EventMethodMap eventMethodMap,
			Object springMvpEventListener, Method executionMethod,
			EventClassInfo springMvpEventClass) {

		MethodCallInfo methodCallInfo = createMethodCallInfo(executionMethod,
				springMvpEventClass);

		if (methodCallInfo != null) {
			eventMethodMap.addEventListener(methodCallInfo.getEventClassInfo(),
					methodCallInfo);

		}
	}

	/**
	 * This method gets a MethodCallInfo from the given method and the given
	 * method argument class. The listener keys are taken from the annotation
	 * (if present).
	 * 
	 * @param executionMethod
	 *            the method which shall be executed. this must be not null
	 * @param eventClassInfo
	 *            the classInfo which is used as argument to call the class. If this
	 *            class is not given the class is taken from the method.
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MethodCallInfo createMethodCallInfo(Method executionMethod,
			EventClassInfo eventClassInfo) {

		if (executionMethod == null) {
			throw new RuntimeException(
					"MethodCallInfo can only be created if the executionMethod is not null");
		}

		String executionmethodName = executionMethod.getName();

		Set<String> dispatcherKeys = new HashSet<String>();

		HandleSpringMvpEvent handleSpringMvpEventAnnotation = executionMethod
				.getAnnotation(HandleSpringMvpEvent.class);
		if (handleSpringMvpEventAnnotation != null) {
			dispatcherKeys.addAll(Arrays.asList(handleSpringMvpEventAnnotation
					.eventScopes()));
		}
		HandleSpringMvpEvents handleSpringMvpEventsAnnotation = executionMethod
				.getAnnotation(HandleSpringMvpEvents.class);
		if (handleSpringMvpEventsAnnotation != null) {
			dispatcherKeys.addAll(Arrays.asList(handleSpringMvpEventsAnnotation
					.eventScopes()));
		}

		/*
		 * Doing some tests to ensure the EventDispatcher is able to call the
		 * method.
		 */
		Class<?>[] methodParameterTypes = executionMethod.getParameterTypes();

		if (eventClassInfo != null) {

			// check if parameter is of type SpringMvpEvent
			if (methodParameterTypes.length < 1
					|| !methodParameterTypes[0]
							.isAssignableFrom(eventClassInfo
									.getEventClass())) {

				StringBuilder errorMessage = new StringBuilder();
				errorMessage.append("The method ")
						.append(executionMethod.getName()).append("(");
				if (methodParameterTypes.length > 0) {
					
					List<String> methodArguments = new ArrayList<String>();
					for(Class<?> methodParameterType : methodParameterTypes){
						StringBuilder methodParameter = new StringBuilder();
						methodParameter.append(methodParameterType.getName()).append(" ").append(methodParameterType.getSimpleName().toLowerCase());
						methodArguments.add(methodParameter.toString());
					}
					
					errorMessage
							.append(StringUtils.join(methodArguments, ","));
				}
				errorMessage
						.append(")")
						.append(" can't be executed with argument of type ")
						.append(eventClassInfo.toString())
						.append(". No MethodCallInfo was created for this method.");

				logger.error(errorMessage.toString());
				return null;
			}

			return new MethodCallInfo(executionmethodName, eventClassInfo,
					new ArrayList<String>(dispatcherKeys));
		} else {

			// check if the method to register has one argument
			if (methodParameterTypes == null
					|| methodParameterTypes.length != 1) {
				logger.error("Only for methods with one SpringMvpEvent parameter can be used for MethodCallInfo creation. Method "
						+ executionmethodName + " was not processed.");
				return null;

			}

			/*
			 * Get the generic information (if existent) of the method argument.
			 */
			Type[] genericParameterTypes = executionMethod
					.getGenericParameterTypes();
			Class<?> genericParameterClass = null;

			if (genericParameterTypes != null
					&& genericParameterTypes.length > 0) {
				if (genericParameterTypes[0] instanceof ParameterizedType) {
					ParameterizedType genericType = (ParameterizedType) genericParameterTypes[0];
					if (genericType != null) {
						Type[] actualTypeArguments = genericType
								.getActualTypeArguments();
						if (actualTypeArguments != null
								&& actualTypeArguments.length > 0) {
							if(actualTypeArguments[0] instanceof Class){
								genericParameterClass = (Class<?>) actualTypeArguments[0];
							}
							
						}
					}

				}

			}

			// the event is automatically the argument in the method
			return new MethodCallInfo(executionmethodName, new EventClassInfo(
					(Class<? extends SpringMvpEvent>) methodParameterTypes[0],
					genericParameterClass), new ArrayList<String>(
					dispatcherKeys));

		}

	}

	/**
	 * Normally the class identification of a {@link SpringMvpEvent} should be
	 * done by event.getClass(). This works fine until Java generics are used in
	 * the events.
	 * <p>
	 * In Java the generics information are not available at runtime, so there
	 * is no change to differentiate between something like
	 * ModelWasAddedEvent<String> and ModelWasAddedEvent<Integer>.
	 * </p>
	 * In case of event dispatching both events are different.
	 * 
	 * @param springMvpEvent
	 * @return {@link EventClassInfo} with event className and the generics
	 *         class. The classnames are fully qualified.
	 */
	public EventClassInfo getEventClassInfo(SpringMvpEvent springMvpEvent) {

		if (springMvpEvent == null) {
			return null;
		}

		return new EventClassInfo(springMvpEvent.getClass(),
				springMvpEvent.getGenericType());

	}

	/**
	 * Normally the class identification of a {@link SpringMvpEvent} should be
	 * done by event.getClass(). This works fine until Java generics are used in
	 * the events.
	 * <p>
	 * In Java the generics information are not available at runtime, so there
	 * is no change to differentiate between something like
	 * ModelWasAddedEvent<String> and ModelWasAddedEvent<Integer>.
	 * </p>
	 * In case of event dispatching both events are different.
	 * 
	 * @param springMvpEvent
	 * @return {@link EventClassInfo} with event className and no generic
	 *         information. The classname is fully qualified.
	 */
	public EventClassInfo getEventClassInfo(
			Class<? extends SpringMvpEvent> springMvpEventClass) {

		if (springMvpEventClass == null) {
			return null;
		}

		return new EventClassInfo(springMvpEventClass, null);

	}

}
