/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.view;

/**
 * A View is a complex UI component. Views are tightly coupled to Presenters,
 * which are intended to hold all the logic which belongs to the UI components
 * combined in the {@link View}.
 * <p>
 * Put all UI elements into to the view and all logic (Listeners, ActionHandlers
 * etc.) into the presenter to keep the View as clean an slim as possible.
 * </p>
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface View<T> {

	/**
	 * Sets the Presenter in the view
	 * 
	 * @param presenter
	 */
	void setPresenter(T presenter);

}
