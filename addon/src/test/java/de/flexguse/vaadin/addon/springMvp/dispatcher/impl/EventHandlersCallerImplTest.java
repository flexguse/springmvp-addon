/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.impl;

import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;

import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import de.flexguse.vaadin.addon.springMvp.dispatcher.model.EventClassInfo;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.MethodCallInfo;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowErrorMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowTrayNotificationEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowWarningMessageEvent;
import de.flexguse.vaadin.addon.springMvp.util.SpringMvpHandlerUtil;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class EventHandlersCallerImplTest {

	private EventHandlersCallerImpl caller;
	private SpringMvpHandlerUtil mvpHandlerUtil = new SpringMvpHandlerUtil();
		
	@Before
	public void setUp() {
		caller = new EventHandlersCallerImpl();
		caller.setEventClassInfo(new EventClassInfo(ShowTrayNotificationEvent.class));
		caller.setSpringMvpHandlerUtil(new SpringMvpHandlerUtil());
	}

	@Ignore
	public void handleTestEvent(ShowTrayNotificationEvent event) {
		// intended to do nothing
	}

	@Ignore
	public void showSomethingInTheTray(ShowTrayNotificationEvent event) {
		// intended to do nothing		
	}

	@Ignore
	public void handleTestEvent(ShowWarningMessageEvent event) {
		// intended to do nothing
	}

	@Test
	public void testRegisterHandler() throws NoSuchMethodException,
			SecurityException {

		caller.registerHandlerMethod(null, (Method)null);
		assertEquals(0, caller.getEventHandlersRegistry().size());

		caller.registerHandlerMethod(this, (MethodCallInfo)null);
		assertEquals(0, caller.getEventHandlersRegistry().size());

		caller.registerHandlerMethod(this, (Method)null);
		assertEquals(0, caller.getEventHandlersRegistry().size());

		caller.registerHandlerMethod(
				this,
				this.getClass().getDeclaredMethod("handleTestEvent",
						ShowTrayNotificationEvent.class));
		assertEquals(1, caller.getEventHandlersRegistry().size());
		assertEquals(1, caller.getEventHandlersRegistry().get(this).size());

		caller.registerHandlerMethod(
				this,
				this.getClass().getDeclaredMethod("handleTestEvent",
						ShowWarningMessageEvent.class));
		assertEquals(2, caller.getEventHandlersRegistry().get(this).size());

		caller.registerHandlerMethod(
				this,
				this.getClass().getDeclaredMethod("showSomethingInTheTray",
						ShowTrayNotificationEvent.class));
		assertEquals(3, caller.getEventHandlersRegistry().get(this).size());

	}

	@Test
	public void testUnregisterHandler() throws NoSuchMethodException,
			SecurityException {

		// this method calls must not cause exceptions
		caller.unregisterHandler(null);
		caller.unregisterHandler(new String("Hello"));
		caller.unregisterHandler(this);

		// register to be able to unregister
		caller.registerHandlerMethod(
				this,
				this.getClass().getDeclaredMethod("handleTestEvent",
						ShowTrayNotificationEvent.class));
		caller.registerHandlerMethod(
				this,
				this.getClass().getDeclaredMethod("showSomethingInTheTray",
						ShowTrayNotificationEvent.class));

		caller.unregisterHandler(this);
		assertEquals(0, caller.getEventHandlersRegistry().size());

	}

	@Test
	public void testUnregisterHandlerMethod() throws NoSuchMethodException,
			SecurityException {

		// this method calls must not cause exceptions
		caller.unregisterHandlerMethod(null, null, null);
		caller.unregisterHandlerMethod(this, null, null);
		caller.unregisterHandlerMethod(
				null,
				this.getClass().getDeclaredMethod("handleTestEvent",
						ShowTrayNotificationEvent.class), new EventClassInfo(ShowTrayNotificationEvent.class));

		// register to be able to unregister
		caller.registerHandlerMethod(
				this,
				this.getClass().getDeclaredMethod("handleTestEvent",
						ShowTrayNotificationEvent.class));
		caller.registerHandlerMethod(
				this,
				this.getClass().getDeclaredMethod("showSomethingInTheTray",
						ShowTrayNotificationEvent.class));

		caller.unregisterHandlerMethod(
				this,
				this.getClass().getDeclaredMethod("handleTestEvent",
						ShowTrayNotificationEvent.class), new EventClassInfo(ShowTrayNotificationEvent.class, String.class));		
		Set<MethodCallInfo> methodCallInfos = caller.getEventHandlersRegistry()
				.get(this);
		assertEquals("both registered EventListeners must be in the caller, because the unregistering was done with a different EventClassInfo", 2, methodCallInfos.size());
		caller.unregisterHandlerMethod(
				this,
				this.getClass().getDeclaredMethod("handleTestEvent",
						ShowTrayNotificationEvent.class), new EventClassInfo(ShowTrayNotificationEvent.class));		
		methodCallInfos = caller.getEventHandlersRegistry()
				.get(this);
		assertEquals(1, methodCallInfos.size());

		MethodCallInfo expectedInfo = new MethodCallInfo(
				"showSomethingInTheTray", new EventClassInfo(ShowTrayNotificationEvent.class), new ArrayList<String>());
		assertTrue(methodCallInfos.contains(expectedInfo));

		caller.unregisterHandlerMethod(
				this,
				this.getClass().getDeclaredMethod("showSomethingInTheTray",
						ShowTrayNotificationEvent.class), new EventClassInfo(ShowTrayNotificationEvent.class));
		assertEquals(0, caller.getEventHandlersRegistry().size());

	}
	
	@Test
	public void testCleanUp() throws NoSuchMethodException, SecurityException{
		caller.cleanUp();
		
		caller.registerHandlerMethod(
				this,
				this.getClass().getDeclaredMethod("handleTestEvent",
						ShowTrayNotificationEvent.class));
		caller.registerHandlerMethod(
				this,
				this.getClass().getDeclaredMethod("showSomethingInTheTray",
						ShowTrayNotificationEvent.class));
		
		caller.cleanUp();
		assertEquals(0, caller.getEventHandlersRegistry().size());
	}
	
	@Test
	public void testCallHandlerMethod() throws NoSuchMethodException, SecurityException{
		
		caller.callHandlerMethods(null);
		caller.callHandlerMethods(new ShowErrorMessageEvent(this, "title", "info"));
		
		EventHandlersCallerImplTest handler1 = mock(EventHandlersCallerImplTest.class);
		EventHandlersCallerImplTest handler2 = mock(EventHandlersCallerImplTest.class);
		EventHandlersCallerImplTest handler3 = mock(EventHandlersCallerImplTest.class);
		
		caller.registerHandlerMethod(handler1, mvpHandlerUtil.getDeclareMethod(handler1,"showSomethingInTheTray", ShowTrayNotificationEvent.class));
		caller.registerHandlerMethod(handler1,  mvpHandlerUtil.getDeclareMethod(handler1, "handleTestEvent", ShowTrayNotificationEvent.class));
		caller.registerHandlerMethod(handler2,  mvpHandlerUtil.getDeclareMethod(handler2, "showSomethingInTheTray", ShowTrayNotificationEvent.class));
		caller.registerHandlerMethod(handler3,  mvpHandlerUtil.getDeclareMethod(handler3,"showSomethingInTheTray", ShowTrayNotificationEvent.class));
		caller.registerHandlerMethod(handler3,  mvpHandlerUtil.getDeclareMethod(handler3, "handleTestEvent", ShowTrayNotificationEvent.class));
		
		ShowTrayNotificationEvent testEvent = new ShowTrayNotificationEvent(this, "test", "info");
		caller.callHandlerMethods(testEvent);
		
		verify(handler1, times(1)).showSomethingInTheTray(testEvent);
		verify(handler1, times(1)).handleTestEvent(testEvent);
		verify(handler1, times(0)).handleTestEvent((ShowWarningMessageEvent)anyObject());
		
		verify(handler2, times(1)).showSomethingInTheTray(testEvent);
		verify(handler2, times(0)).handleTestEvent(testEvent);
		verify(handler2, times(0)).handleTestEvent((ShowWarningMessageEvent)anyObject());
		
		verify(handler3, times(1)).showSomethingInTheTray(testEvent);
		verify(handler3, times(1)).handleTestEvent(testEvent);
		verify(handler3, times(0)).handleTestEvent((ShowWarningMessageEvent)anyObject());
		
	}

}
