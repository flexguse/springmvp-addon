/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import de.flexguse.vaadin.addon.springMvp.dispatcher.model.EventClassInfo;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.MethodCallInfo;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowTrayNotificationEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowWarningMessageEvent;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class MethodCallInfoTest {

	@Test
	public void testEquals() {

		String methodName = "someMethod";

		MethodCallInfo info1 = new MethodCallInfo(methodName,
				new EventClassInfo(ShowWarningMessageEvent.class),
				new ArrayList<String>());
		assertFalse(info1.equals(null));

		MethodCallInfo info2 = new MethodCallInfo(methodName,
				new EventClassInfo(ShowWarningMessageEvent.class),
				Arrays.asList("test1", "test2"));
		assertFalse(info1.equals(info2));

		MethodCallInfo info3 = new MethodCallInfo(methodName,
				new EventClassInfo(ShowTrayNotificationEvent.class),
				new ArrayList<String>());
		assertFalse(info2.equals(info3));

		MethodCallInfo info4 = new MethodCallInfo(methodName,
				new EventClassInfo(ShowWarningMessageEvent.class),
				Arrays.asList("test1", "test2"));
		assertTrue(info2.equals(info4));

	}

}
