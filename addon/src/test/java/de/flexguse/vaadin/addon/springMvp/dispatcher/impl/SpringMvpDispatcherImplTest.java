/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.flexguse.vaadin.addon.springMvp.dispatcher.impl.handler.AnnotatedEventListener;
import de.flexguse.vaadin.addon.springMvp.dispatcher.impl.handler.NotAnnotatedEventListener;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.EventClassInfo;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent.ExecutionType;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowErrorMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowHumanizedMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowTrayNotificationEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowWarningMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasUpdatedEvent;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/springMvp-context.xml",
		"classpath:/spring/dispatcher-test-context.xml" })
public class SpringMvpDispatcherImplTest implements ApplicationContextAware {

	private SpringMvpDispatcherImpl dispatcher;

	private Logger logger = LoggerFactory.getLogger(getClass());

	private ApplicationContext springContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.springContext = applicationContext;
	}

	@Before
	public void setUp() {
		dispatcher = springContext.getBean(SpringMvpDispatcherImpl.class);
	}

	/**
	 * Registering can be watched in logfiles. Goal is to throw no exceptions
	 * and to pass assertions.
	 * 
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	@Test
	public void testRegisterMethod() throws NoSuchMethodException,
			SecurityException {

		// test registering null objects
		dispatcher.registerListener(null, null,
				(Class<? extends SpringMvpEvent>) null);
		assertTrue(dispatcher.getEventCallerLookupMap().isEmpty());

		dispatcher.registerListener(this, null,
				(Class<? extends SpringMvpEvent>) null);
		assertTrue(dispatcher.getEventCallerLookupMap().isEmpty());

		// this method can't be registered because no argument is given
		dispatcher.registerListener(this,
				this.getClass().getDeclaredMethod("testRegisterMethod"),
				ShowWarningMessageEvent.class);
		assertTrue(dispatcher.getEventCallerLookupMap().isEmpty());

		// this method can't be registered because it has two arguments
		Method notRegisterable = this.getClass().getDeclaredMethod(
				"notRegisterableListenerMethod", String.class,
				SpringMvpEvent.class);
		dispatcher.registerListener(this, notRegisterable,
				ShowWarningMessageEvent.class);
		assertTrue(dispatcher.getEventCallerLookupMap().isEmpty());

		// this method is registerable
		Method registerable = this.getClass().getDeclaredMethod(
				"handleSpringMvpEvent", SpringMvpEvent.class);
		dispatcher.registerListener(this, registerable,
				ShowWarningMessageEvent.class);
		assertNotNull(dispatcher.getEventCallerLookupMap().get(
				new EventClassInfo(ShowWarningMessageEvent.class)));
		assertEquals(1, dispatcher.getEventCallerLookupMap().size());
		EventHandlersCallerImpl callerImpl = (EventHandlersCallerImpl) dispatcher
				.getEventCallerLookupMap().get(
						new EventClassInfo(ShowWarningMessageEvent.class));
		assertEquals(1, callerImpl.getEventHandlersRegistry().get(this).size());

		// register valid method
		dispatcher.registerListener(
				this,
				this.getClass().getDeclaredMethod("echoWarningMessage",
						ShowWarningMessageEvent.class),
				ShowWarningMessageEvent.class);
		assertNotNull(dispatcher.getEventCallerLookupMap().get(
				new EventClassInfo(ShowWarningMessageEvent.class)));
		assertEquals(1, dispatcher.getEventCallerLookupMap().size());
		callerImpl = (EventHandlersCallerImpl) dispatcher
				.getEventCallerLookupMap().get(
						new EventClassInfo(ShowWarningMessageEvent.class));
		assertEquals(2, callerImpl.getEventHandlersRegistry().get(this).size());

	}

	@Test
	public void testRegisterMethodWithWrongEvent()
			throws NoSuchMethodException, SecurityException {
		// try to register method which fails because the EventClass and the
		// method argument does not match.
		dispatcher.registerListener(
				this,
				this.getClass().getDeclaredMethod("echoHumanizedMessage",
						ShowHumanizedMessageEvent.class),
				ShowWarningMessageEvent.class);
		assertTrue(dispatcher.getEventCallerLookupMap().isEmpty());

	}

	/**
	 * This testcase tests if an annotated Listener object is registered
	 * correctly.
	 */
	@Test
	public void testRegisterEventListener() {

		dispatcher.registerListener(null);
		assertTrue("no listeners expected after null registration", dispatcher
				.getEventCallerLookupMap().isEmpty());

		NotAnnotatedEventListener notAnnotatedEventListener = new NotAnnotatedEventListener();
		dispatcher.registerListener(notAnnotatedEventListener);
		assertTrue(dispatcher.getEventCallerLookupMap().isEmpty());

		AnnotatedEventListener annotatedEventListener = new AnnotatedEventListener();
		dispatcher.registerListener(annotatedEventListener);
		assertEquals(
				"expected registration for ShowTrayNotificationEvent, ShowWarningMessageEvent, ModelWasUpdatedEvent<String> and ModelWasUpdatedEvent<Long>",
				4, dispatcher.getEventCallerLookupMap().size());

		EventHandlersCallerImpl trayNotificationHandlersCaller = (EventHandlersCallerImpl) dispatcher
				.getEventCallerLookupMap().get(
						new EventClassInfo(ShowTrayNotificationEvent.class));
		assertEquals(2, trayNotificationHandlersCaller
				.getEventHandlersRegistry().get(annotatedEventListener).size());
		EventHandlersCallerImpl warningmessageHandlersCaller = (EventHandlersCallerImpl) dispatcher
				.getEventCallerLookupMap().get(
						new EventClassInfo(ShowWarningMessageEvent.class));
		assertEquals(2, warningmessageHandlersCaller.getEventHandlersRegistry()
				.get(annotatedEventListener).size());

	}

	@Test
	public void testDispatchEvent() throws NoSuchMethodException,
			SecurityException {

		dispatcher.dispatchEvent(null);

		dispatcher.dispatchEvent(new ShowWarningMessageEvent(this, "title",
				"info"));

		// register listener
		AnnotatedEventListener eventListener = new AnnotatedEventListener();
		dispatcher.registerListener(eventListener);

		/*
		 * dispatch event for which no handlers are registered
		 */
		ShowErrorMessageEvent errorMessageEvent = new ShowErrorMessageEvent(
				this, "title", "info");
		dispatcher.dispatchEvent(errorMessageEvent);
		assertEquals(0, eventListener.getHandleShowTrayNotificationCounter());
		assertEquals(0, eventListener.getHandleShowWarningMessageCounter());
		assertEquals(0, eventListener.getHandleWarningTrayMessageCounter());

	}

	@Test
	public void testSyncEventDispatching() {

		AnnotatedEventListener eventListener = new AnnotatedEventListener();
		dispatcher.registerListener(eventListener);

		/*
		 * dispatch event synchronously
		 */
		ShowWarningMessageEvent warningEvent = new ShowWarningMessageEvent(
				this, "title", "info");
		dispatcher.dispatchEvent(warningEvent);

		assertEquals("handleShowWarningMessage", 1,
				eventListener.getHandleShowWarningMessageCounter());
		assertEquals("handleWarningTrayMessage", 1,
				eventListener.getHandleWarningTrayMessageCounter());
		assertEquals("handleShowTrayNotification", 0,
				eventListener.getHandleShowTrayNotificationCounter());

	}

	@Test
	public void testAsyncEventDispatching() throws InterruptedException {

		AnnotatedEventListener eventListener = new AnnotatedEventListener();
		dispatcher.registerListener(eventListener);

		/*
		 * dispatch an event with asynchronous listener method calls
		 */
		SpringMvpEvent event = new ShowTrayNotificationEvent(this,
				"Asynchronously executed",
				"The event-listener methods for this event were called asynchronously.");
		event.setExecutionType(ExecutionType.ASYNC);
		dispatcher.dispatchEvent(event);

		// wait a little so the async operations are done
		Thread.sleep(2000);

		assertEquals("handleShowWarningMessage", 0,
				eventListener.getHandleShowWarningMessageCounter());
		assertEquals("handleWarningTrayMessage", 1,
				eventListener.getHandleWarningTrayMessageCounter());
		assertEquals("handleShowTrayNotification", 1,
				eventListener.getHandleShowTrayNotificationCounter());

	}

	/**
	 * This method can't be registered as {@link SpringMvpEvent} listener.
	 * 
	 * @param test
	 * @param event
	 */
	public void notRegisterableListenerMethod(String test, SpringMvpEvent event) {
		// intended to do nothing
	}

	/**
	 * This method can't be registered.
	 * 
	 * @param test
	 */
	public void notRegisterableListenerMethod(String test) {
	}

	public void handleSpringMvpEvent(SpringMvpEvent event) {
		logger.info("handleSpringMvpEvent was executed");
	}

	public void echoWarningMessage(ShowWarningMessageEvent event) {

		logger.info(event.getTitle() + ": " + event.getInfo());

	}

	public void echoHumanizedMessage(ShowHumanizedMessageEvent event) {

		logger.info(event.getTitle() + ": " + event.getInfo());

	}

	@Test
	public void testUnregisterMethod() throws NoSuchMethodException,
			SecurityException {

		dispatcher.unregisterListener(null, (Method) null,
				(SpringMvpEvent) null);
		dispatcher.unregisterListener(this, (Method) null,
				(SpringMvpEvent) null);
		dispatcher.unregisterListener(
				this,
				this.getClass().getDeclaredMethod("handleSpringMvpEvent",
						SpringMvpEvent.class), (SpringMvpEvent) null);

		// register two methods
		dispatcher.registerListener(
				this,
				this.getClass().getDeclaredMethod("handleSpringMvpEvent",
						SpringMvpEvent.class), ShowHumanizedMessageEvent.class);

		dispatcher.registerListener(
				this,
				this.getClass().getDeclaredMethod("echoHumanizedMessage",
						ShowHumanizedMessageEvent.class),
				ShowHumanizedMessageEvent.class);

		// unregister handleSpringMvpEvent method
		dispatcher.unregisterListener(
				this,
				this.getClass().getDeclaredMethod("handleSpringMvpEvent",
						SpringMvpEvent.class), ShowHumanizedMessageEvent.class);

		// check if one method is still registered
		EventHandlersCallerImpl humanizedMessageHandlersCaller = (EventHandlersCallerImpl) dispatcher
				.getEventCallerLookupMap().get(
						new EventClassInfo(ShowHumanizedMessageEvent.class));
		assertEquals(
				"number of events registered for ShowHumanizedMessageEvent",
				1,
				humanizedMessageHandlersCaller.getEventHandlersRegistry()
						.get(this).size());

		// unregister second listener
		dispatcher.unregisterListener(
				this,
				this.getClass().getDeclaredMethod("echoHumanizedMessage",
						ShowHumanizedMessageEvent.class),
				ShowHumanizedMessageEvent.class);

		humanizedMessageHandlersCaller = (EventHandlersCallerImpl) dispatcher
				.getEventCallerLookupMap().get(
						new EventClassInfo(ShowHumanizedMessageEvent.class));
		assertNull(humanizedMessageHandlersCaller);

	}

	@Test
	public void testUnregisterObject() {

		// must not throw exception
		dispatcher.unregisterListener(null);

		AnnotatedEventListener annotatedEventLister = new AnnotatedEventListener();
		dispatcher.registerListener(annotatedEventLister);

		dispatcher.unregisterListener(annotatedEventLister);
		assertEquals(0, dispatcher.getEventCallerLookupMap().size());

	}

	@Test
	public void testGenericEvents() {

		AnnotatedEventListener annotatedEventListener = new AnnotatedEventListener();
		dispatcher.registerListener(annotatedEventListener);

		ModelWasUpdatedEvent<String> stringUpdateEvent = new ModelWasUpdatedEvent<String>(
				this, String.class);
		stringUpdateEvent.setExecutionType(ExecutionType.SYNC);
		dispatcher.dispatchEvent(stringUpdateEvent);

		assertEquals(1, annotatedEventListener.getHandleStringUpdatedCounter());
		assertEquals(0, annotatedEventListener.getHandleLongUpdatedCounter());

		ModelWasUpdatedEvent<Long> longUpdateEvent = new ModelWasUpdatedEvent<Long>(
				this, Long.class);
		longUpdateEvent.setExecutionType(ExecutionType.SYNC);
		dispatcher.dispatchEvent(longUpdateEvent);

		assertEquals(1, annotatedEventListener.getHandleStringUpdatedCounter());
		assertEquals(1, annotatedEventListener.getHandleLongUpdatedCounter());

	}

}
