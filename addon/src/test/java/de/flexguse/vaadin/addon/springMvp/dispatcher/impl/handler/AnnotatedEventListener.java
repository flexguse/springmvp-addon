/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.impl.handler;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;
import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvents;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowTrayNotificationEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowWarningMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasUpdatedEvent;

/**
 * This class is used for tests only.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class AnnotatedEventListener {

	private int handleShowWarningMessageCounter = 0;
	private int handleShowTrayNotificationCounter = 0;
	private int handleWarningTrayMessageCounter = 0;
	private int handleStringUpdatedCounter = 0;
	private int handleLongUpdatedCounter = 0;

	public int getHandleShowWarningMessageCounter() {
		return handleShowWarningMessageCounter;
	}

	public int getHandleShowTrayNotificationCounter() {
		return handleShowTrayNotificationCounter;
	}

	public int getHandleWarningTrayMessageCounter() {
		return handleWarningTrayMessageCounter;
	}
	
	

	/**
	 * @return the handleStringUpdatedCounter
	 */
	public int getHandleStringUpdatedCounter() {
		return handleStringUpdatedCounter;
	}

	/**
	 * @return the handleLongUpdatedCounter
	 */
	public int getHandleLongUpdatedCounter() {
		return handleLongUpdatedCounter;
	}

	@HandleSpringMvpEvent
	public void handleShowWarningMessage(
			ShowWarningMessageEvent warningMessageEvent) {

		handleShowWarningMessageCounter++;

	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.SpringMvpApplication })
	public void handleShowTrayNotification(
			ShowTrayNotificationEvent trayNotificationEvent) {

		handleShowTrayNotificationCounter++;

	}

	@HandleSpringMvpEvents({ ShowWarningMessageEvent.class,
			ShowTrayNotificationEvent.class })
	public void handleWarningTrayMessage(SpringMvpEvent event) {

		handleWarningTrayMessageCounter++;

	}

	@HandleSpringMvpEvent
	public void handleStringUpdated(ModelWasUpdatedEvent<String> updateEvent) {
		handleStringUpdatedCounter++;
	}

	@HandleSpringMvpEvent
	public void handleLongUpdated(ModelWasUpdatedEvent<Long> updateEvent) {
		handleLongUpdatedCounter++;
	}

}
