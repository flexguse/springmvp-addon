/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.impl.handler;

import org.junit.Ignore;

/**
 * This class is used for tests only.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@Ignore
public class NotAnnotatedEventListener {

	public void doSomething(String something) {

		// intended to do nothing

	}

}
