/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.model;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;

import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvents;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowHumanizedMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowWarningMessageEvent;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@Ignore
public class EventListenerObject {

    private List<EventListenerCaller> callers = new ArrayList<EventListenerCaller>();

    public enum EventListenerCaller {
	EVENT, MESSAGE_EVENT, WARNING_EVENT
    }

    /**
     * @return the callers
     */
    public List<EventListenerCaller> getCallers() {
	return callers;
    }

    @HandleSpringMvpEvents({ ShowHumanizedMessageEvent.class,
	    ShowWarningMessageEvent.class })
    public void handleEvent(SpringMvpEvent event) {
	callers.add(EventListenerCaller.EVENT);
    }

    @HandleSpringMvpEvent
    public void handleShowHumanizedMessageEvent(ShowHumanizedMessageEvent event) {
	callers.add(EventListenerCaller.MESSAGE_EVENT);
    }

    @HandleSpringMvpEvent
    public void handleShowWarningMessageEvent(ShowWarningMessageEvent event) {
	callers.add(EventListenerCaller.WARNING_EVENT);
    }

}
