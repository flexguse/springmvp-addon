/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowErrorMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowTrayNotificationEvent;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class EventMethodMapTest {

	private EventMethodMap eventMethodMap;

	@Before
	public void setUp() {
		eventMethodMap = new EventMethodMap();
	}

	@Test
	public void testAddEventListener() {

		// must not throw exception
		eventMethodMap.addEventListener(null, null);
		eventMethodMap.addEventListener(null, new MethodCallInfo(null, null,
				null));
		eventMethodMap.addEventListeners(null, new MethodCallInfoList());
		eventMethodMap.addEventListener(new EventClassInfo(), new MethodCallInfo(null,
				null, null));
		eventMethodMap.addEventListeners(new EventClassInfo(), new MethodCallInfoList());

		MethodCallInfo methodCallInfo = new MethodCallInfo("methodName",
				new EventClassInfo(SpringMvpEvent.class), new ArrayList<String>());

		eventMethodMap.addEventListener(new EventClassInfo(ShowTrayNotificationEvent.class), methodCallInfo);

		Set<EventClassInfo> mapKeys = eventMethodMap.getKeys();
		assertTrue(mapKeys.contains(new EventClassInfo(ShowTrayNotificationEvent.class)));
		MethodCallInfoList infoList = eventMethodMap.get(new EventClassInfo(ShowTrayNotificationEvent.class));
		assertTrue(infoList.getMethodCallInfos().contains(methodCallInfo));

	}

	@Test
	public void testRemoveEventListener() {

		EventClassInfo eventClassInfo1 = new EventClassInfo(ShowTrayNotificationEvent.class, String.class);
		EventClassInfo eventClassInfo2 = new EventClassInfo(ShowTrayNotificationEvent.class, Long.class);
		
		// must not throw Exception
		eventMethodMap.removeEventListener(null, null);
		eventMethodMap.removeEventListener(null, new MethodCallInfo(null, null,
				null));
		eventMethodMap.removeEventListener(new EventClassInfo(), new MethodCallInfo(
				null, null, null));

		MethodCallInfo methodCallInfo = new MethodCallInfo("methodName",
				eventClassInfo1, new ArrayList<String>());
		eventMethodMap.addEventListener(eventClassInfo1, methodCallInfo);

		MethodCallInfo methodCallInfo2 = new MethodCallInfo("otherMethodName",
				eventClassInfo2, new ArrayList<String>());
		eventMethodMap.addEventListener(eventClassInfo2, methodCallInfo2);

		eventMethodMap.removeEventListener(new EventClassInfo(), methodCallInfo);
		Set<EventClassInfo> keys = eventMethodMap.getKeys();
		assertTrue(keys.contains(eventClassInfo1));
		assertTrue(keys.contains(eventClassInfo2));

		eventMethodMap
				.removeEventListener(eventClassInfo2, methodCallInfo2);
		keys = eventMethodMap.getKeys();
		assertTrue(keys.contains(eventClassInfo1));
		assertFalse(keys.contains(eventClassInfo2));

	}

	@Test
	public void testSortByDispatcherKeys() {

		Map<String, EventMethodMap> sorted = eventMethodMap
				.sortByDispatcherKeys();
		assertTrue(sorted.isEmpty());

		eventMethodMap.addEventListener(
				new EventClassInfo(ShowErrorMessageEvent.class),
				new MethodCallInfo("method1", new EventClassInfo(ShowErrorMessageEvent.class),
						Arrays.asList(EventScope.SpringMvpApplication,
								EventScope.AllSpringMvpApplications)));
		eventMethodMap.addEventListener(new EventClassInfo(ShowTrayNotificationEvent.class),
				new MethodCallInfo("method2", new EventClassInfo(ShowTrayNotificationEvent.class),
						Arrays.asList(EventScope.SpringMvpApplication)));
		
		sorted = eventMethodMap.sortByDispatcherKeys();
		assertEquals("Registrations for SpringMvpApplication and AllSpringMvpApplications expected", 2, sorted.entrySet().size());
		EventMethodMap applicationEvents = sorted.get(EventScope.SpringMvpApplication);
		assertEquals(2, applicationEvents.getKeys().size());
		MethodCallInfoList errorMessageMethods = applicationEvents.get(new EventClassInfo(ShowErrorMessageEvent.class));
		assertEquals(1, errorMessageMethods.getMethodCallInfos().size());
	}

}
