/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.dispatcher.model;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class MethodCallInfoListTest {

	private MethodCallInfoList methodCallInfoList;

	@Before
	public void setUp() {
		methodCallInfoList = new MethodCallInfoList();
	}

	@Test
	public void testConstructor() {

		MethodCallInfoList infoList1 = new MethodCallInfoList(null);
		assertTrue(infoList1.isEmpty());
		assertEquals(0, infoList1.getMethodCallInfos().size());

		MethodCallInfo methodCallInfo = new MethodCallInfo("someMethodName",
				new EventClassInfo(SpringMvpEvent.class),
				new ArrayList<String>());

		MethodCallInfoList infoList = new MethodCallInfoList(methodCallInfo);
		assertFalse(infoList.isEmpty());
		assertTrue(infoList.getMethodCallInfos().contains(methodCallInfo));

	}

	@Test
	public void testAddMethodCallInfo() {

		methodCallInfoList.addMethodCallInfo(null);
		assertTrue(methodCallInfoList.isEmpty());

		MethodCallInfo methodCallInfo1 = new MethodCallInfo("someMethodName",
				new EventClassInfo(SpringMvpEvent.class, null),
				new ArrayList<String>());
		methodCallInfoList.addMethodCallInfo(methodCallInfo1);
		assertEquals(1, methodCallInfoList.getMethodCallInfos().size());
		assertTrue(methodCallInfoList.getMethodCallInfos().contains(
				methodCallInfo1));

		MethodCallInfo methodCallInfo2 = new MethodCallInfo(
				"someOtherMethodName", new EventClassInfo(SpringMvpEvent.class,
						null), new ArrayList<String>());
		methodCallInfoList.addMethodCallInfo(methodCallInfo2);
		assertEquals(2, methodCallInfoList.getMethodCallInfos().size());
		assertTrue(methodCallInfoList.getMethodCallInfos().contains(
				methodCallInfo2));

	}

	@Test
	public void testAddAll() {

		// must not cause an exception
		methodCallInfoList.addAll(null);
		methodCallInfoList.addAll(new MethodCallInfoList());

		MethodCallInfo methodCallInfo1 = new MethodCallInfo("someMethodName",
				new EventClassInfo(SpringMvpEvent.class, null),
				new ArrayList<String>());
		MethodCallInfo methodCallInfo2 = new MethodCallInfo(
				"someOtherMethodName", new EventClassInfo(SpringMvpEvent.class,
						null), new ArrayList<String>());
		MethodCallInfo methodCallInfo3 = new MethodCallInfo(
				"theThirdMethodName", new EventClassInfo(SpringMvpEvent.class,
						null), new ArrayList<String>());

		methodCallInfoList.addMethodCallInfo(methodCallInfo1);

		MethodCallInfoList otherList = new MethodCallInfoList();
		otherList.addMethodCallInfo(methodCallInfo2);
		otherList.addMethodCallInfo(methodCallInfo3);

		methodCallInfoList.addAll(otherList);

		assertEquals(3, methodCallInfoList.getMethodCallInfos().size());
		assertTrue(methodCallInfoList.getMethodCallInfos().contains(
				methodCallInfo1));
		assertTrue(methodCallInfoList.getMethodCallInfos().contains(
				methodCallInfo2));
		assertTrue(methodCallInfoList.getMethodCallInfos().contains(
				methodCallInfo3));

	}

	@Test
	public void testRemoveMethodCallInfo() {

		// must not throw an exception
		methodCallInfoList.removeMethodCallInfo(null);

		MethodCallInfo methodCallInfo1 = new MethodCallInfo("someMethodName",
				new EventClassInfo(SpringMvpEvent.class, null), new ArrayList<String>());
		MethodCallInfo methodCallInfo2 = new MethodCallInfo(
				"someOtherMethodName", new EventClassInfo(SpringMvpEvent.class, null),
				new ArrayList<String>());

		// must not throw an exception
		methodCallInfoList.removeMethodCallInfo(methodCallInfo1);

		// add both method call infos
		methodCallInfoList.addMethodCallInfo(methodCallInfo1);
		methodCallInfoList.addMethodCallInfo(methodCallInfo2);

		methodCallInfoList.removeMethodCallInfo(methodCallInfo1);
		assertEquals(1, methodCallInfoList.getMethodCallInfos().size());
		assertTrue(methodCallInfoList.getMethodCallInfos().contains(
				methodCallInfo2));
		assertFalse(methodCallInfoList.getMethodCallInfos().contains(
				methodCallInfo1));
	}

	@Test
	public void testRemoveMethodCallInfos() {

		// must not throw an exception
		methodCallInfoList.removeMethodCallInfos(null);
		methodCallInfoList.removeMethodCallInfos(new MethodCallInfoList());

		/*
		 * fill
		 */
		MethodCallInfo methodCallInfo1 = new MethodCallInfo("someMethodName",
				new EventClassInfo(SpringMvpEvent.class, null), new ArrayList<String>());
		MethodCallInfo methodCallInfo2 = new MethodCallInfo(
				"someOtherMethodName", new EventClassInfo(SpringMvpEvent.class, null),
				new ArrayList<String>());
		MethodCallInfo methodCallInfo3 = new MethodCallInfo(
				"theThirdMethodName", new EventClassInfo(SpringMvpEvent.class, null),
				new ArrayList<String>());

		methodCallInfoList.addMethodCallInfo(methodCallInfo1);

		MethodCallInfoList otherList = new MethodCallInfoList();
		otherList.addMethodCallInfo(methodCallInfo2);
		otherList.addMethodCallInfo(methodCallInfo3);

		methodCallInfoList.addAll(otherList);

		/*
		 * remove and check
		 */
		methodCallInfoList.removeMethodCallInfos(otherList);
		assertEquals(1, methodCallInfoList.getMethodCallInfos().size());
		assertTrue(methodCallInfoList.getMethodCallInfos().contains(
				methodCallInfo1));

	}

}
