/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.locale.impl;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring/translator-test-context.xml" })
public class TranslatorImplTest {

	private static final String INCOMPLETE = "incomplete";
	private static final String COMPLETE = "complete";
	@Autowired
	private Translator translator;
	
    @Test
    public void testSetLocale() {

	// create translator and check default setting
	Translator translator = new TranslatorImpl();
	assertEquals(Locale.ENGLISH, translator.getUsedLocale());

	// test set null value
	translator.setLocale(null);
	assertEquals(Locale.ENGLISH, translator.getUsedLocale());

	translator.setLocale(Locale.GERMAN);
	assertEquals(Locale.GERMAN, translator.getUsedLocale());
    }

    @Test
    public void testSetLanguage() {

	// create translator and check default setting
	Translator translator = new TranslatorImpl();
	assertEquals(Locale.ENGLISH, translator.getUsedLocale());

	// test setting null language
	translator.setLanguage(null);
	assertEquals(Locale.ENGLISH, translator.getUsedLocale());

	// test setting emtpy language
	translator.setLanguage(" ");
	assertEquals(Locale.ENGLISH, translator.getUsedLocale());

	// test setting non existent language
	translator.setLanguage("no_Lang");
	assertEquals(new Locale("no_Lang"), translator.getUsedLocale());

	translator.setLanguage("it");
	assertEquals(Locale.ITALIAN, translator.getUsedLocale());

    }

    @Test
    public void testUninitialisedGetTranslation() {

	Translator translator = new TranslatorImpl();
	assertEquals(TranslatorImpl.MESSAGESOURCE_NOT_SET,
		translator.getTranslation("not.initialised"));

    }
    
    @Test
	public void testGetTranslation(){
    	
    	// check available translations
    	assertEquals(COMPLETE, translator.getTranslation(COMPLETE));
    	translator.setLanguage("de");
    	assertEquals("komplett", translator.getTranslation(COMPLETE));
    	
    	// check not available translation
    	translator.setLanguage("en");
    	assertEquals(INCOMPLETE, translator.getTranslation(INCOMPLETE));
    	translator.setLanguage("de");
    	assertEquals("'incomplete' for 'de' not found", translator.getTranslation(INCOMPLETE));
    	
    	// check value pass
    	translator.setLanguage("en");
    	assertEquals("Hello Adam", translator.getTranslation("hello", new Object[]{"Adam"}));
    	translator.setLanguage("de");
    	assertEquals("Hallo Adam", translator.getTranslation("hello", new Object[]{"Adam"}));
    	
    	// check wrong value pass
    	translator.setLanguage("en");
    	assertEquals("Hello {1}", translator.getTranslation("incomplete.value", new Object[]{"Adam"}));
    	translator.setLanguage("de");
    	assertEquals("Hallo {1}", translator.getTranslation("incomplete.value", new Object[]{"Adam"}));

    	
    }
    
    

}
