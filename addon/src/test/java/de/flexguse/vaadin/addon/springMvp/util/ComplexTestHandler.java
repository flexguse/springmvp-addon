/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.util;

import org.junit.Ignore;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;
import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvents;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowErrorMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowHumanizedMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowTrayNotificationEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowWarningMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasAddedEvent;

/**
 * Just for testing purposes
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@Ignore
public class ComplexTestHandler {

	@HandleSpringMvpEvent
	public void handleMessageEvent(ShowHumanizedMessageEvent event) {
	}

	@HandleSpringMvpEvent
	public void handleNotificationEvent(ShowTrayNotificationEvent event) {
	}

	@HandleSpringMvpEvents({ ShowWarningMessageEvent.class,
			ShowErrorMessageEvent.class })
	public void handleMultipleEvents(SpringMvpEvent event) {
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.SpringMvpApplication,
			EventScope.AllSpringMvpApplications })
	public void handleMessageEventFromMultipleDispatchers(
			ShowHumanizedMessageEvent event) {
	}

	@HandleSpringMvpEvents(value = { ShowHumanizedMessageEvent.class,
			ShowTrayNotificationEvent.class }, eventScopes = {
			EventScope.SpringMvpApplication,
			EventScope.AllSpringMvpApplications })
	public void handleMessageEventsFromMultipleDispatchers(
			ShowHumanizedMessageEvent event) {
	}
	
	@HandleSpringMvpEvent
	public void handleGenericEvent(ModelWasAddedEvent<String> event){}

	public void notAnnotatedHandleMessageEvent(ShowTrayNotificationEvent event) {
	}

}
