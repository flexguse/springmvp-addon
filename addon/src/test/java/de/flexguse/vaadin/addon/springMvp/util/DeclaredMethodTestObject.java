/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.util;

import org.junit.Ignore;

import de.flexguse.vaadin.addon.springMvp.events.messages.ShowErrorMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasUpdatedEvent;

/**
 * This class is only for testing purposes.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@Ignore
public class DeclaredMethodTestObject {

	public void handleEvent(ShowErrorMessageEvent event){}
	
	public void handleEvent1(ModelWasUpdatedEvent<Long> event){}
	
	public void handleEvent2(ModelWasUpdatedEvent<Integer> event){}
	
}
