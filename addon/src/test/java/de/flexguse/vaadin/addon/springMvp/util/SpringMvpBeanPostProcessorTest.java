/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.util;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;

import de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class SpringMvpBeanPostProcessorTest {

	@Test
	public void testConstructors(){
		
		@SuppressWarnings("unused")
		SpringMvpBeanPostProcessor processor;
		
		try{
			processor = new SpringMvpBeanPostProcessor(null);
			fail("null DispatcherManager is not allowed");
		}catch(RuntimeException e){
			// intended to do nothing
		}
		
		try{
			processor = new SpringMvpBeanPostProcessor(null, null);
			fail("all constructor arguments must be given");
		}catch(RuntimeException e){
			// intended to do nothing
		}
		
		try{
			processor = new SpringMvpBeanPostProcessor(mock(DispatcherManager.class), null);
			fail("all constructor arguments must be given");
		}catch(RuntimeException e){
			// intended to do nothing
		}
		
		try{
			processor = new SpringMvpBeanPostProcessor(null, "some.package.name");
			fail("all constructor arguments must be given");
		}catch(RuntimeException e){
			// intended to do nothing
		}
	}
	
	@Test
	public void testBeanPackageMatching(){
		
		DispatcherManager dispatcherManager = mock(DispatcherManager.class);
		SpringMvpBeanPostProcessor processor = new SpringMvpBeanPostProcessor(dispatcherManager, "de.flexguse.vaadin.addon.springMvp.*");
		
		TestHandler testEventHandler = new TestHandler();
		processor.postProcessAfterInitialization(testEventHandler, "testHandler");
		verify(dispatcherManager, times(1)).registerListener(testEventHandler);
		
		TestHandler testEventHandler2 = new TestHandler();
		processor = new SpringMvpBeanPostProcessor(dispatcherManager, "de.flexguse.other.*");
		processor.postProcessAfterInitialization(testEventHandler2, "testHandler");
		verify(dispatcherManager, times(0)).registerListener(testEventHandler2);
		
	}
	
	
	@Test
	public void testSpringMvpApplicationRegistration(){
		
		DispatcherManager dispatcherManager = mock(DispatcherManager.class);
		SpringMvpBeanPostProcessor processor = new SpringMvpBeanPostProcessor(dispatcherManager);
		
		TestApplication testApplication = new TestApplication();
		processor.postProcessAfterInitialization(testApplication, "testApplication");
		verify(dispatcherManager, times(1)).registerListener(testApplication);
		
	}
}
