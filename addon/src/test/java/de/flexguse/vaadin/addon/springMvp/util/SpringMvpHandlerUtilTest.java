/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import static org.mockito.Mockito.*;

import java.util.Set;

import org.junit.Test;

import de.flexguse.vaadin.addon.springMvp.dispatcher.model.EventClassInfo;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.EventMethodMap;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.MethodCallInfo;
import de.flexguse.vaadin.addon.springMvp.dispatcher.model.MethodCallInfoList;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowErrorMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowHumanizedMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowTrayNotificationEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowWarningMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasAddedEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasUpdatedEvent;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class SpringMvpHandlerUtilTest {

	private SpringMvpHandlerUtil springMvpHandlerUtil = new SpringMvpHandlerUtil();

	@Test
	public void testProcessNullObject() {

		EventMethodMap eventMethodMap = springMvpHandlerUtil
				.getEventMethodMap(null);
		assertNull(eventMethodMap);

	}

	@Test
	public void testProcessNonAnnotatedObject() {

		EventMethodMap eventMethodMap = springMvpHandlerUtil
				.getEventMethodMap(new String());
		assertTrue(eventMethodMap.isEmpty());

	}

	@Test
	public void testProcessHandleSpringMvpEventAnnotated() {

		TestHandler testHandler = new TestHandler();

		EventMethodMap eventMethodMap = springMvpHandlerUtil
				.getEventMethodMap(testHandler);
		assertFalse(eventMethodMap.isEmpty());

		Set<EventClassInfo> eventClassNames = eventMethodMap.getKeys();
		assertTrue(eventClassNames.contains(new EventClassInfo(ShowTrayNotificationEvent.class
				)));
		assertTrue(eventClassNames.contains(new EventClassInfo(ShowHumanizedMessageEvent.class)));
				

		MethodCallInfoList notificationCallInfoList = eventMethodMap
				.get(new EventClassInfo(ShowTrayNotificationEvent.class));
		MethodCallInfo notificationCallInfo = notificationCallInfoList
				.getMethodCallInfos().get(0);
		assertEquals(ShowTrayNotificationEvent.class, notificationCallInfo
				.getEventClassInfo().getEventClass());
		assertEquals("handleEvent", notificationCallInfo.getMethodName());

		MethodCallInfoList messageCallInfoList = eventMethodMap
				.get(new EventClassInfo(ShowHumanizedMessageEvent.class));
		MethodCallInfo messageCallInfo = messageCallInfoList
				.getMethodCallInfos().get(0);
		assertEquals(ShowHumanizedMessageEvent.class, messageCallInfo
				.getEventClassInfo().getEventClass());
		assertEquals("handleEvent", notificationCallInfo.getMethodName());

	}

	@Test
	public void testProcessComplexHandler() {

		ComplexTestHandler complexTestHandler = new ComplexTestHandler();

		EventMethodMap eventMethodMap = springMvpHandlerUtil
				.getEventMethodMap(complexTestHandler);

		assertFalse(eventMethodMap.isEmpty());

		Set<EventClassInfo> eventClassNames = eventMethodMap.getKeys();

		assertEquals(5, eventClassNames.size());
		assertTrue(eventClassNames.contains(new EventClassInfo(ShowHumanizedMessageEvent.class)));
		assertTrue(eventClassNames.contains(new EventClassInfo(ShowTrayNotificationEvent.class)));
				
		assertTrue(eventClassNames.contains(new EventClassInfo(ShowWarningMessageEvent.class)));
				
		assertTrue(eventClassNames.contains(new EventClassInfo(ShowErrorMessageEvent.class)));
				
		assertTrue(eventClassNames.contains(new EventClassInfo(ModelWasAddedEvent.class, String.class)));

	}

	@Test
	public void testCreateMethodCallInfo() throws NoSuchMethodException {

		// test null exceptions
		try {
			springMvpHandlerUtil.createMethodCallInfo(null, null);
			fail("exception expected");
		} catch (RuntimeException e) {
			// expected exception
		}

		// test null exceptions
		try {
			springMvpHandlerUtil.createMethodCallInfo(null,
					new EventClassInfo());
			fail("exception expected");
		} catch (RuntimeException e) {
			// expected exception
		}

		// test MethodCallInfo creation for method which has invalid method
		// arguments
		assertNull(
				"The method has no SpringMvpEvent argument, so no MethodCallInfo must be created",
				springMvpHandlerUtil.createMethodCallInfo(getClass()
						.getDeclaredMethod("testCreateMethodCallInfo"), null));

		// test method call info creation for method which is not annotated
		// a method call info must be returned because
		ComplexTestHandler complexTestHandler = new ComplexTestHandler();

		MethodCallInfo methodCallInfo = springMvpHandlerUtil
				.createMethodCallInfo(
						complexTestHandler.getClass().getDeclaredMethod(
								"notAnnotatedHandleMessageEvent",
								ShowTrayNotificationEvent.class), null);
		assertEquals(0, methodCallInfo.getDispatcherKeys().size());

		// test wrong method argument (Event type)
		methodCallInfo = springMvpHandlerUtil.createMethodCallInfo(
				complexTestHandler.getClass().getDeclaredMethod(
						"notAnnotatedHandleMessageEvent",
						ShowTrayNotificationEvent.class), new EventClassInfo(
						ShowErrorMessageEvent.class));
		assertNull(
				"MethodCallInfo must not be created because of wrong event type",
				methodCallInfo);

		// test EventScope merging
		methodCallInfo = springMvpHandlerUtil.createMethodCallInfo(
				complexTestHandler.getClass().getDeclaredMethod(
						"handleMessageEventFromMultipleDispatchers",
						ShowHumanizedMessageEvent.class), null);
		assertEquals(2, methodCallInfo.getDispatcherKeys().size());

		methodCallInfo = springMvpHandlerUtil.createMethodCallInfo(
				complexTestHandler.getClass().getDeclaredMethod(
						"handleMessageEventsFromMultipleDispatchers",
						ShowHumanizedMessageEvent.class), null);
		assertEquals(2, methodCallInfo.getDispatcherKeys().size());

	}

	@Test
	public void testCreateMethodCallInfoForGenerics()
			throws NoSuchMethodException, SecurityException {

		/**
		 * test MethodCallInfo creation for generics
		 */
		ComplexTestHandler complexTestHandler = new ComplexTestHandler();
		MethodCallInfo methodCallInfo = springMvpHandlerUtil
				.createMethodCallInfo(
						complexTestHandler.getClass().getDeclaredMethod(
								"handleGenericEvent", ModelWasAddedEvent.class),
						null);

		assertEquals(
				"de.flexguse.vaadin.addon.springMvp.events.model.ModelWasAddedEvent",
				methodCallInfo.getEventClassInfo().getEventClass().getName());
		assertEquals("java.lang.String", methodCallInfo.getEventClassInfo()
				.getEventGenericClass().getName());

	}
	
	@Test
	public void testGetDeclaredMethod(){
		
		Object eventListener = new DeclaredMethodTestObject();
		
		assertNull(springMvpHandlerUtil.getDeclareMethod(null, null, null));
		assertNull(springMvpHandlerUtil.getDeclareMethod(eventListener, null, null));
		assertNull(springMvpHandlerUtil.getDeclareMethod(eventListener, "someMethod", null));
		assertNull(springMvpHandlerUtil.getDeclareMethod(eventListener, "", ShowErrorMessageEvent.class));
		
		assertNull(springMvpHandlerUtil.getDeclareMethod(eventListener, "nonexistent", ShowErrorMessageEvent.class));
		assertNull(springMvpHandlerUtil.getDeclareMethod(eventListener, "handleEvent1", ShowTrayNotificationEvent.class));
		assertNotNull(springMvpHandlerUtil.getDeclareMethod(eventListener, "handleEvent2", ModelWasUpdatedEvent.class));
		
		/*
		 * test getting methods from mocked objects
		 */
		eventListener = mock(DeclaredMethodTestObject.class);
//		assertNull(springMvpHandlerUtil.getDeclareMethod(eventListener, "handleEvent1", new EventClassInfo(ShowTrayNotificationEvent.class)));
//		assertNotNull(springMvpHandlerUtil.getDeclareMethod(eventListener, "handleEvent2", new EventClassInfo(ModelWasUpdatedEvent.class)));
		
		
	}
	
	@Test
	public void testGetEventClassInfo(){
		
		EventClassInfo eventClassInfo = springMvpHandlerUtil.getEventClassInfo(new ShowTrayNotificationEvent(this, "some title", "some info"));
		EventClassInfo expectedEventClassInfo = new EventClassInfo(ShowTrayNotificationEvent.class);
		assertEquals(expectedEventClassInfo, eventClassInfo);
		
		ModelWasUpdatedEvent<String> event = new ModelWasUpdatedEvent<String>(this, String.class);
		eventClassInfo = springMvpHandlerUtil.getEventClassInfo(event);
		expectedEventClassInfo = new EventClassInfo(ModelWasUpdatedEvent.class, String.class);
		assertEquals(expectedEventClassInfo, eventClassInfo);
		
	}
}
