/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.util;

import org.junit.Ignore;
import org.springframework.web.context.ConfigurableWebApplicationContext;

import de.flexguse.vaadin.addon.springMvp.application.SpringMvpVaadinApplication;

/**
 * Just used in {@link SpringMvpBeanPostProcessorTest}.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@Ignore
public class TestApplication extends SpringMvpVaadinApplication {

	private static final long serialVersionUID = -6162960760511990215L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.dellroad.stuff.vaadin.SpringContextApplication#initSpringApplication
	 * (org.springframework.web.context.ConfigurableWebApplicationContext)
	 */
	@Override
	protected void initSpringApplication(ConfigurableWebApplicationContext arg0) {
		// intended to do nothing

	}

}
