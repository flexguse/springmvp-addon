/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.util;

import org.junit.Ignore;

import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowHumanizedMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowTrayNotificationEvent;

/**
 * For use in {@link SpringMvpBeanPostProcessor} only.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@Ignore
public class TestHandler {

	@HandleSpringMvpEvent
	public void handleEvent(ShowTrayNotificationEvent event){
	}
	
	@HandleSpringMvpEvent
	public void handleEvent(ShowHumanizedMessageEvent event){
		
	}
	
}
