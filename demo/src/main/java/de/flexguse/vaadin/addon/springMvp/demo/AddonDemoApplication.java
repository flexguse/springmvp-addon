package de.flexguse.vaadin.addon.springMvp.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.ConfigurableWebApplicationContext;

import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import de.flexguse.vaadin.addon.springMvp.application.SpringMvpVaadinApplication;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.AddonDemoMenu;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.logging.EventMonitor;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.ShowShoppingCartViewEvent;
import de.flexguse.vaadin.addon.springMvp.locale.Translator;
import de.flexguse.vaadin.addon.springMvp.view.View;

/**
 * The Application's "main" class.
 * 
 * @author Christoph Guse, info@flexguse.de
 */
@SuppressWarnings("serial")
public class AddonDemoApplication extends SpringMvpVaadinApplication implements
		View<AddonDemoApplicationPresenter> {
	private Window window;

	private AddonDemoApplicationPresenter presenter;

	@Autowired
	private AddonDemoMenu menu;

	@Autowired
	private EventMonitor eventMonitor;

	@Autowired(required = true)
	private Translator translator;

	private VerticalLayout viewLayout;

	/**
	 * If you want to autowire the Presenter, add @Autowired to the setter so
	 * the registration of the View is done in this method.
	 * <p>
	 * Normally it would be a good idea to configure the presenter in the Spring
	 * xml configuration, this does not work for the Application.
	 * </p>
	 */
	@Autowired
	public void setPresenter(AddonDemoApplicationPresenter presenter) {
		this.presenter = presenter;
		presenter.setView(this);
	}

	public void setView(AbstractComponent component) {

		if (component != null) {
			component.setSizeFull();
			viewLayout.removeAllComponents();
			viewLayout.addComponent(component);
		}
	}

	@Override
	protected void initSpringApplication(ConfigurableWebApplicationContext arg0) {

		setTheme("spring-mvp");

		window = new Window("SpringMvpAddon Demo Application");
		setMainWindow(window);

		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		mainLayout.setSpacing(true);

		/*
		 * create header for application.
		 */
		HorizontalLayout headerLabelLayout = new HorizontalLayout();
		headerLabelLayout.setWidth("100%");
		headerLabelLayout.setSpacing(true);

		// add label
		Label headerLabel = new Label(
				"flexguse spring-mvp Vaadin Addon Demo Application");
		headerLabel.setStyleName("headertext");
		headerLabel.setHeight("30px");
		headerLabelLayout.addComponent(headerLabel);

		// add info text
		Label infoText = new Label(
				translator.getTranslation("header.info.text"));
		infoText.setStyleName("infotext");
		headerLabelLayout.addComponent(infoText);

		mainLayout.addComponent(headerLabelLayout);

		mainLayout.addComponent(createMenu());

		viewLayout = new VerticalLayout();
		viewLayout.setSpacing(true);
		viewLayout.setSizeFull();
		mainLayout.addComponent(viewLayout);

		eventMonitor.setWidth("100%");
		eventMonitor.setHeight("250px");
		mainLayout.addComponent(eventMonitor);

		mainLayout.setExpandRatio(viewLayout, 1f);

		window.addComponent(mainLayout);
		window.getContent().setSizeFull();

		presenter.dispatchEvent(new ShowShoppingCartViewEvent(this));

	}

	/**
	 * This helper method creates a horizontal component including menu-bar and
	 * application-close button.
	 * 
	 * @return
	 */
	private HorizontalLayout createMenu() {

		HorizontalLayout header = new HorizontalLayout();
		header.setWidth("100%");
		header.setSpacing(true);

		// add menu bar
		menu.setWidth("100%");
		header.addComponent(menu);

		// header.setExpandRatio(menu, 1f);
		header.setComponentAlignment(menu, Alignment.MIDDLE_LEFT);

		return header;
	}

}
