/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.articles.ArticlesManagement;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.shoppingcart.ShoppingCartManagement;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.ShowArticlesViewEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.ShowShoppingCartViewEvent;
import de.flexguse.vaadin.addon.springMvp.presenter.AbstractPresenter;

/**
 * This Presenter contains logic for the {@link AddonDemoApplication}.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class AddonDemoApplicationPresenter extends
		AbstractPresenter<AddonDemoApplication> {

	@Autowired
	private ApplicationContext springContext;

	@HandleSpringMvpEvent
	public void handleShowShoppingCatView(ShowShoppingCartViewEvent event) {
		getView().setView(springContext.getBean(ShoppingCartManagement.class));
	}

	@HandleSpringMvpEvent
	public void handleShowArticlesView(ShowArticlesViewEvent event) {
		getView().setView(springContext.getBean(ArticlesManagement.class));
	}
}
