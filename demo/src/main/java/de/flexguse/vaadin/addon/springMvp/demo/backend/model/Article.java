/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.backend.model;

import javax.validation.constraints.NotNull;

import de.flexguse.vaadin.addon.springMvp.demo.validation.MinPrice;

/**
 * A {@link Model} instance which holds Article information (just a normal Bean
 * or ValueObject).<br/>
 * <br/>
 * One article which can be added to the {@link ShoppingCart}.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class Article extends Model {

	private static final long serialVersionUID = -7496510354768365661L;

	public static final String NUMBER_EMPTY_EXCEPTION = "{article.number.empty}";
	public static final String NAME_EMPTY_EXCEPTION = "{article.name.empty}";
	public static final String PRICE_TOO_LOW = "{article.price.low}";

	public static final String ATTRIBUTE_NUMBER = "number";
	public static final String ATTRIBUTE_NAME = "name";
	public static final String ATTRIBUTE_DESCRIPTION = "description";
	public static final String ATTRIBUTE_PRICE = "price";

	private boolean active = true;

	@NotNull(message = NUMBER_EMPTY_EXCEPTION)
	private String number;

	@NotNull(message = NAME_EMPTY_EXCEPTION)
	private String name;

	private String description;

	@NotNull(message = PRICE_TOO_LOW)
	@MinPrice(value = 0.01f, message = PRICE_TOO_LOW)
	private Float price = 0f;

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Article other = (Article) obj;
		if (active != other.active)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		return true;
	}

}
