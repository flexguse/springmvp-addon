/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.backend.model;

import java.io.Serializable;
import java.util.UUID;

/**
 * Abstract object which holds information which is relevant for all Beans or
 * ValueObjects.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public abstract class Model implements Serializable {

	private static final long serialVersionUID = 1880854840610591060L;

	public static final String ATTRIBUTE_ID = "id";

	protected String id = UUID.randomUUID().toString();

	public String getId() {
		return id;
	}
}
