/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.backend.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ShoppingCart extends Model {

	private static final long serialVersionUID = 3467936309599253613L;

	/**
	 * The ResourceBundle key for the error which is shown if no cart name was
	 * given.
	 */
	public static final String EMPTY_NAME_EXCEPTION = "cart.empty.name";

	public static final String ATTRIBUTE_NAME = "name";
	public static final String ATTRIBUTE_PRICE = "price";
	public static final String ATTRIBUTE_ARTICLES = "articles";

	@NotNull(message = EMPTY_NAME_EXCEPTION)
	@NotEmpty(message = EMPTY_NAME_EXCEPTION)
	private String name;

	private List<Article> articles = new ArrayList<Article>();

	private Float price = 0.0f;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
		price = calculatePrice();
	}

	public Float getPrice() {
		return price;
	}

	/**
	 * Convenience method to add a single article.
	 * 
	 * @param article
	 */
	public void addArticle(Article article) {

		if (article != null) {
			price = price + article.getPrice();
			articles.add(article);
		}

	}

	/**
	 * Convenience method to remove a single article.
	 * 
	 * @param article
	 */
	public void removeArticle(Article article) {

		if (article != null) {

			if (articles.contains(article)) {
				articles.remove(article);
				price = price - article.getPrice();
			}

		}

	}

	/**
	 * This helper method calculates the price for all articles in the shopping
	 * cart.
	 * 
	 * @return
	 */
	private Float calculatePrice() {

		Float result = Float.valueOf(0);

		if (articles != null) {
			for (Article article : articles) {

				if (article.getPrice() != null) {
					result = result + article.getPrice();
				}

			}
		}

		return result;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((articles == null) ? 0 : articles.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShoppingCart other = (ShoppingCart) obj;
		if (articles == null) {
			if (other.articles != null)
				return false;
		} else if (!articles.equals(other.articles))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		return true;
	}

}
