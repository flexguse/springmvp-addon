/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.backend.service;

import java.util.List;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface ArticleService {

	/**
	 * Deletes an {@link Article}
	 * 
	 * @param article
	 *            the Article to delete
	 */
	void deleteArticle(Article article);

	/**
	 * Creates or updates an {@link Article}.
	 * 
	 * @param article
	 * @return
	 */
	Article saveArticle(Article article);

	/**
	 * Gets all existing articles.
	 * 
	 * @return
	 */
	List<Article> getAllArticles();

	/**
	 * Gets all articles which have the status 'active'.
	 * 
	 * @return
	 */
	List<Article> getAllActiveArticles();

}
