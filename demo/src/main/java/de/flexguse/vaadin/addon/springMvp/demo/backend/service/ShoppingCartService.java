/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.backend.service;

import java.util.List;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.ShoppingCart;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface ShoppingCartService {

	/**
	 * Deletes a shopping cart.
	 * 
	 * @param shoppingCart
	 */
	void deleteShoppingCart(ShoppingCart shoppingCart);

	/**
	 * Saves or updates the Shopping cart.
	 * 
	 * @param shoppingCart
	 * @return
	 */
	ShoppingCart saveShoppingCart(ShoppingCart shoppingCart);

	/**
	 * Gets all ShoppingCarts.
	 * 
	 * @return
	 */
	List<ShoppingCart> getAllShoppingCarts();

}
