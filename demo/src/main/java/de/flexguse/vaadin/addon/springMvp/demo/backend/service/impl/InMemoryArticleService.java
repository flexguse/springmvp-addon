/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.backend.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;
import de.flexguse.vaadin.addon.springMvp.demo.backend.service.ArticleService;

/**
 * <p>
 * This implementation of the {@link ArticleService} persists the Articles in
 * memory only. This means with an webserver restart all articles are lost.
 * </p>
 * 
 * Validation is not done in this service, it is assumed the given Articles are
 * validated by the UI. In a real world application the bean validation should
 * be done in the service AND the user interface.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class InMemoryArticleService implements ArticleService {

	private Map<String, Article> articleMap = new HashMap<String, Article>();

	/**
	 * In the constructor some initial articles are created.
	 */
	public InMemoryArticleService() {

		createDummyEntries();

	}

	/**
	 * This helper method creates some dummy Articles in the service.
	 */
	private void createDummyEntries() {

		Article persil = new Article();
		persil.setActive(true);
		persil.setNumber("Wash-1");
		persil.setName("Persil washing powder");
		persil.setDescription("Premium concentrated washing powder. Your laundry will be whiter than white.");
		persil.setPrice(7.99f);
		saveArticle(persil);

		Article pizza = new Article();
		pizza.setActive(true);
		pizza.setNumber("Piz-4");
		pizza.setName("Pizza quatro stagioni");
		pizza.setDescription("The best pizza in your life. Just put it into the oven and bake it.");
		pizza.setPrice(1.99f);
		saveArticle(pizza);

		Article toothbrush = new Article();
		toothbrush.setActive(true);
		toothbrush.setNumber("Brush-8");
		toothbrush.setName("Electrical Toothbrush");
		toothbrush
				.setDescription("Charm your tooth and use this sonic toothbrush.");
		toothbrush.setPrice(49.99f);
		saveArticle(toothbrush);

	}

	public InMemoryArticleService(boolean createDummyEntries) {
		if (createDummyEntries) {
			createDummyEntries();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.flexguse.vaadin.addon.springMvp.backend.service.ArticleService#
	 * deleteArticle(de.flexguse.vaadin.addon.springMvp.backend.model.Article)
	 */
	@Override
	public void deleteArticle(Article article) {

		if (article != null) {
			articleMap.remove(article.getId());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.vaadin.addon.springMvp.backend.service.ArticleService#saveArticle
	 * (de.flexguse.vaadin.addon.springMvp.backend.model.Article)
	 */
	@Override
	public Article saveArticle(Article article) {

		if (article != null) {
			articleMap.put(article.getId(), article);
		}

		return article;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.flexguse.vaadin.addon.springMvp.backend.service.ArticleService#
	 * getAllArticles()
	 */
	@Override
	public List<Article> getAllArticles() {
		return new ArrayList<Article>(articleMap.values());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.flexguse.vaadin.addon.springMvp.backend.service.ArticleService#
	 * getAllActiveArticles()
	 */
	@Override
	public List<Article> getAllActiveArticles() {

		List<Article> result = new ArrayList<Article>();

		for (Article article : articleMap.values()) {
			if (article.isActive()) {
				result.add(article);
			}
		}

		return result;
	}

}
