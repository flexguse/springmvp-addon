/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.backend.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.ShoppingCart;
import de.flexguse.vaadin.addon.springMvp.demo.backend.service.ShoppingCartService;

/**
 * <p>
 * This implementation of {@link ShoppingCartService} persists in memory only.
 * All {@link ShoppingCart}s are lost with webserver restart.
 * </p>
 * 
 * In this implementation no validation is done for the given shoppingCarts. In
 * a real world application validation should be done in the UI AND the service
 * implementation.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class InMemoryShoppingCartService implements ShoppingCartService {

	private Map<String, ShoppingCart> shoppingCartMap = new HashMap<String, ShoppingCart>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.vaadin.addon.springMvp.backend.service.ShoppingCartService
	 * #deleteShoppingCart
	 * (de.flexguse.vaadin.addon.springMvp.backend.model.ShoppingCart)
	 */
	@Override
	public void deleteShoppingCart(ShoppingCart shoppingCart) {

		if (shoppingCart != null) {
			shoppingCartMap.remove(shoppingCart.getId());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.vaadin.addon.springMvp.backend.service.ShoppingCartService
	 * #saveShoppingCart
	 * (de.flexguse.vaadin.addon.springMvp.backend.model.ShoppingCart)
	 */
	@Override
	public ShoppingCart saveShoppingCart(ShoppingCart shoppingCart) {

		if (shoppingCart != null) {
			shoppingCartMap.put(shoppingCart.getId(), shoppingCart);
		}

		return shoppingCart;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.vaadin.addon.springMvp.backend.service.ShoppingCartService
	 * #getAllShoppingCarts()
	 */
	@Override
	public List<ShoppingCart> getAllShoppingCarts() {

		return new ArrayList<ShoppingCart>(shoppingCartMap.values());
	}

}
