/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.ui.MenuBar;

import de.flexguse.vaadin.addon.springMvp.demo.ui.events.OpenMessageEditorEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.ShowArticlesViewEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.ShowShoppingCartViewEvent;
import de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * Menu component for the demo application.
 * <br/>
 * <img src="doc-files/addon-demo-menu.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class AddonDemoMenu extends MenuBar implements InitializingBean {

	private static final long serialVersionUID = 5029000385967208823L;

	@Autowired
	private DispatcherManager eventDispatcherManager;

	@Autowired
	private Translator translator;

	@Override
	public void afterPropertiesSet() throws Exception {

		addItem(translator.getTranslation("menu.shoppingcart"),
				new DemoMenuDispatchingItemCommand(
						new ShowShoppingCartViewEvent(this)));

		addItem(translator.getTranslation("menu.articles"),
				new DemoMenuDispatchingItemCommand(new ShowArticlesViewEvent(
						this)));

		createMessagesMenuItems();
	}

	/**
	 * This helper method creates the menuItem for messages with all submenu
	 * items.
	 * 
	 * @return
	 */
	private void createMessagesMenuItems() {

		// create menubar items
		MenuItem messagesItem = addItem(
				translator.getTranslation("menu.messages"), null);

		messagesItem.addItem(translator.getTranslation("menu.show.message"),
				new DemoMenuDispatchingItemCommand(new OpenMessageEditorEvent(
						this)));

	}

	/**
	 * Helper class to dispatch an event by a menu item.
	 * 
	 * @author Christoph Guse, info@flexguse.de
	 * 
	 */
	public class DemoMenuDispatchingItemCommand implements Command {

		private static final long serialVersionUID = 4959405323401333926L;

		private SpringMvpEvent event;

		public DemoMenuDispatchingItemCommand(SpringMvpEvent event) {
			this.event = event;
		}

		@Override
		public void menuSelected(MenuItem selectedItem) {
			eventDispatcherManager.dispatchEvent(event);

		}

	}

}
