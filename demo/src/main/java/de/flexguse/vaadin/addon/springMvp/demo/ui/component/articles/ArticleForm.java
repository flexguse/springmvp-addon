/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.articles;

import java.util.ArrayList;
import java.util.Collection;

import com.vaadin.addon.beanvalidation.BeanValidationForm;
import com.vaadin.data.util.BeanItem;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;

/**
 * Input form for {@link Article}. <br/>
 * <img src="doc-files/article-form.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ArticleForm extends BeanValidationForm<Article> {

	private static final long serialVersionUID = -2428270616028659391L;

	private Collection<String> visibleInputFields;

	private Article article;

	public ArticleForm() {
		super(Article.class);

		visibleInputFields = new ArrayList<String>();
		visibleInputFields.add(Article.ATTRIBUTE_NUMBER);
		visibleInputFields.add(Article.ATTRIBUTE_NAME);
		visibleInputFields.add(Article.ATTRIBUTE_DESCRIPTION);
		visibleInputFields.add(Article.ATTRIBUTE_PRICE);
	}

	public void setArticle(Article article) {
		
		if(article ==  null){
			article = new Article();
		}
		
		this.article = article;
		setItemDataSource(new BeanItem<Article>(article), visibleInputFields);
	}

	public Article getArticle() {
		return article;
	}

}
