/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.articles;

import it.portus.addon.numberfield.SpinnerNumberField;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;
import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * The {@link FormFieldFactory} which is used to populate the
 * {@link ArticleForm} fields. Matches the {@link ArticleForm} bean.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ArticleFormFieldFactory extends DefaultFieldFactory implements
		FormFieldFactory {

	private static final String NULL_REPRESENTATION = "";

	private static final long serialVersionUID = -6249900357038085684L;

	@Autowired
	private Translator translator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.ui.FormFieldFactory#createField(com.vaadin.data.Item,
	 * java.lang.Object, com.vaadin.ui.Component)
	 */
	@Override
	public Field createField(Item item, Object propertyId, Component uiContext) {

		Field field = null;

		if (Article.ATTRIBUTE_DESCRIPTION.equals(propertyId)) {

			TextArea textArea = new TextArea();
			textArea.setNullRepresentation(NULL_REPRESENTATION);
			textArea.setRows(10);
			textArea.setCaption(translator
					.getTranslation("articles.description"));
			field = textArea;

		} else if (Article.ATTRIBUTE_NUMBER.equals(propertyId)) {
			field = super.createField(item, propertyId, uiContext);
			field.setCaption(translator.getTranslation("articles.number"));
		} else if (Article.ATTRIBUTE_NAME.equals(propertyId)) {
			field = super.createField(item, propertyId, uiContext);
			field.setCaption(translator.getTranslation("articles.name"));
		} else if (Article.ATTRIBUTE_PRICE.equals(propertyId)) {

			SpinnerNumberField<Float> numberField = new SpinnerNumberField<Float>(
					Float.class);
			numberField.setAllowNegative(false);
			numberField.setAllowNull(false);
			field = numberField;
			field.setCaption(translator.getTranslation("articles.price"));
		}

		else {
			field = super.createField(item, propertyId, uiContext);
		}

		field.setWidth("100%");

		if (field instanceof TextField) {
			((TextField) field).setNullRepresentation(NULL_REPRESENTATION);
		}

		return field;
	}

}
