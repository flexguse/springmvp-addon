/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.articles;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.Property;
import com.vaadin.data.util.BeanContainer;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;
import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.format.PricePropertyFormatter;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.model.ModelList;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasAddedEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasDeletedEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasUpdatedEvent;

/**
 * <p>
 * This UI component displays a list of {@link Article}s. The shown
 * {@link Article} objects must be set into the component by using setModels(List articles).
 * </p>
 * <img src="doc-files/article-list.png" />
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ArticlesList extends ModelList<Article> implements
		InitializingBean {

	private static final long serialVersionUID = 4938029917583261178L;

	@Autowired
	private PricePropertyFormatter priceFormatter;

	/**
	 * Constructor doing some initialisation work.
	 */
	public ArticlesList() {
		super(new String[] { Article.ATTRIBUTE_NAME, Article.ATTRIBUTE_PRICE });
	}

	@Override
	public void afterPropertiesSet() throws Exception {

		setImmediate(true);
		setColumnCollapsingAllowed(true);
		setSelectable(true);
		setMultiSelect(false);

		beanContainer = new BeanContainer<String, Article>(Article.class);
		beanContainer.setBeanIdProperty(Article.ATTRIBUTE_ID);
		setContainerDataSource(beanContainer);
		setVisibleColumns(visibleColumns);

		setColumnHeader(Article.ATTRIBUTE_NAME,
				translator.getTranslation("articles.list.header.name"));
		setColumnHeader(Article.ATTRIBUTE_PRICE,
				translator.getTranslation("articles.list.header.price"));

	}

	@Override
	protected String formatPropertyValue(Object rowId, Object colId,
			Property property) {

		if (Article.ATTRIBUTE_PRICE.equals(colId)) {
			return priceFormatter.format(property.getValue());
		}

		return super.formatPropertyValue(rowId, colId, property);
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications })
	public void handleArticleAddedEvent(ModelWasAddedEvent<Article> event) {
		addModel(event.getAddedModel());
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications })
	public void handleArticleUpdatedEvent(ModelWasUpdatedEvent<Article> event) {
		updateModel(event.getUpdatedModel());
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications })
	public void handleArticleDeletedEvent(ModelWasDeletedEvent<Article> event) {
		removeModel(event.getDeletedModel());
	}

}
