/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.articles;

import org.springframework.beans.factory.annotation.Autowired;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.model.ModelManagement;
import de.flexguse.vaadin.addon.springMvp.view.View;

/**
 * <p>
 * This component contains all logic needed to provide a full {@link Article}
 * management.
 * </p>
 * <img src="doc-files/article-management.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ArticlesManagement extends ModelManagement<Article> implements
		View<ArticlesManagementPresenter> {

	private static final long serialVersionUID = -6640668364390758750L;

	/**
	 * @param articlesList
	 *            the articlesList to set
	 */
	@Autowired
	public void setArticlesList(ArticlesList articlesList) {
		modelList = articlesList;
	}

	@Override
	public void setPresenter(ArticlesManagementPresenter presenter) {

		this.presenter = presenter;
		presenter.setView(this);
	}

}
