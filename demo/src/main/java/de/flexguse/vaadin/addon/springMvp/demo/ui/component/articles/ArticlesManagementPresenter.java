/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.articles;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;
import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.model.ModelManagementPresenter;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.ModelWasChosenEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasDeletedEvent;

/**
 * This presenter contains some logic which is specific for
 * {@link ArticlesManagement}.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ArticlesManagementPresenter extends
		ModelManagementPresenter<Article> {

	public ArticlesManagementPresenter() {
		super(Article.class);
	}

	public void setView(ArticlesManagement view) {
		super.setView(view);

		// register the Handler for the articles list
		addModelsListHandler();
		addModelSelectionListener();
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications })
	public void handleArticleDeletedEvent(ModelWasDeletedEvent<Article> event) {
		handleModelWasChosen(new ModelWasChosenEvent<Article>(null,
				Article.class));
	}

}
