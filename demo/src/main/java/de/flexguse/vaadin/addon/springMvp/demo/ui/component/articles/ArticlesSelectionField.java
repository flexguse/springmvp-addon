/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.articles;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.addon.customfield.CustomField;

import com.vaadin.data.Property;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.OpenModelEditorEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.service.UIArticlesService;
import de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager;
import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * <p>
 * This input field was created to be able to select multiple {@link Article}s.
 * For a better overview the presented {@link ArticlesList} can be searched. If
 * an {@link Article} is missing, a new one can be created.
 * </p>
 * <img src="doc-files/articles-selection-field.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ArticlesSelectionField extends CustomField implements
		InitializingBean {

	private static final long serialVersionUID = 1375239320070273773L;

	@Autowired
	private Translator translator;

	@Autowired
	private ArticlesList articlesList;

	@Autowired
	private UIArticlesService uiArticlesService;

	@Autowired
	private DispatcherManager dispatcherManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.vaadin.addon.customfield.CustomField#getType()
	 */
	@Override
	public Class<?> getType() {
		return List.class;
	}

	@Override
	public void afterPropertiesSet() throws Exception {

		/**
		 * create layout
		 */
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		mainLayout.setSpacing(true);
		setCompositionRoot(mainLayout);

		/*
		 * create search bar
		 */
		HorizontalLayout searchBar = new HorizontalLayout();
		searchBar.setWidth("100%");
		searchBar.setSpacing(true);

		TextField searchInput = new TextField();
		searchInput.setTextChangeEventMode(TextChangeEventMode.LAZY);
		searchInput.setTextChangeTimeout(200);
		searchInput.setWidth("100%");
		searchInput.setInputPrompt(translator
				.getTranslation("article.search.input.prompt"));
		searchInput.addListener(new TextChangeListener() {

			private static final long serialVersionUID = -1819901126600696571L;

			@SuppressWarnings("unchecked")
			@Override
			public void textChange(TextChangeEvent event) {

				BeanContainer<String, Article> beanContainer = (BeanContainer<String, Article>) articlesList
						.getContainerDataSource();
				beanContainer.removeAllContainerFilters();
				beanContainer.addContainerFilter(Article.ATTRIBUTE_NAME,
						event.getText(), true, false);

			}
		});
		searchBar.addComponent(searchInput);

		Button createNewArticleButton = new Button(
				translator.getTranslation("articles.button.addNew"));
		createNewArticleButton.addListener(new Button.ClickListener() {

			private static final long serialVersionUID = 7603081138367759923L;

			@Override
			public void buttonClick(ClickEvent event) {
				OpenModelEditorEvent<Article> openEvent = new OpenModelEditorEvent<Article>(
						this, Article.class);
				openEvent.setNewModel(true);
				dispatcherManager.dispatchEvent(openEvent);
			}
		});

		searchBar.addComponent(createNewArticleButton);
		searchBar.setExpandRatio(searchInput, 1f);
		mainLayout.addComponent(searchBar);

		/*
		 * add articles list
		 */
		articlesList.setSizeFull();
		articlesList.setImmediate(true);
		articlesList.setMultiSelect(true);
		articlesList.setModels(uiArticlesService.getAllModels());
		mainLayout.addComponent(articlesList);
		mainLayout.setExpandRatio(articlesList, 1f);

	}

	/**
	 * In the commit the selected models in the articles list are put into the
	 * property datasource.
	 */
	@Override
	public void commit() throws SourceException, InvalidValueException {

		getPropertyDataSource().setValue(articlesList.getSelectedModels());
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setPropertyDataSource(Property newDataSource) {
		super.setPropertyDataSource(newDataSource);

		if (newDataSource != null) {

			/*
			 * expected to get set of articles
			 */
			Collection<String> listValues = new ArrayList<String>();
			if (newDataSource.getValue() instanceof List) {
				for (Object listItem : (List) newDataSource.getValue()) {
					if (listItem instanceof Article) {
						listValues.add(((Article) listItem).getId());
					}
				}
			}

			articlesList.setValue(listValues);
		}

	}
}
