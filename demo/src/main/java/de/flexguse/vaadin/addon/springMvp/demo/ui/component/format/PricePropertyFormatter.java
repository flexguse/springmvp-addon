/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.format;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.util.PropertyFormatter;

import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * This {@link PropertyFormatter} uses the locale in the Translator to format
 * the given price property.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@SuppressWarnings("unchecked")
public class PricePropertyFormatter extends PropertyFormatter implements
		InitializingBean {

	private static final long serialVersionUID = -2126653424395005387L;

	@Autowired
	private Translator translator;

	private NumberFormat numberFormat;

	/**
	 * Initialize the NumberFormatter.
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		if(translator == null){
			numberFormat = DecimalFormat.getInstance(Locale.ENGLISH);
		} else{
			numberFormat = DecimalFormat.getInstance(translator.getUsedLocale());
		}
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.data.util.PropertyFormatter#format(java.lang.Object)
	 */
	@Override
	public String format(Object value) {

		return numberFormat.format(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.data.util.PropertyFormatter#parse(java.lang.String)
	 */
	@Override
	public Object parse(String formattedValue) throws Exception {

		return numberFormat.parseObject(formattedValue);
	}

}
