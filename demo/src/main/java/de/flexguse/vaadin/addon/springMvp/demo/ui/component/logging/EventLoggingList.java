/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.logging;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.util.BeanContainer;
import com.vaadin.ui.Table;

import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * <p>
 * A {@link Table} component which shows {@link SpringMvpEvent} information. The
 * latest shown event is always added on top.
 * </p>
 * 
 * <img src="doc-files/event-logging-list.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class EventLoggingList extends Table implements InitializingBean {

	private static final long serialVersionUID = -477477446844870682L;

	@Autowired
	private Translator translator;

	private Object[] visibleFields = new String[] {
			EventWrapper.ATTRIBUTE_SCOPE, EventWrapper.ATTRIBUTE_DATE,
			EventWrapper.ATTRIBUTE_CLASSNAME,
			EventWrapper.ATTRIBUTE_GENERICTYPE,
			EventWrapper.ATTRIBUTE_SOURCENAME,
			EventWrapper.ATTRIBUTE_EXECUTIONTYPE };

	private BeanContainer<String, EventWrapper> beanContainer = new BeanContainer<String, EventWrapper>(
			EventWrapper.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {

		setCaption("Events");

		beanContainer.setBeanIdProperty(EventWrapper.ATTRIBUTE_ID);
		setContainerDataSource(beanContainer);
		setVisibleColumns(visibleFields);

		setColumnHeader(EventWrapper.ATTRIBUTE_SCOPE,
				translator.getTranslation("monitor.column.eventscope"));
		setColumnHeader(EventWrapper.ATTRIBUTE_DATE,
				translator.getTranslation("monitor.column.eventdate"));
		setColumnHeader(EventWrapper.ATTRIBUTE_CLASSNAME,
				translator.getTranslation("monitor.column.eventclassname"));
		setColumnHeader(EventWrapper.ATTRIBUTE_GENERICTYPE,
				translator.getTranslation("monitor.column.eventgeneric"));
		setColumnHeader(EventWrapper.ATTRIBUTE_SOURCENAME,
				translator.getTranslation("monitor.column.eventsource"));
		setColumnHeader(EventWrapper.ATTRIBUTE_EXECUTIONTYPE,
				translator.getTranslation("monitor.column.eventexecutiontype"));

	}

	/**
	 * Adds the given event to the first position in the table.
	 * 
	 * @param event
	 */
	public void addEvent(SpringMvpEvent event) {

		beanContainer.addBeanAt(0, new EventWrapper(event));

	}
}
