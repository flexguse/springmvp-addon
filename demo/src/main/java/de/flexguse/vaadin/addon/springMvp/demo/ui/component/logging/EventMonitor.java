/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.logging;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;
import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvents;
import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;
import de.flexguse.vaadin.addon.springMvp.demo.backend.model.ShoppingCart;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.DeleteModelEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.OpenMessageEditorEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.OpenModelEditorEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.ShowArticlesViewEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.ShowShoppingCartViewEvent;
import de.flexguse.vaadin.addon.springMvp.dispatcher.DispatcherManager;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowErrorMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowHumanizedMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowTrayNotificationEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowWarningMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasAddedEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasDeletedEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasUpdatedEvent;
import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * This component monitors the dispatched events in the current application and
 * dispatched by all applications.
 * 
 * <img src="doc-files/event-monitor.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class EventMonitor extends CustomComponent implements InitializingBean {

	private static final long serialVersionUID = 7133397069667804634L;

	@Autowired
	private EventLoggingList eventList;

	@Autowired
	private Translator translator;

	@Autowired
	private DispatcherManager dispatcherManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {

		HorizontalLayout mainLayout = new HorizontalLayout();
		mainLayout.setSizeFull();
		mainLayout.setSpacing(true);

		eventList.setSizeFull();

		mainLayout.addComponent(eventList);

		setCompositionRoot(mainLayout);

	}

	@HandleSpringMvpEvents(eventScopes = { EventScope.AllSpringMvpApplications,
			EventScope.SpringMvpApplication }, value = {
			ShowErrorMessageEvent.class, ShowHumanizedMessageEvent.class,
			ShowTrayNotificationEvent.class, ShowWarningMessageEvent.class,
			OpenMessageEditorEvent.class, ShowArticlesViewEvent.class,
			ShowShoppingCartViewEvent.class })
	public void handleSpringMvpEvent(SpringMvpEvent event) {

		eventList.addEvent(event);
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications,
			EventScope.SpringMvpApplication })
	public void handleArticleWasAdded(ModelWasAddedEvent<Article> event) {
		eventList.addEvent(event);
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications,
			EventScope.SpringMvpApplication })
	public void handleShoppingcartWasAdded(
			ModelWasAddedEvent<ShoppingCart> event) {
		eventList.addEvent(event);
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications,
			EventScope.SpringMvpApplication })
	public void handleArticleWasDeleted(ModelWasDeletedEvent<Article> event) {
		eventList.addEvent(event);
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications,
			EventScope.SpringMvpApplication })
	public void handleShoppingcartWasDeleted(
			ModelWasDeletedEvent<ShoppingCart> event) {
		eventList.addEvent(event);
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications,
			EventScope.SpringMvpApplication })
	public void handleArticleWasUpdated(ModelWasUpdatedEvent<Article> event) {
		eventList.addEvent(event);
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications,
			EventScope.SpringMvpApplication })
	public void handleShoppingcartWasUpdated(
			ModelWasUpdatedEvent<ShoppingCart> event) {
		eventList.addEvent(event);
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications,
			EventScope.SpringMvpApplication })
	public void handleArticleDelete(DeleteModelEvent<Article> event) {
		eventList.addEvent(event);
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications,
			EventScope.SpringMvpApplication })
	public void handleShoppingcartDelete(DeleteModelEvent<ShoppingCart> event) {
		eventList.addEvent(event);
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications,
			EventScope.SpringMvpApplication })
	public void handleOpenArticleEditor(OpenModelEditorEvent<Article> event) {
		eventList.addEvent(event);
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications,
			EventScope.SpringMvpApplication })
	public void handleOpenShoppingcartEditor(
			OpenModelEditorEvent<ShoppingCart> event) {
		eventList.addEvent(event);
	}

}
