/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.logging;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * Wrapper for the {@link SpringMvpEvent} to ease the display in a table. Only
 * needed fot {@link EventLoggingList}.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class EventWrapper implements Serializable {

	private static final long serialVersionUID = -5088825666197352545L;

	public static final String ATTRIBUTE_ID = "wrapperId";
	public static final String ATTRIBUTE_DATE = "eventDate";
	public static final String ATTRIBUTE_CLASSNAME = "eventClassName";
	public static final String ATTRIBUTE_SOURCENAME = "eventSourceClassName";
	public static final String ATTRIBUTE_EXECUTIONTYPE = "eventExecutionType";
	public static final String ATTRIBUTE_GENERICTYPE = "eventGenericType";
	public static final String ATTRIBUTE_SCOPE = "eventScope";

	private String wrapperId = UUID.randomUUID().toString();
	private Date eventDate;
	private String eventClassName;
	private String eventSourceClassName;
	private String eventExecutionType;
	private String eventGenericType;
	private String eventScope;

	public EventWrapper(SpringMvpEvent event) {

		this.eventDate = new Date();
		if (event != null) {
			if (event.getClass() != null) {
				this.eventClassName = event.getClass().getName();
			}
			if (event.getGenericType() != null) {
				this.eventGenericType = event.getGenericType().getName();
			}
			if (event.getSource() != null) {
				this.eventSourceClassName = event.getSource().getClass()
						.getSimpleName();
			}
			if (event.getExecutionType() != null) {
				this.eventExecutionType = event.getExecutionType().toString();
			}
			this.eventScope = event.getEventScope();

		}
	}

	/**
	 * @return the wrapperId
	 */
	public String getWrapperId() {
		return wrapperId;
	}

	/**
	 * @return the eventDate
	 */
	public Date getEventDate() {
		return eventDate;
	}

	/**
	 * @return the eventClassName
	 */
	public String getEventClassName() {
		return eventClassName;
	}

	/**
	 * @return the eventSourceClassName
	 */
	public String getEventSourceClassName() {
		return eventSourceClassName;
	}

	/**
	 * @return the eventExecutionType
	 */
	public String getEventExecutionType() {
		return eventExecutionType;
	}

	/**
	 * @return the eventGenericType
	 */
	public String getEventGenericType() {
		return eventGenericType;
	}

	/**
	 * @return the eventScope
	 */
	public String getEventScope() {
		return eventScope;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((eventClassName == null) ? 0 : eventClassName.hashCode());
		result = prime * result
				+ ((eventDate == null) ? 0 : eventDate.hashCode());
		result = prime
				* result
				+ ((eventExecutionType == null) ? 0 : eventExecutionType
						.hashCode());
		result = prime
				* result
				+ ((eventGenericType == null) ? 0 : eventGenericType.hashCode());
		result = prime * result
				+ ((eventScope == null) ? 0 : eventScope.hashCode());
		result = prime
				* result
				+ ((eventSourceClassName == null) ? 0 : eventSourceClassName
						.hashCode());
		result = prime * result
				+ ((wrapperId == null) ? 0 : wrapperId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof EventWrapper)) {
			return false;
		}
		EventWrapper other = (EventWrapper) obj;
		if (eventClassName == null) {
			if (other.eventClassName != null) {
				return false;
			}
		} else if (!eventClassName.equals(other.eventClassName)) {
			return false;
		}
		if (eventDate == null) {
			if (other.eventDate != null) {
				return false;
			}
		} else if (!eventDate.equals(other.eventDate)) {
			return false;
		}
		if (eventExecutionType == null) {
			if (other.eventExecutionType != null) {
				return false;
			}
		} else if (!eventExecutionType.equals(other.eventExecutionType)) {
			return false;
		}
		if (eventGenericType == null) {
			if (other.eventGenericType != null) {
				return false;
			}
		} else if (!eventGenericType.equals(other.eventGenericType)) {
			return false;
		}
		if (eventScope == null) {
			if (other.eventScope != null) {
				return false;
			}
		} else if (!eventScope.equals(other.eventScope)) {
			return false;
		}
		if (eventSourceClassName == null) {
			if (other.eventSourceClassName != null) {
				return false;
			}
		} else if (!eventSourceClassName.equals(other.eventSourceClassName)) {
			return false;
		}
		if (wrapperId == null) {
			if (other.wrapperId != null) {
				return false;
			}
		} else if (!wrapperId.equals(other.wrapperId)) {
			return false;
		}
		return true;
	}

}
