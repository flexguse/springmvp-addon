/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.message;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.ui.ComboBox;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;
import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * This select provides all eventscopes. Scopes are hardcoded.<br/>
 * 
 * <img src="doc-files/event-scope-selection.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class EventScopeSelection extends ComboBox implements InitializingBean {

	private static final long serialVersionUID = -7835404097875392255L;

	@Autowired
	private Translator translator;

	@Override
	public void afterPropertiesSet() throws Exception {

		setNullSelectionAllowed(false);
		setImmediate(true);

		// add the select options
		addItem(EventScope.SpringMvpApplication);
		setItemCaption(EventScope.SpringMvpApplication,
				translator.getTranslation("message.scope.application"));

		addItem(EventScope.AllSpringMvpApplications);
		setItemCaption(EventScope.AllSpringMvpApplications,
				translator.getTranslation("message.scope.allapplication"));

	}

}
