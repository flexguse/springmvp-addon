/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.message;

import org.springframework.beans.factory.InitializingBean;

import com.vaadin.ui.ComboBox;

import de.flexguse.vaadin.addon.springMvp.events.messages.ShowErrorMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowHumanizedMessageEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowTrayNotificationEvent;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowWarningMessageEvent;

/**
 * This selection provides all Event types. EventTypes are hardcoded.<br/>
 * 
 * <img src="doc-files/event-type-selection.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class EventTypeSelection extends ComboBox implements InitializingBean {

	private static final long serialVersionUID = 3785723114674825678L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {

		setNullSelectionAllowed(false);
		setImmediate(true);

		addItem(ShowHumanizedMessageEvent.class);
		setItemCaption(ShowHumanizedMessageEvent.class,
				ShowHumanizedMessageEvent.class.getSimpleName());

		addItem(ShowWarningMessageEvent.class);
		setItemCaption(ShowWarningMessageEvent.class,
				ShowWarningMessageEvent.class.getSimpleName());

		addItem(ShowErrorMessageEvent.class);
		setItemCaption(ShowErrorMessageEvent.class,
				ShowErrorMessageEvent.class.getSimpleName());

		addItem(ShowTrayNotificationEvent.class);
		setItemCaption(ShowTrayNotificationEvent.class,
				ShowTrayNotificationEvent.class.getSimpleName());

	}

}
