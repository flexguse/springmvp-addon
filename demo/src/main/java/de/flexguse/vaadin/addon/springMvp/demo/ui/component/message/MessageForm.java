/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.message;

import java.util.Arrays;
import java.util.Collection;

import com.vaadin.addon.beanvalidation.BeanValidationForm;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Form;

import de.flexguse.vaadin.addon.springMvp.demo.ui.component.message.model.MessageSelectionModel;

/**
 * <p>
 * The {@link Form} which is shown to dispatch a Message. Validation is done via
 * BeanValidation.
 * </p>
 * 
 * <img src="doc-files/message-form.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class MessageForm extends BeanValidationForm<MessageSelectionModel> {

	private static final long serialVersionUID = 4287037970828388496L;

	private MessageSelectionModel messageSelectionModel;

	private Collection<String> propertyIds = Arrays.asList(
			MessageSelectionModel.ATTRIBUTE_TYPE,
			MessageSelectionModel.ATTRIBUTE_TITLE,
			MessageSelectionModel.ATTRIBUTE_TEXT,
			MessageSelectionModel.ATTRIBUTE_SCOPE);

	public MessageForm() {
		super(MessageSelectionModel.class);
	}

	public void setMessageSelectionModel(MessageSelectionModel model) {
		this.messageSelectionModel = model;
		setItemDataSource(new BeanItem<MessageSelectionModel>(model),
				propertyIds);
	}

	public MessageSelectionModel getMessageSelectionModel() {
		return messageSelectionModel;
	}

}
