/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

import de.flexguse.vaadin.addon.springMvp.demo.ui.component.message.model.MessageSelectionModel;
import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * The {@link FormFieldFactory} for a form which displays input for
 * {@link MessageSelectionModel}.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class MessageFormFieldFactory extends DefaultFieldFactory {

	private static final String NULL_REPRESENTATION = "";

	private static final long serialVersionUID = 5026159941919833338L;

	@Autowired
	private Translator translator;
	
	@Autowired
	private ApplicationContext springContext;

	@Override
	public Field createField(Item item, Object propertyId, Component uiContext) {
		
		Field field = null;

		if (MessageSelectionModel.ATTRIBUTE_TEXT.equals(propertyId)) {

			field = new TextArea();
			((TextArea) field).setRows(3);
			((TextArea) field).setNullRepresentation(NULL_REPRESENTATION);
			field.setCaption(translator.getTranslation("message.text"));

		} else if (MessageSelectionModel.ATTRIBUTE_TITLE.equals(propertyId)) {

			field = new TextField();
			((TextField) field).setNullRepresentation(NULL_REPRESENTATION);
			field.setCaption(translator.getTranslation("message.title"));

		} else if(MessageSelectionModel.ATTRIBUTE_SCOPE.equals(propertyId)){
			
			field = springContext.getBean(EventScopeSelection.class);
			field.setCaption(translator.getTranslation("message.scope"));
			
		} else if(MessageSelectionModel.ATTRIBUTE_TYPE.equals(propertyId)){
			field = springContext.getBean(EventTypeSelection.class);
			field.setCaption(translator.getTranslation("message.type"));
		}

		if (field != null) {

			field.setWidth("100%");

			return field;
		} else {
			return super.createField(item, propertyId, uiContext);
		}

		
		
	}
	
}
