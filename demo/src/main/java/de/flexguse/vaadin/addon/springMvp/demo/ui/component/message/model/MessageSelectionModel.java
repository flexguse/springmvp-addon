/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.message.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * This Model contains all information which is needed to generate a Form in
 * which a configurable Message can be created.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class MessageSelectionModel implements Serializable {

	private static final long serialVersionUID = -4548087444234713037L;

	public static final String ATTRIBUTE_TITLE = "messageTitle";
	public static final String ATTRIBUTE_TEXT = "messageText";
	public static final String ATTRIBUTE_TYPE = "eventType";
	public static final String ATTRIBUTE_SCOPE = "eventScope";
	
	@NotNull(message="{message.required.title}")
	private String messageTitle;
	
	@NotNull(message="{message.required.text}")
	private String messageText;
	
	@NotNull(message="{message.required.eventtype}")
	private Class<? extends SpringMvpEvent> eventType;
	
	@NotNull(message="{message.required.eventscope}")
	private String eventScope;

	/**
	 * @return the messageTitle
	 */
	public String getMessageTitle() {
		return messageTitle;
	}

	/**
	 * @param messageTitle
	 *            the messageTitle to set
	 */
	public void setMessageTitle(String messageTitle) {
		this.messageTitle = messageTitle;
	}

	/**
	 * @return the messageText
	 */
	public String getMessageText() {
		return messageText;
	}

	/**
	 * @param messageText
	 *            the messageText to set
	 */
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	/**
	 * @return the eventType
	 */
	public Class<? extends SpringMvpEvent> getEventType() {
		return eventType;
	}

	/**
	 * @param eventType
	 *            the eventType to set
	 */
	public void setEventType(Class<? extends SpringMvpEvent> eventType) {
		this.eventType = eventType;
	}

	/**
	 * @return the eventScope
	 */
	public String getEventScope() {
		return eventScope;
	}

	/**
	 * @param eventScope
	 *            the eventScope to set
	 */
	public void setEventScope(String eventScope) {
		this.eventScope = eventScope;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((eventScope == null) ? 0 : eventScope.hashCode());
		result = prime * result
				+ ((eventType == null) ? 0 : eventType.hashCode());
		result = prime * result
				+ ((messageText == null) ? 0 : messageText.hashCode());
		result = prime * result
				+ ((messageTitle == null) ? 0 : messageTitle.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof MessageSelectionModel)) {
			return false;
		}
		MessageSelectionModel other = (MessageSelectionModel) obj;
		if (eventScope == null) {
			if (other.eventScope != null) {
				return false;
			}
		} else if (!eventScope.equals(other.eventScope)) {
			return false;
		}
		if (eventType == null) {
			if (other.eventType != null) {
				return false;
			}
		} else if (!eventType.equals(other.eventType)) {
			return false;
		}
		if (messageText == null) {
			if (other.messageText != null) {
				return false;
			}
		} else if (!messageText.equals(other.messageText)) {
			return false;
		}
		if (messageTitle == null) {
			if (other.messageTitle != null) {
				return false;
			}
		} else if (!messageTitle.equals(other.messageTitle)) {
			return false;
		}
		return true;
	}

}
