/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.model;

import com.vaadin.ui.Window;

import de.steinwedel.vaadin.MessageBox;
import de.steinwedel.vaadin.MessageBox.VisibilityInterceptor;

/**
 * This custom {@link VisibilityInterceptor} is needed because the
 * {@link MessageBox}es are used as PopUp Windows. A {@link MessageBox} closes
 * in any case. If a Form is contained in the {@link MessageBox} the validation
 * result needs to be shown. Using this interceptor helps to keep the
 * {@link MessageBox} open.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ModelFormInterceptor implements VisibilityInterceptor {

	private static final long serialVersionUID = 4385098802396887283L;

	private boolean closeAble = false;

	public boolean isCloseAble() {
		return closeAble;
	}

	public void setCloseAble(boolean closeAble) {
		this.closeAble = closeAble;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.steinwedel.vaadin.MessageBox.VisibilityInterceptor#show(com.vaadin
	 * .ui.Window, de.steinwedel.vaadin.MessageBox)
	 */
	@Override
	public boolean show(Window parentWindow, MessageBox displayedDialog) {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.steinwedel.vaadin.MessageBox.VisibilityInterceptor#close(com.vaadin
	 * .ui.Window, de.steinwedel.vaadin.MessageBox)
	 */
	@Override
	public boolean close(Window parentWindow, MessageBox displayedDialog) {
		return closeAble;
	}

}
