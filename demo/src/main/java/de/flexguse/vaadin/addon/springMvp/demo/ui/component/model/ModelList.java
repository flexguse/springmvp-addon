/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.util.BeanContainer;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Table;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Model;
import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * This abstract Table contains several helper method which eases the usage of
 * the {@link Table} in combination with custom model lists.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public abstract class ModelList<T extends Model> extends Table {

	private static final long serialVersionUID = -3997490516441236034L;

	@Autowired
	protected Translator translator;

	protected BeanContainer<String, T> beanContainer;

	protected String[] visibleColumns;

	public ModelList() {
	}

	public ModelList(String[] visibleColumns) {
		this.visibleColumns = visibleColumns;
	}

	/**
	 * Sets the models (items) in the list.
	 * 
	 * @param models
	 */
	public void setModels(List<T> models) {

		beanContainer.removeAllItems();
		beanContainer.addAll(models);

	}

	/**
	 * Adds a single model (item) to the list.
	 * 
	 * @param model
	 */
	public void addModel(T model) {
		beanContainer.addBean(model);
	}

	/**
	 * Removes a single model (item) in the list.
	 * 
	 * @param model
	 */
	public void removeModel(T model) {
		beanContainer.removeItem(model.getId());
	}

	/**
	 * Updates a single model (item) in the list.
	 * 
	 * @param model
	 */
	public void updateModel(T model) {
		int articleIndex = beanContainer.indexOfId(model.getId());
		if (articleIndex > -1) {
			beanContainer.removeItem(model.getId());
			beanContainer.addBeanAt(articleIndex, model);
		}
	}

	/**
	 * This method gets the selected item.
	 * 
	 * @return selected model or null
	 */
	public T getSelectedModel() {

		if (beanContainer != null && getValue() != null) {

			BeanItem<T> beanItem = beanContainer.getItem(getValue());
			if (beanItem != null) {
				return beanContainer.getItem(getValue()).getBean();
			}

		}

		return null;
	}

	/**
	 * Gets all selected models from the list.
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List<T> getSelectedModels() {

		List<T> result = new ArrayList<T>();

		if (beanContainer != null && getValue() != null) {

			Object modelIds = getValue();
			if (modelIds != null && modelIds instanceof Set) {
				for (Object modelId : (Set) modelIds) {

					result.add(beanContainer.getItem(modelId).getBean());

				}
			}

		}

		return result;

	}

	/**
	 * This method gets the model having the given ID.
	 * 
	 * @param modelId
	 * @return null or found model
	 */
	public T getModel(String artileId) {
		if (StringUtils.isNotBlank(artileId)) {
			return beanContainer.getItem(artileId).getBean();
		}
		return null;
	}

}
