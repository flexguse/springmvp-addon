/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.model;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Model;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.RefreshModelEvent;
import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * Abstract {@link ModelManagement}. Extend this class with your {@link Model}
 * instances and you have quickly a CRUD List.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public abstract class ModelManagement<T extends Model> extends CustomComponent
		implements InitializingBean {

	private static final long serialVersionUID = 8033233067431225155L;

	@Autowired
	private Translator translator;

	private Button editButton;
	private Button deleteButton;

	private String translationPrefix;

	protected ModelList<T> modelList;

	protected ModelManagementPresenter<T> presenter;

	/**
	 * @param translationPrefix
	 *            the translationPrefix to set
	 */
	public void setTranslationPrefix(String translationPrefix) {
		this.translationPrefix = translationPrefix;
	}

	/**
	 * @return the translationPrefix
	 */
	public String getTranslationPrefix() {
		return translationPrefix;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		VerticalLayout managementLayout = new VerticalLayout();
		managementLayout.setSpacing(true);
		managementLayout.setSizeFull();

		/**
		 * add the buttons
		 */
		HorizontalLayout buttonBar = new HorizontalLayout();
		buttonBar.setWidth("100%");
		HorizontalLayout buttons = createButtonBar();
		buttonBar.addComponent(buttons);
		buttonBar.setComponentAlignment(buttons, Alignment.MIDDLE_RIGHT);
		managementLayout.addComponent(buttonBar);

		/**
		 * add the articles list
		 */
		modelList.setSizeFull();
		managementLayout.addComponent(modelList);
		managementLayout.setExpandRatio(modelList, 1f);

		setCompositionRoot(managementLayout);

	}

	/**
	 * This helper method creates the ButtonBar for the articles.
	 * 
	 * @return
	 */
	private HorizontalLayout createButtonBar() {

		HorizontalLayout buttonBar = new HorizontalLayout();
		buttonBar.setSpacing(true);

		/*
		 * add the refresh-button including listener
		 */
		Button refreshButton = new Button(
				translator
						.getTranslation(translationPrefix + ".button.refresh"));
		refreshButton.addListener(new Button.ClickListener() {

			private static final long serialVersionUID = 8577365621209877374L;

			@Override
			public void buttonClick(ClickEvent event) {
				presenter.handleRefreshModels(new RefreshModelEvent<T>(this,
						presenter.getModelClass()));

			}
		});
		buttonBar.addComponent(refreshButton);

		/*
		 * the the add-button including listener
		 */
		Button addButton = new Button(
				translator.getTranslation(translationPrefix + ".button.addNew"));
		addButton.addListener(new Button.ClickListener() {

			private static final long serialVersionUID = -5780192901426628833L;

			@Override
			public void buttonClick(ClickEvent event) {
				presenter.openModelEditor(null, true);
			}
		});
		buttonBar.addComponent(addButton);

		/*
		 * the edit-button including listener
		 */
		editButton = new Button(translator.getTranslation(translationPrefix
				+ ".button.edit"));
		editButton.addListener(new Button.ClickListener() {

			private static final long serialVersionUID = -2684118723990479991L;

			@Override
			public void buttonClick(ClickEvent event) {
				presenter.openModelEditor(modelList.getSelectedModel(), false);

			}
		});
		editButton.setEnabled(false);
		buttonBar.addComponent(editButton);

		/*
		 * the delete-button including listener
		 */
		deleteButton = new Button(translator.getTranslation(translationPrefix
				+ ".button.delete"));
		deleteButton.setEnabled(false);
		deleteButton.addListener(new Button.ClickListener() {

			private static final long serialVersionUID = -9193939012810639598L;

			@Override
			public void buttonClick(ClickEvent event) {
				presenter.deleteModel(modelList.getSelectedModel());
			}
		});
		buttonBar.addComponent(deleteButton);

		return buttonBar;
	}

	/**
	 * This helper method enables all buttons which are only enabled if an
	 * Article is chosen.
	 */
	public void enableArticleSpecificButtons() {
		editButton.setEnabled(true);
		deleteButton.setEnabled(true);
	}

	/**
	 * This helper method disables all buttons which are only enabled if an
	 * Article is chosen.
	 */
	public void disableArticleSpecificButtons() {
		editButton.setEnabled(false);
		deleteButton.setEnabled(false);
	}

	/**
	 * Gets the ModelList view component which shows all the models.
	 * 
	 * @return
	 */
	public ModelList<T> getModelList() {
		return modelList;
	}

}
