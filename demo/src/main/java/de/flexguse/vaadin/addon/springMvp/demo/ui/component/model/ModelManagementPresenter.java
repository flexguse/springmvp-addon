/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.model;

import org.apache.commons.lang3.ArrayUtils;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;

import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;
import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Model;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.DeleteModelEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.ModelWasChosenEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.OpenModelEditorEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.RefreshModelEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.service.UiModelService;
import de.flexguse.vaadin.addon.springMvp.presenter.AbstractPresenter;

/**
 * Presenter for {@link ModelManagement} containing logic.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public abstract class ModelManagementPresenter<T extends Model> extends
		AbstractPresenter<ModelManagement<?>> {

	private T chosenModel = null;

	private UiModelService<T> modelService;

	private Class<T> modelClass;

	/**
	 * The constructor for the {@link ModelManagementPresenter}.
	 * 
	 * @param modelClass
	 *            the class for the model. Needed to be able to instantiate a
	 *            new instance in openModelEditor.
	 */
	public ModelManagementPresenter(Class<T> modelClass) {
		this.modelClass = modelClass;
	}

	/**
	 * @return the modelClass
	 */
	public Class<T> getModelClass() {
		return modelClass;
	}

	/**
	 * @param modelService
	 *            the modelService to set
	 */
	public void setModelService(UiModelService<T> modelService) {
		this.modelService = modelService;
	}

	/**
	 * Gets the ModelList from the ModelManagement.
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ModelList<T> getModelList() {
		return (ModelList<T>) getView().getModelList();
	}

	/**
	 * This method handles the {@link ModelWasChosenEvent} which can be
	 * dispatched all over the application.
	 * <p>
	 * In some cases it seems to be more straight forward if this method is
	 * called directly instead of using the event dispatcher.
	 * </p>
	 * 
	 * @param event
	 */
	@HandleSpringMvpEvent
	public void handleModelWasChosen(ModelWasChosenEvent<T> event) {

		chosenModel = event.getChosenModel();

		if (chosenModel == null) {
			getView().disableArticleSpecificButtons();
		} else {
			getView().enableArticleSpecificButtons();
		}
	}

	/**
	 * This method handles the {@link } which can be dispatched all over the
	 * application.
	 * <p>
	 * In some cases it seems to be more straight forward if this method is
	 * called directly instead of using the event dispatcher.
	 * </p>
	 * 
	 * @param event
	 */
	@HandleSpringMvpEvent
	public void handleRefreshModels(RefreshModelEvent<T> event) {
		getModelList().setModels(modelService.getAllModels());
	}

	/**
	 * This method dispatches the {@link OpenModelEditorEvent} to open an editor
	 * for the given {@link Article}.
	 * 
	 * @param model
	 * @param isNew
	 */
	public void openModelEditor(T model, boolean isNew) {

		if (model == null) {
			try {

				model = modelClass.newInstance();
			} catch (InstantiationException e) {
				throw new RuntimeException(e);
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			}

		}

		OpenModelEditorEvent<T> event = new OpenModelEditorEvent<T>(this,
				model.getClass());
		event.setModel(model);
		event.setNewModel(isNew);
		dispatchEvent(event);

	}

	/**
	 * This method dispatches the {@link DeleteModelEvent} and starts the
	 * deletion including confirmation of the given {@link Model}.
	 * 
	 * @param model
	 */
	public void deleteModel(T model) {
		DeleteModelEvent<T> deleteEvent = new DeleteModelEvent<T>(this, model);
		dispatchEvent(deleteEvent);
	}

	/**
	 * This helper method gets the name of the ModelClass in lower case.
	 * 
	 * @return
	 */
	protected String getModelClassName() {

		if (modelClass != null) {
			return modelClass.getSimpleName().toLowerCase();
		}

		return "";

	}

	/**
	 * This helper method adds the ActionHandler for the modelList.
	 */
	protected void addModelsListHandler() {

		final Action refreshAction = new Action(translate(getModelClassName()
				+ ".list.refresh"));
		final Action addAction = new Action(translate(getModelClassName()
				+ ".list.new"));
		final Action editAction = new Action(translate(getModelClassName()
				+ ".list.edit"));
		final Action deleteAction = new Action(translate(getModelClassName()
				+ ".list.delete"));

		getView().getModelList().addActionHandler(new Handler() {

			private static final long serialVersionUID = -1065187465892552160L;

			@Override
			public void handleAction(Action action, Object sender, Object target) {

				if (action == refreshAction) {
					handleRefreshModels(new RefreshModelEvent<T>(this,
							modelClass));
				} else if (action == addAction) {
					openModelEditor(null, true);
				} else if (action == editAction) {

					T model = getModelList().getSelectedModel();
					// the model might not be selected, so the targeted
					// mnodel is used
					if (model == null) {
						model = getModelList().getModel((String) target);
					}

					openModelEditor(model, false);
				} else if (action == deleteAction) {
					deleteModel(getModelList().getModel((String) target));
				}

			}

			@Override
			public Action[] getActions(Object target, Object sender) {

				Action[] actions = new Action[] { refreshAction, addAction };
				if (target != null) {
					actions = ArrayUtils.add(actions, editAction);
					actions = ArrayUtils.add(actions, deleteAction);
				}

				return actions;
			}
		});

	}

	/**
	 * This helper method adds a listener to the articles list and dispatches a
	 * {@link ModelWasChosenEvent} every time an article was chosen.
	 */
	protected void addModelSelectionListener() {

		getView().getModelList().addListener(
				new Property.ValueChangeListener() {

					private static final long serialVersionUID = -6022763605228615203L;

					@Override
					public void valueChange(ValueChangeEvent event) {
						if (getView().getModelList().getSelectedModel() != null) {
							handleModelWasChosen(new ModelWasChosenEvent<T>(
									getView().getModelList(), getModelList()
											.getSelectedModel()));
						} else {
							handleModelWasChosen(new ModelWasChosenEvent<T>(
									getView().getModelList(), modelClass));
						}

					}
				});

	}

}
