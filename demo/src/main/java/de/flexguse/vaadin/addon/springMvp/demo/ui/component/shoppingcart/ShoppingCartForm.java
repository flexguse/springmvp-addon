/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.shoppingcart;

import java.util.ArrayList;
import java.util.Collection;

import com.vaadin.addon.beanvalidation.BeanValidationForm;
import com.vaadin.data.util.BeanItem;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.ShoppingCart;

/**
 * The {@link ShoppingCartForm} is the Form for creating/editing
 * {@link ShoppingCart} objects. <br/>
 * <img src="doc-files/shopping-cart-form.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ShoppingCartForm extends BeanValidationForm<ShoppingCart> {

	private static final long serialVersionUID = 7632801347062552562L;

	private ShoppingCart shoppingCart;

	private Collection<String> visibleInputFields;

	public ShoppingCartForm() {
		super(ShoppingCart.class);

		visibleInputFields = new ArrayList<String>();
		visibleInputFields.add(ShoppingCart.ATTRIBUTE_NAME);
		visibleInputFields.add(ShoppingCart.ATTRIBUTE_ARTICLES);

	}

	public void setShoppingCart(ShoppingCart shoppingCart) {

		if (shoppingCart == null) {
			shoppingCart = new ShoppingCart();
		}

		this.shoppingCart = shoppingCart;
		setItemDataSource(new BeanItem<ShoppingCart>(shoppingCart,
				visibleInputFields));

	}

	/**
	 * @return the shoppingCart
	 */
	public ShoppingCart getShoppingCart() {
		return shoppingCart;
	}

}
