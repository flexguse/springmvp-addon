/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.shoppingcart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextField;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.ShoppingCart;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.articles.ArticlesSelectionField;
import de.flexguse.vaadin.addon.springMvp.locale.Translator;

/**
 * The {@link FormFieldFactory} for the {@link ShoppingCartForm}.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ShoppingCartFormFieldFactory extends DefaultFieldFactory implements
		FormFieldFactory {

	private static final long serialVersionUID = -4349884456969276785L;

	@Autowired
	private ApplicationContext springContext;
	
	@Autowired
	private Translator translator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.ui.FormFieldFactory#createField(com.vaadin.data.Item,
	 * java.lang.Object, com.vaadin.ui.Component)
	 */
	@Override
	public Field createField(Item item, Object propertyId, Component uiContext) {

		Field field = null;

		if (ShoppingCart.ATTRIBUTE_ARTICLES.equals(propertyId)) {

			field = springContext.getBean(ArticlesSelectionField.class);
			field.setHeight("240px");

		} else {
			field = super.createField(item, propertyId, uiContext);
		}

		field.setCaption(translator.getTranslation("shoppingcart." + propertyId.toString()));
		field.setWidth("100%");
		
		if(field instanceof TextField){
			((TextField) field).setNullRepresentation("");
		}

		return field;
	}

}
