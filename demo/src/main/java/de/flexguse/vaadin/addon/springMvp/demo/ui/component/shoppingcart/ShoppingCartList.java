/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.shoppingcart;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.Property;
import com.vaadin.data.util.BeanContainer;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;
import de.flexguse.vaadin.addon.springMvp.demo.backend.model.ShoppingCart;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.format.PricePropertyFormatter;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.model.ModelList;

/**
 * The {@link ShoppingCartList} is a table component which is designed to show a
 * list {@link ShoppingCart} objects. <br/>
 * <img src="doc-files/shopping-cart-list.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ShoppingCartList extends ModelList<ShoppingCart> implements
		InitializingBean {

	private static final long serialVersionUID = 711793594011496834L;

	@Autowired
	private PricePropertyFormatter priceFormatter;

	public ShoppingCartList() {

		super(new String[] { ShoppingCart.ATTRIBUTE_NAME,
				ShoppingCart.ATTRIBUTE_PRICE });

	}

	@Override
	public void afterPropertiesSet() throws Exception {

		setImmediate(true);
		setColumnCollapsingAllowed(true);
		setSelectable(true);
		setMultiSelect(false);

		beanContainer = new BeanContainer<String, ShoppingCart>(
				ShoppingCart.class);
		beanContainer.setBeanIdProperty(ShoppingCart.ATTRIBUTE_ID);
		setContainerDataSource(beanContainer);
		setVisibleColumns(visibleColumns);

		setColumnHeader(ShoppingCart.ATTRIBUTE_NAME,
				translator.getTranslation("shoppingcart.list.header.name"));
		setColumnHeader(Article.ATTRIBUTE_PRICE,
				translator.getTranslation("shoppingcart.list.header.price"));

	}

	@Override
	protected String formatPropertyValue(Object rowId, Object colId,
			Property property) {

		if (ShoppingCart.ATTRIBUTE_PRICE.equals(colId)) {
			return priceFormatter.format(property.getValue());
		}

		return super.formatPropertyValue(rowId, colId, property);
	}

}
