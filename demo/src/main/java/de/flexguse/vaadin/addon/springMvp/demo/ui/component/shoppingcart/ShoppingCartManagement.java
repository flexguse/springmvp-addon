/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.shoppingcart;

import org.springframework.beans.factory.annotation.Autowired;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.ShoppingCart;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.model.ModelManagement;
import de.flexguse.vaadin.addon.springMvp.view.View;

/**
 * The {@link ShoppingCartManagement} contains the complete management for
 * {@link ShoppingCart} objects. <br/>
 * <img src="doc-files/shopping-cart-management.png" />
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ShoppingCartManagement extends ModelManagement<ShoppingCart>
		implements View<ShoppingCartManagementPresenter> {

	private static final long serialVersionUID = -7234656980141973339L;

	@Autowired
	public void setShoppingCartList(ShoppingCartList shoppingCartList) {
		modelList = shoppingCartList;
	}

	@Override
	public void setPresenter(ShoppingCartManagementPresenter presenter) {
		this.presenter = presenter;

		// register the view in the presenter
		presenter.setView(this);
	}

}
