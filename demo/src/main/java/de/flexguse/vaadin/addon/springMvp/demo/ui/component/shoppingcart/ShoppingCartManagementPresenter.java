/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.component.shoppingcart;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;
import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.demo.backend.model.ShoppingCart;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.model.ModelManagementPresenter;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.ModelWasChosenEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasAddedEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasDeletedEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasUpdatedEvent;

/**
 * Presenter for {@link ShoppingCartManagement}.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ShoppingCartManagementPresenter extends
		ModelManagementPresenter<ShoppingCart> {

	public ShoppingCartManagementPresenter() {
		super(ShoppingCart.class);
	}

	public void setView(ShoppingCartManagement view) {
		super.setView(view);

		// register the Handler for the articles list
		addModelsListHandler();
		addModelSelectionListener();
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications })
	public void handleShoppingcartAddedEvent(
			ModelWasAddedEvent<ShoppingCart> event) {
		getModelList().addModel(event.getAddedModel());
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications })
	public void handleShoppingcartUpdatedEvent(
			ModelWasUpdatedEvent<ShoppingCart> event) {
		getModelList().updateModel(event.getUpdatedModel());
	}

	@HandleSpringMvpEvent(eventScopes = { EventScope.AllSpringMvpApplications })
	public void handleShoppingcartDeletedEvent(
			ModelWasDeletedEvent<ShoppingCart> event) {
		getModelList().removeModel(event.getDeletedModel());
		handleModelWasChosen(new ModelWasChosenEvent<ShoppingCart>(null,
				ShoppingCart.class));
	}
}
