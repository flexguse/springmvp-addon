/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.events;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Model;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * Dispatch this event if an {@link Model} shall be deleted. This event always
 * produces a delete confirmation window.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class DeleteModelEvent<T> extends SpringMvpEvent {

	private static final long serialVersionUID = -4578483525205050769L;

	private T toDelete;

	public DeleteModelEvent(Object source, Class<T> modelClass) {
		super(source, modelClass);
	}

	public DeleteModelEvent(Object source, T toDelete) {
		super(source, toDelete.getClass());
		this.toDelete = toDelete;
	}

	/**
	 * The {@link Model} instance which the user required to be deleted.
	 * 
	 * @return
	 */
	public T getToDelete() {
		return toDelete;
	}

	/**
	 * Sets the {@link Model} which the user wants to delete.
	 * 
	 * @param toDelete
	 */
	public void setToDelete(T toDelete) {
		this.toDelete = toDelete;
	}

}
