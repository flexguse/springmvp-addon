/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.events;

import de.flexguse.vaadin.addon.springMvp.demo.ui.component.model.ModelList;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * This Event is dispatched in the {@link ModelList} implementations when an
 * entry in the list was chosen or unchosen.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ModelWasChosenEvent<T> extends SpringMvpEvent {

	private static final long serialVersionUID = 422581870431775068L;

	private T chosenModel;

	public ModelWasChosenEvent(Object eventSource, Class<T> modelClass) {
		super(eventSource, modelClass);
	}

	public ModelWasChosenEvent(Object eventSource, T chosenModel) {
		super(eventSource, chosenModel.getClass());
		this.chosenModel = chosenModel;
	}

	public T getChosenModel() {
		return chosenModel;
	}

	public void setChosenArticle(T chosenModel) {
		this.chosenModel = chosenModel;
	}

}
