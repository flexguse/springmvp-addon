/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.events;

import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * Dispatch this event if you want to show the Editor for a new Message.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class OpenMessageEditorEvent extends SpringMvpEvent {

	private static final long serialVersionUID = -5824952150661813092L;

	public OpenMessageEditorEvent(Object eventSource) {
		super(eventSource, null);
	}

}
