/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.events;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Model;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * Dispatch this event if you want to show the editor form for a n
 * {@link Model}.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class OpenModelEditorEvent<T> extends SpringMvpEvent {

	private static final long serialVersionUID = 4182741206723581835L;

	private boolean newModel = false;
	private T model;

	public boolean isNewModel() {
		return newModel;
	}

	/**
	 * Set a flag if the model in this event is treated as new.
	 * Default value is false.
	 * 
	 * @param newModel
	 */
	public void setNewModel(boolean newModel) {
		this.newModel = newModel;
	}

	public T getModel() {
		return model;
	}

	/**
	 * Sets the model which is shown in the editor.
	 * 
	 * @param model
	 */
	public void setModel(T model) {
		this.model = model;
	}

	public OpenModelEditorEvent(Object eventSource, Class<?> genericClass) {
		super(eventSource, genericClass);
	}

}
