/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.events;

import de.flexguse.vaadin.addon.springMvp.annotations.EventScope;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * Dispatch this event if a Model instance needs to be refreshed in the UI. The
 * {@link EventScope} is set to {@link EventScope#SpringMvpApplication} and the
 * {@link ExecutionType} is set to {@link ExecutionType#SYNC}.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class RefreshModelEvent<T> extends SpringMvpEvent {

	private static final long serialVersionUID = 2409555190034178114L;

	public RefreshModelEvent(Object eventSource, Class<T> modelClass) {
		super(eventSource, modelClass);
	}

}
