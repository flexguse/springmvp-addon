/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.events;

import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * Dispatch this event to show the Article View.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ShowArticlesViewEvent extends SpringMvpEvent {

	private static final long serialVersionUID = -7682300622168995827L;

	public ShowArticlesViewEvent(Object eventSource) {
		super(eventSource, null);
	}

}
