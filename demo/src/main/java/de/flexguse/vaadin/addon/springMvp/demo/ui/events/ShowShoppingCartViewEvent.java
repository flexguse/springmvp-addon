package de.flexguse.vaadin.addon.springMvp.demo.ui.events;

import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;

/**
 * Dispatch this event to open the ShoppingCart view.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ShowShoppingCartViewEvent extends SpringMvpEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3475450065703953358L;

	public ShowShoppingCartViewEvent(Object eventSource) {
		super(eventSource, null);
	}

}
