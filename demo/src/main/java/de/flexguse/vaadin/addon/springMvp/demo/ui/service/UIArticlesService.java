/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.Validator.InvalidValueException;

import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;
import de.flexguse.vaadin.addon.springMvp.demo.backend.service.ArticleService;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.articles.ArticleForm;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.model.ModelFormInterceptor;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.DeleteModelEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.OpenModelEditorEvent;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent.ExecutionType;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowTrayNotificationEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasAddedEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasDeletedEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasUpdatedEvent;
import de.flexguse.vaadin.addon.springMvp.service.AbstractUIService;
import de.flexguse.vaadin.addon.springMvp.service.UIService;
import de.steinwedel.vaadin.MessageBox;
import de.steinwedel.vaadin.MessageBox.ButtonType;

/**
 * This ArticleService is designed to be used directly in the UI components.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class UIArticlesService extends AbstractUIService implements UIService,
		UiModelService<Article> {

	@Autowired
	private ArticleService articleService;

	@Autowired
	private ArticleForm articleForm;

	/**
	 * This method gets all Articles from the backend persistence.
	 * 
	 * @return
	 */
	public List<Article> getAllModels() {

		return articleService.getAllActiveArticles();

	}

	/**
	 * This method listens for an {@link OpenModelEditorEvent} and opens an
	 * article editor containing the {@link Article} given by the event.
	 * 
	 * @param event
	 */
	@SuppressWarnings("static-access")
	@HandleSpringMvpEvent
	public void handleOpenArticleEditorEvent(
			final OpenModelEditorEvent<Article> event) {

		articleForm.setArticle(event.getModel());

		String editorTitle = translate("articles.editor.title.new");
		if (!event.isNewModel()) {
			editorTitle = translate("articles.editor.title.edit");
		}

		final MessageBox messageBox = new MessageBox(getMainWindow(), "45%",
				"450px", editorTitle, MessageBox.Icon.NONE, articleForm,
				MessageBox.BUTTON_DEFAULT_ALIGNMENT,
				new MessageBox.ButtonConfig(ButtonType.CANCEL,
						translate("cancel")), new MessageBox.ButtonConfig(
						ButtonType.SAVE, translate("save")));
		final ModelFormInterceptor interceptor = new ModelFormInterceptor();
		messageBox.VISIBILITY_INTERCEPTOR = interceptor;
		messageBox.show(true, new MessageBox.EventListener() {

			private static final long serialVersionUID = 2948376877660010667L;

			@Override
			public void buttonClicked(ButtonType buttonType) {
				if (buttonType.equals(ButtonType.SAVE)) {
					try {
						articleForm.commit();
						articleService.saveArticle(articleForm.getArticle());

						if (event.isNewModel()) {
							ModelWasAddedEvent<Article> event = new ModelWasAddedEvent<Article>(
									this, Article.class);
							event.setAddedModel(articleForm.getArticle());
							dispatchEvent(event);

							// show added tray notification
							ShowTrayNotificationEvent trayEvent = new ShowTrayNotificationEvent(
									this, translate("articles.added.title"),
									translate("articles.added.info",
											new Object[] { articleForm
													.getArticle().getName() }));
							trayEvent.setExecutionType(ExecutionType.ASYNC);
							dispatchEvent(trayEvent);

						} else {
							ModelWasUpdatedEvent<Article> event = new ModelWasUpdatedEvent<Article>(
									this, Article.class);
							event.setUpdatedModel(articleForm.getArticle());
							dispatchEvent(event);

							// show updated tray notification
							ShowTrayNotificationEvent trayEvent = new ShowTrayNotificationEvent(
									this, translate("articles.updated.title"),
									translate("articles.updated.info",
											new Object[] { articleForm
													.getArticle().getName() }));
							trayEvent.setExecutionType(ExecutionType.ASYNC);
							dispatchEvent(trayEvent);
						}

						interceptor.setCloseAble(true);
						messageBox.close();

					} catch (InvalidValueException e) {
						interceptor.setCloseAble(false);
					}

				} else {
					interceptor.setCloseAble(true);
					messageBox.close();

				}

			}
		});

	}

	@HandleSpringMvpEvent
	public void handleDeleteArticleEvent(final DeleteModelEvent<Article> event) {

		MessageBox deleteConfirmation = new MessageBox(getMainWindow(),
				translate("articles.delete.confirmation.title"),
				MessageBox.Icon.QUESTION, translate(
						"articles.delete.confirmation.question",
						new Object[] { event.getToDelete().getName() }),
				new MessageBox.ButtonConfig(ButtonType.NO, translate("no")),
				new MessageBox.ButtonConfig(ButtonType.YES, translate("yes")));
		deleteConfirmation.show(true, new MessageBox.EventListener() {

			private static final long serialVersionUID = -7925625315552580719L;

			@Override
			public void buttonClicked(ButtonType buttonType) {
				if (buttonType.equals(ButtonType.YES)) {
					articleService.deleteArticle(event.getToDelete());

					// dispatch deleted event
					ModelWasDeletedEvent<Article> deletedEvent = new ModelWasDeletedEvent<Article>(
							this, Article.class);
					deletedEvent.setDeletedModel(event.getToDelete());
					dispatchEvent(deletedEvent);

					// show notification
					ShowTrayNotificationEvent notificationEvent = new ShowTrayNotificationEvent(
							this, translate("articles.deleted.title"),
							translate("articles.deleted.info",
									new Object[] { event.getToDelete()
											.getName() }));
					notificationEvent.setExecutionType(ExecutionType.ASYNC);
					dispatchEvent(notificationEvent);
				}

			}
		});

	}

}
