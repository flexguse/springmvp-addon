/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.service;

import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.data.Validator.InvalidValueException;

import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.message.MessageForm;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.message.model.MessageSelectionModel;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.model.ModelFormInterceptor;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.OpenMessageEditorEvent;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent.ExecutionType;
import de.flexguse.vaadin.addon.springMvp.service.AbstractUIService;
import de.steinwedel.vaadin.MessageBox;
import de.steinwedel.vaadin.MessageBox.ButtonType;

/**
 * The UI Service which is responsible to open the {@link MessageForm} in a
 * modal PopUp and which dispatches the event.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class UIMessageService extends AbstractUIService {

	private MessageForm messageForm;

	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private ApplicationContext springContext;

	@SuppressWarnings("static-access")
	@HandleSpringMvpEvent
	public void handleShowMessageEditor(OpenMessageEditorEvent event) {

		messageForm = springContext.getBean(MessageForm.class);
		messageForm.setMessageSelectionModel(new MessageSelectionModel());

		final MessageBox messageBox = new MessageBox(getMainWindow(), "45%",
				"450px", translate("message.editor.title"),
				MessageBox.Icon.NONE, messageForm,
				MessageBox.BUTTON_DEFAULT_ALIGNMENT,
				new MessageBox.ButtonConfig(ButtonType.CANCEL,
						translate("cancel")), new MessageBox.ButtonConfig(
						ButtonType.SAVE, translate("button.send.message"), "160"));
		

		// ModelFormInterceptor works here too
		final ModelFormInterceptor interceptor = new ModelFormInterceptor();
		messageBox.VISIBILITY_INTERCEPTOR = interceptor;

		messageBox.show(true, new MessageBox.EventListener() {

			private static final long serialVersionUID = -1466336209875480948L;

			@Override
			public void buttonClicked(ButtonType buttonType) {

				if (ButtonType.SAVE.equals(buttonType)) {

					try {

						messageForm.commit();
						interceptor.setCloseAble(true);
						messageBox.close();
						dispatchMessageSelectionEvent(messageForm
								.getMessageSelectionModel());
					} catch (InvalidValueException e) {
						// intended to do nothing
					}

				} else {
					interceptor.setCloseAble(true);
					messageBox.close();
				}

			}
		});

	}

	/**
	 * This helper method creates an {@link SpringMvpEvent} from the given
	 * messageModel and dispatches it.
	 * 
	 * @param messageModel
	 */
	private void dispatchMessageSelectionEvent(
			MessageSelectionModel messageModel) {

		try {
			SpringMvpEvent springMvpEvent = messageModel
					.getEventType()
					.getConstructor(Object.class, String.class, String.class)
					.newInstance(this, messageModel.getMessageTitle(),
							messageModel.getMessageText());

			springMvpEvent.setEventScope(messageModel.getEventScope());
			springMvpEvent.setExecutionType(ExecutionType.ASYNC);
			dispatchEvent(springMvpEvent);

		} catch (InstantiationException e) {
			logger.error(e);
		} catch (IllegalAccessException e) {
			logger.error(e);
		} catch (IllegalArgumentException e) {
			logger.error(e);
		} catch (InvocationTargetException e) {
			logger.error(e);
		} catch (NoSuchMethodException e) {
			logger.error(e);
		} catch (SecurityException e) {
			logger.error(e);
		}

	}

}
