/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.Validator.InvalidValueException;

import de.flexguse.vaadin.addon.springMvp.annotations.HandleSpringMvpEvent;
import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;
import de.flexguse.vaadin.addon.springMvp.demo.backend.model.ShoppingCart;
import de.flexguse.vaadin.addon.springMvp.demo.backend.service.ShoppingCartService;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.model.ModelFormInterceptor;
import de.flexguse.vaadin.addon.springMvp.demo.ui.component.shoppingcart.ShoppingCartForm;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.DeleteModelEvent;
import de.flexguse.vaadin.addon.springMvp.demo.ui.events.OpenModelEditorEvent;
import de.flexguse.vaadin.addon.springMvp.events.SpringMvpEvent.ExecutionType;
import de.flexguse.vaadin.addon.springMvp.events.messages.ShowTrayNotificationEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasAddedEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasDeletedEvent;
import de.flexguse.vaadin.addon.springMvp.events.model.ModelWasUpdatedEvent;
import de.flexguse.vaadin.addon.springMvp.service.AbstractUIService;
import de.steinwedel.vaadin.MessageBox;
import de.steinwedel.vaadin.MessageBox.ButtonType;

/**
 * This ShoppingCartService is designed to be used directly in the UI
 * components.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class UIShoppingCartService extends AbstractUIService implements
		UiModelService<ShoppingCart> {

	@Autowired
	private ShoppingCartService shoppingCartService;

	@Autowired
	private ShoppingCartForm shoppingCartForm;

	@Override
	public List<ShoppingCart> getAllModels() {
		return shoppingCartService.getAllShoppingCarts();
	}

	/**
	 * This method listens for an {@link OpenModelEditorEvent} and opens an
	 * article editor containing the {@link Article} given by the event.
	 * 
	 * @param event
	 */
	@SuppressWarnings("static-access")
	@HandleSpringMvpEvent
	public void handleOpenShoppingCartEditorEvent(
			final OpenModelEditorEvent<ShoppingCart> event) {

		shoppingCartForm.setShoppingCart(event.getModel());
		String editorTitle = translate("shoppingcart.editor.title.new");
		if (!event.isNewModel()) {
			editorTitle = translate("shoppingcart.editor.title.edit");
		}

		final MessageBox messageBox = new MessageBox(getMainWindow(), "45%",
				"450px", editorTitle, MessageBox.Icon.NONE, shoppingCartForm,
				MessageBox.BUTTON_DEFAULT_ALIGNMENT,
				new MessageBox.ButtonConfig(ButtonType.CANCEL,
						translate("cancel")), new MessageBox.ButtonConfig(
						ButtonType.SAVE, translate("save")));
		final ModelFormInterceptor interceptor = new ModelFormInterceptor();
		messageBox.VISIBILITY_INTERCEPTOR = interceptor;
		messageBox.show(true, new MessageBox.EventListener() {

			private static final long serialVersionUID = 2948376877660010667L;

			@Override
			public void buttonClicked(ButtonType buttonType) {
				if (buttonType.equals(ButtonType.SAVE)) {
					try {
						shoppingCartForm.commit();
						shoppingCartService.saveShoppingCart(shoppingCartForm
								.getShoppingCart());

						if (event.isNewModel()) {
							ModelWasAddedEvent<ShoppingCart> event = new ModelWasAddedEvent<ShoppingCart>(
									this, ShoppingCart.class);
							event.setAddedModel(shoppingCartForm
									.getShoppingCart());
							dispatchEvent(event);

							// show added tray notification
							ShowTrayNotificationEvent trayEvent = new ShowTrayNotificationEvent(
									this,
									translate("shoppingcart.added.title"),
									translate("shoppingcart.added.info",
											new Object[] { shoppingCartForm
													.getShoppingCart()
													.getName() }));
							trayEvent.setExecutionType(ExecutionType.ASYNC);
							dispatchEvent(trayEvent);

						} else {
							ModelWasUpdatedEvent<ShoppingCart> event = new ModelWasUpdatedEvent<ShoppingCart>(
									this, ShoppingCart.class);
							event.setUpdatedModel(shoppingCartForm
									.getShoppingCart());
							dispatchEvent(event);

							// show updated tray notification
							ShowTrayNotificationEvent trayEvent = new ShowTrayNotificationEvent(
									this,
									translate("shoppingcart.updated.title"),
									translate("shoppingcart.updated.info",
											new Object[] { shoppingCartForm
													.getShoppingCart()
													.getName() }));
							trayEvent.setExecutionType(ExecutionType.ASYNC);
							dispatchEvent(trayEvent);
						}

						interceptor.setCloseAble(true);
						messageBox.close();

					} catch (InvalidValueException e) {
						interceptor.setCloseAble(false);
					}

				} else {
					interceptor.setCloseAble(true);
					messageBox.close();

				}

			}
		});

	}

	@HandleSpringMvpEvent
	public void handleDeleteShoppingCartEvent(
			final DeleteModelEvent<ShoppingCart> event) {

		MessageBox deleteConfirmation = new MessageBox(getMainWindow(),
				translate("shoppingcart.delete.confirmation.title"),
				MessageBox.Icon.QUESTION, translate(
						"shoppingcart.delete.confirmation.question",
						new Object[] { event.getToDelete().getName() }),
				new MessageBox.ButtonConfig(ButtonType.NO, translate("no")),
				new MessageBox.ButtonConfig(ButtonType.YES, translate("yes")));
		deleteConfirmation.show(true, new MessageBox.EventListener() {

			private static final long serialVersionUID = -7925625315552580719L;

			@Override
			public void buttonClicked(ButtonType buttonType) {
				if (buttonType.equals(ButtonType.YES)) {
					shoppingCartService.deleteShoppingCart(event.getToDelete());

					// dispatch deleted event
					ModelWasDeletedEvent<ShoppingCart> deletedEvent = new ModelWasDeletedEvent<ShoppingCart>(
							this, ShoppingCart.class);
					deletedEvent.setDeletedModel(event.getToDelete());
					dispatchEvent(deletedEvent);

					// show notification
					ShowTrayNotificationEvent notificationEvent = new ShowTrayNotificationEvent(
							this, translate("shoppingcart.deleted.title"),
							translate("shoppingcart.deleted.info",
									new Object[] { event.getToDelete()
											.getName() }));
					notificationEvent.setExecutionType(ExecutionType.ASYNC);
					dispatchEvent(notificationEvent);
				}

			}
		});

	}

}
