/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.ui.service;

import java.util.List;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface UiModelService<T> {

	/**
	 * Gets all Models from the backend.
	 * 
	 * @return
	 */
	List<T> getAllModels();
	
	

}
