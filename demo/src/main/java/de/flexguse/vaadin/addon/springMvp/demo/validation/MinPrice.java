/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Custom Validation annotation to be able to set minimum price in cent.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MinPriceValidator.class)
@Documented
public @interface MinPrice {

	String message() default "{price.low}";
	
	Class<?>[] groups() default {};
	
	Class<? extends Payload>[] payload() default{};
	
	float value();
	
}
