/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.demo.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validation implementation for {@link MinPrice}.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class MinPriceValidator implements ConstraintValidator<MinPrice, Float> {

	private float minPrice;

	@Override
	public void initialize(MinPrice constraintAnnotation) {
		this.minPrice = constraintAnnotation.value();

	}

	@Override
	public boolean isValid(Float price,
			ConstraintValidatorContext constraintContext) {

		return price >= minPrice;

	}

}
