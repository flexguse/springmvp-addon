package de.flexguse.vaadin.addon.springMvp.backend.model;

import static org.junit.Assert.fail;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Abstract test which contains methods which can be used in all model tests.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring/model-test-context.xml" })
public abstract class AbstractModelTest {

	@Autowired
	protected Validator validator;

	/**
	 * This helper method checks if the expected error keys match the provided
	 * errors.
	 * 
	 * @param expectedErrorKeys
	 * @param violations
	 */
	protected void checkErrorKeys(
			List<String> expectedErrorKeys,
			@SuppressWarnings("rawtypes") Set<? extends ConstraintViolation> violations) {

		if (expectedErrorKeys != null && violations != null) {

			if (expectedErrorKeys.size() != violations.size()) {
				fail("Number of expected errors (" + expectedErrorKeys.size()
						+ ") and provided errors (" + violations.size()
						+ ") is different");
			}

			for (ConstraintViolation<?> violation : violations) {
				if (!expectedErrorKeys.contains(violation.getMessageTemplate())) {
					fail("error " + violation.getMessageTemplate()
							+ " was not expected");
				}
			}

		}

		if (expectedErrorKeys == null && violations != null) {
			fail("no expected errors but provided errors");
		}

		if (expectedErrorKeys != null && violations == null) {
			fail("errors expected but none provided");
		}

	}

}
