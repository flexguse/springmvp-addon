package de.flexguse.vaadin.addon.springMvp.backend.model;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;

public class ArticleTest extends AbstractModelTest {

	@Test
	public void testArticleValidation() {

		Article article = new Article();

		// test validating empty article
		List<String> expectedErrorKeys = new ArrayList<String>();
		expectedErrorKeys.add(Article.NAME_EMPTY_EXCEPTION);
		expectedErrorKeys.add(Article.NUMBER_EMPTY_EXCEPTION);
		expectedErrorKeys.add(Article.PRICE_TOO_LOW);

		checkErrorKeys(expectedErrorKeys, validator.validate(article));

		// test article with name set only
		article.setName("Some article name");
		expectedErrorKeys.clear();
		expectedErrorKeys.add(Article.NUMBER_EMPTY_EXCEPTION);
		expectedErrorKeys.add(Article.PRICE_TOO_LOW);
		checkErrorKeys(expectedErrorKeys, validator.validate(article));

		// test article with name and article number
		article.setNumber("A-9876-N");
		expectedErrorKeys.clear();
		expectedErrorKeys.add(Article.PRICE_TOO_LOW);
		checkErrorKeys(expectedErrorKeys, validator.validate(article));

		// test article with too low price
		article.setPrice(-1.5f);
		checkErrorKeys(expectedErrorKeys, validator.validate(article));

		// test valid article
		article.setPrice(1.5f);
		expectedErrorKeys.clear();
		checkErrorKeys(expectedErrorKeys, validator.validate(article));

	}

}
