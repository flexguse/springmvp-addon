/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.backend.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;
import de.flexguse.vaadin.addon.springMvp.demo.backend.model.ShoppingCart;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class ShoppingCartTest extends AbstractModelTest {

	@Test
	public void testShoppingCatValidation(){
		
		ShoppingCart cart = new ShoppingCart();
		
		List<String> expectedErrors = new ArrayList<String>();
		expectedErrors.add(ShoppingCart.EMPTY_NAME_EXCEPTION);
		expectedErrors.add(ShoppingCart.EMPTY_NAME_EXCEPTION);
		checkErrorKeys(expectedErrors, validator.validate(cart));
		
		cart.setName("Some shopping cart");
		expectedErrors.clear();
		checkErrorKeys(expectedErrors, validator.validate(cart));
	}
	
	@Test
	public void testAddArticle(){
		
		ShoppingCart cart = new ShoppingCart();
		
		// must not throw an exception
		cart.addArticle(null);
		
		Article article = new Article();
		article.setName("Article 1");
		article.setNumber("A1");
		article.setPrice(20.98f);
		cart.addArticle(article);
		
		assertEquals(article.getPrice(), cart.getPrice());
		assertEquals(1, cart.getArticles().size());
		
		Article article2 = new Article();
		article.setName("Article 2");
		article2.setNumber("A2");
		article2.setPrice(9.99f);
		cart.addArticle(article2);
		
		assertEquals(Float.valueOf(30.97f), cart.getPrice());
		assertEquals(2, cart.getArticles().size());
		
	}
	
	@Test
	public void testRemoveArticle(){
		
		ShoppingCart cart = new ShoppingCart();
		
		// must not throw exception
		cart.removeArticle(null);
		cart.removeArticle(new Article());

		List<Article> articles = new ArrayList<Article>();
		Article article = new Article();
		article.setName("Article 1");
		article.setNumber("A1");
		article.setPrice(20.98f);
		articles.add(article);

		Article article2 = new Article();
		article.setName("Article 2");
		article2.setNumber("A2");
		article2.setPrice(9.99f);
		articles.add(article2);
		
		cart.setArticles(articles);
		assertEquals(Float.valueOf(30.97f), cart.getPrice());
		
		// must not throw exception
		Article toRemove = new Article();
		toRemove.setPrice(19.99f);
		cart.removeArticle(toRemove);
		assertEquals(Float.valueOf(30.97f), cart.getPrice());
		
		cart.removeArticle(article);
		assertEquals(1, cart.getArticles().size());
		assertEquals(article2.getPrice(), cart.getPrice());
		
	}
	
	@Test
	public void testSetArticles(){
		
		ShoppingCart cart = new ShoppingCart();
		
		cart.setArticles(null);
		cart.setArticles(new ArrayList<Article>());
		
	}
}
