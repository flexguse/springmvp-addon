/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.backend.service.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.Article;
import de.flexguse.vaadin.addon.springMvp.demo.backend.service.impl.InMemoryArticleService;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class InMemoryArticleServiceTest {

	private static final String ARTICLE_UPDATED = "Article updated";
	private InMemoryArticleService articleService;

	@Before
	public void setUp() {
		articleService = new InMemoryArticleService(false);
	}

	@Test
	public void testSaveArticle() {

		// must not throw an exception
		articleService.saveArticle(null);

		// create new article
		Article article = new Article();
		article.setName("Article");
		articleService.saveArticle(article);
		assertEquals(1, articleService.getAllArticles().size());

		// create another article
		Article article2 = new Article();
		article2.setName("Article 2");
		articleService.saveArticle(article2);
		assertEquals(2, articleService.getAllArticles().size());

		// update article
		article.setName(ARTICLE_UPDATED);
		Article updated = articleService.saveArticle(article);
		assertEquals(ARTICLE_UPDATED, updated.getName());

	}

	@Test
	public void testDeleteArticle() {

		// must not throw an exception
		articleService.deleteArticle(null);
		articleService.deleteArticle(new Article());
		
		// create new article
		Article article = new Article();
		article.setName("Article");
		articleService.saveArticle(article);

		// create another article
		Article article2 = new Article();
		article2.setName("Article 2");
		articleService.saveArticle(article2);
		
		// remove article
		articleService.deleteArticle(article);
		assertEquals(article2, articleService.getAllArticles().get(0));
		
		articleService.deleteArticle(article2);
		assertEquals(0, articleService.getAllArticles().size());
	}
	
	@Test
	public void testGetAllActiveArticles(){
		
		// must not throw an exception
		articleService.getAllActiveArticles();

		// create new article
		Article article = new Article();
		article.setName("Article");
		articleService.saveArticle(article);

		// create another article
		Article article2 = new Article();
		article2.setActive(false);
		article2.setName("Article 2");
		articleService.saveArticle(article2);

		assertEquals(1, articleService.getAllActiveArticles().size());
	}

}
