/**
 * 
 */
package de.flexguse.vaadin.addon.springMvp.backend.service.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.flexguse.vaadin.addon.springMvp.demo.backend.model.ShoppingCart;
import de.flexguse.vaadin.addon.springMvp.demo.backend.service.impl.InMemoryShoppingCartService;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class InMemoryShoppingCartServiceTest {

	private InMemoryShoppingCartService service;
	
	@Before
	public void setUp(){
		service = new InMemoryShoppingCartService();
	}
	
	@Test
	public void testSaveShoppingCart(){
		
		// must not throw an exception
		service.saveShoppingCart(null);
		
		ShoppingCart shoppingCart = new ShoppingCart();
		shoppingCart.setName("Cart 1");
		service.saveShoppingCart(shoppingCart);
		
		ShoppingCart shoppingCart2 = new ShoppingCart();
		shoppingCart2.setName("Cart 2");
		service.saveShoppingCart(shoppingCart2);
		
		assertEquals(2, service.getAllShoppingCarts().size());
		
	}
	
	@Test
	public void testDeleteShoppingCart(){
		
		// must not throw an exception
		service.deleteShoppingCart(null);
		service.deleteShoppingCart(new ShoppingCart());
		
		ShoppingCart shoppingCart = new ShoppingCart();
		shoppingCart.setName("Cart 1");
		service.saveShoppingCart(shoppingCart);
		
		ShoppingCart shoppingCart2 = new ShoppingCart();
		shoppingCart2.setName("Cart 2");
		service.saveShoppingCart(shoppingCart2);
		
		service.deleteShoppingCart(shoppingCart);
		assertEquals(1, service.getAllShoppingCarts().size());
		assertEquals(shoppingCart2, service.getAllShoppingCarts().get(0));
		
		
		
	}
	
}
