<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">


	<xsl:import href="urn:docbkx:stylesheet" />
	<xsl:import href="highlight-fo.xsl" />

	<xsl:param name="paper.type" select="A4" />

	<xsl:attribute-set name="monospace.verbatim.properties">
		<xsl:attribute name="font-family">Lucida Sans Typewriter</xsl:attribute>
		<xsl:attribute name="font-size">9pt</xsl:attribute>
		<xsl:attribute name="keep-together.within-column">auto</xsl:attribute>
		<xsl:attribute name="wrap-option">wrap</xsl:attribute>
	</xsl:attribute-set>

	<xsl:param name="shade.verbatim" select="1" />
	<xsl:param name="use.extensions" select="1" />
	<xsl:param name="linenumbering.extension" select="1" />
	<xsl:param name="linenumbering.everyNth" select="1" />
	<xsl:param name="chapter.autolabel" select="1" />
	<xsl:param name="section.autolabel" select="1"/>
	<xsl:param name="section.label.includes.component.label" select="1" />


	<xsl:attribute-set name="shade.verbatim.style">
		<xsl:attribute name="background-color">#E0E0E0</xsl:attribute>
		<xsl:attribute name="border-width">0.5pt</xsl:attribute>
		<xsl:attribute name="border-style">solid</xsl:attribute>
		<xsl:attribute name="border-color">#575757</xsl:attribute>
		<xsl:attribute name="padding">3pt</xsl:attribute>
	</xsl:attribute-set>

</xsl:stylesheet>
